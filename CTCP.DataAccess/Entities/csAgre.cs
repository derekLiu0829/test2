﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CTCP.DataAccess.Helpers;

namespace CTCP.DataAccess.Entities {

	public class csAgre {
		// cAgreNum，char，16，IS_NULLABLE : NO
		[MaxLength(16)]
		[Column(Name = "cAgreNum")]
		public string cAgreNum { get; set; }

		// cPtyId，char，20，IS_NULLABLE : NO
		[MaxLength(20)]
		[Column(Name = "cPtyId")]
		public string cPtyId { get; set; }

		// cClitCode，char，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cClitCode")]
		public string cClitCode { get; set; }

		// cAcctBnch，char，4，IS_NULLABLE : YES
		[MaxLength(4)]
		[Column(Name = "cAcctBnch")]
		public string cAcctBnch { get; set; }

		// cAcct，char，16，IS_NULLABLE : NO
		[MaxLength(16)]
		[Column(Name = "cAcct")]
		public string cAcct { get; set; }

		// cSordAcct，char，16，IS_NULLABLE : YES
		[MaxLength(16)]
		[Column(Name = "cSordAcct")]
		public string cSordAcct { get; set; }

		// cIdNo，char，16，IS_NULLABLE : NO
		[MaxLength(16)]
		[Column(Name = "cIdNo")]
		public string cIdNo { get; set; }

		// cThirdPtyType，char，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cThirdPtyType")]
		public string cThirdPtyType { get; set; }

		// cSecType，char，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cSecType")]
		public string cSecType { get; set; }

		// cSecCode，char，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cSecCode")]
		public string cSecCode { get; set; }

		// cArreStsCode，char，2，IS_NULLABLE : YES
		[MaxLength(2)]
		[Column(Name = "cArreStsCode")]
		public string cArreStsCode { get; set; }

		// cColtType，char，2，IS_NULLABLE : YES
		[MaxLength(2)]
		[Column(Name = "cColtType")]
		public string cColtType { get; set; }

		// cACStsType，char，2，IS_NULLABLE : YES
		[MaxLength(2)]
		[Column(Name = "cACStsType")]
		public string cACStsType { get; set; }

		// cProdType，char，3，IS_NULLABLE : YES
		[MaxLength(3)]
		[Column(Name = "cProdType")]
		public string cProdType { get; set; }

		// cProdCodeMain，char，4，IS_NULLABLE : YES
		[MaxLength(4)]
		[Column(Name = "cProdCodeMain")]
		public string cProdCodeMain { get; set; }

		// cProdCodeDetl，char，4，IS_NULLABLE : YES
		[MaxLength(4)]
		[Column(Name = "cProdCodeDetl")]
		public string cProdCodeDetl { get; set; }

		// cBlkCode，char，1，IS_NULLABLE : YES
		[MaxLength(1)]
		[Column(Name = "cBlkCode")]
		public string cBlkCode { get; set; }

		// cNumOfHolders，int，，IS_NULLABLE : YES
		[Column(Name = "cNumOfHolders")]
		public int? cNumOfHolders { get; set; }

		// cNumOfSecItems，char，18，IS_NULLABLE : YES
		[MaxLength(18)]
		[Column(Name = "cNumOfSecItems")]
		public string cNumOfSecItems { get; set; }

		// cArreDays，int，，IS_NULLABLE : YES
		[Column(Name = "cArreDays")]
		public int? cArreDays { get; set; }

		// cCbalAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cCbalAmt")]
		public decimal? cCbalAmt { get; set; }

		// cArrAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cArrAmt")]
		public decimal? cArrAmt { get; set; }

		// cCtmCbal，decimal，，IS_NULLABLE : YES
		[Column(Name = "cCtmCbal")]
		public decimal? cCtmCbal { get; set; }

		// cAplyAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cAplyAmt")]
		public decimal? cAplyAmt { get; set; }

		// cMinCbal，decimal，，IS_NULLABLE : YES
		[Column(Name = "cMinCbal")]
		public decimal? cMinCbal { get; set; }

		// cLoanCurBal，decimal，，IS_NULLABLE : YES
		[Column(Name = "cLoanCurBal")]
		public decimal? cLoanCurBal { get; set; }

		// cAdvAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cAdvAmt")]
		public decimal? cAdvAmt { get; set; }

		// cLnseRate，decimal，，IS_NULLABLE : YES
		[Column(Name = "cLnseRate")]
		public decimal? cLnseRate { get; set; }

		// cLnRate，decimal，，IS_NULLABLE : YES
		[Column(Name = "cLnRate")]
		public decimal? cLnRate { get; set; }

		// cPerCurAmorAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cPerCurAmorAmt")]
		public decimal? cPerCurAmorAmt { get; set; }

		// cToColAmtBal，decimal，，IS_NULLABLE : YES
		[Column(Name = "cToColAmtBal")]
		public decimal? cToColAmtBal { get; set; }

		// cArrInte，decimal，，IS_NULLABLE : YES
		[Column(Name = "cArrInte")]
		public decimal? cArrInte { get; set; }

		// cWofAmtBal，decimal，，IS_NULLABLE : YES
		[Column(Name = "cWofAmtBal")]
		public decimal? cWofAmtBal { get; set; }

		// cPndfAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cPndfAmt")]
		public decimal? cPndfAmt { get; set; }

		// cPaidAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cPaidAmt")]
		public decimal? cPaidAmt { get; set; }

		// cAvalInte，decimal，，IS_NULLABLE : YES
		[Column(Name = "cAvalInte")]
		public decimal? cAvalInte { get; set; }

		// cAvalAi，decimal，，IS_NULLABLE : YES
		[Column(Name = "cAvalAi")]
		public decimal? cAvalAi { get; set; }

		// cToColDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cToColDT")]
		public string cToColDT { get; set; }

		// cWofDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cWofDT")]
		public string cWofDT { get; set; }

		// cDateEntryToDM，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cDateEntryToDM")]
		public string cDateEntryToDM { get; set; }

		// cAplyDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cAplyDT")]
		public string cAplyDT { get; set; }

		// cPayDueDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cPayDueDT")]
		public string cPayDueDT { get; set; }

		// cFirAdvDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cFirAdvDT")]
		public string cFirAdvDT { get; set; }

		// cArrePamtGracEndDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cArrePamtGracEndDT")]
		public string cArrePamtGracEndDT { get; set; }

		// cPayAmtGracEndDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cPayAmtGracEndDT")]
		public string cPayAmtGracEndDT { get; set; }

		// cEndDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cEndDT")]
		public string cEndDT { get; set; }

		// cAcctFrzDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cAcctFrzDT")]
		public string cAcctFrzDT { get; set; }

		// cReveFiedString1，varchar，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cReveFiedString1")]
		public string cReveFiedString1 { get; set; }

		// cReveFiedString2，varchar，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cReveFiedString2")]
		public string cReveFiedString2 { get; set; }

		// cReveFiedString3，varchar，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cReveFiedString3")]
		public string cReveFiedString3 { get; set; }

		// cReveFiedString4，varchar，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cReveFiedString4")]
		public string cReveFiedString4 { get; set; }

		// cReveFiedVal1，decimal，，IS_NULLABLE : YES
		[Column(Name = "cReveFiedVal1")]
		public decimal? cReveFiedVal1 { get; set; }

		// cReveFiedVal2，decimal，，IS_NULLABLE : YES
		[Column(Name = "cReveFiedVal2")]
		public decimal? cReveFiedVal2 { get; set; }

		// cReveFiedVal3，decimal，，IS_NULLABLE : YES
		[Column(Name = "cReveFiedVal3")]
		public decimal? cReveFiedVal3 { get; set; }

		// cReveFiedVal4，decimal，，IS_NULLABLE : YES
		[Column(Name = "cReveFiedVal4")]
		public decimal? cReveFiedVal4 { get; set; }

		// cReveFiedVal5，decimal，，IS_NULLABLE : YES
		[Column(Name = "cReveFiedVal5")]
		public decimal? cReveFiedVal5 { get; set; }

		// cReveFiedDate1，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cReveFiedDate1")]
		public string cReveFiedDate1 { get; set; }

		// cReveFiedDate2，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cReveFiedDate2")]
		public string cReveFiedDate2 { get; set; }

		// cReveFiedDate3，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cReveFiedDate3")]
		public string cReveFiedDate3 { get; set; }

		// cReveFiedDate4，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cReveFiedDate4")]
		public string cReveFiedDate4 { get; set; }

		// cCretMebrNo，nvarchar，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cCretMebrNo")]
		public string cCretMebrNo { get; set; }

		// cCretDT，datetime，，IS_NULLABLE : YES
		[Column(Name = "cCretDT")]
		public DateTime? cCretDT { get; set; }

		// cMantMebrNo，nvarchar，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cMantMebrNo")]
		public string cMantMebrNo { get; set; }

		// cMantDT，datetime，，IS_NULLABLE : YES
		[Column(Name = "cMantDT")]
		public DateTime? cMantDT { get; set; }

		// SNO，int，，IS_NULLABLE : NO
		[Column(Name = "SNO")]
		public int SNO { get; set; }

		// cNewOldFlag，char，1，IS_NULLABLE : YES
		[MaxLength(1)]
		[Column(Name = "cNewOldFlag")]
		public string cNewOldFlag { get; set; }

		// cPrevBPIAccr，decimal，，IS_NULLABLE : YES
		[Column(Name = "cPrevBPIAccr")]
		public decimal? cPrevBPIAccr { get; set; }

		// cDmactInArrCnt，int，，IS_NULLABLE : YES
		[Column(Name = "cDmactInArrCnt")]
		public int? cDmactInArrCnt { get; set; }

		// cDmactArrStartDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cDmactArrStartDT")]
		public string cDmactArrStartDT { get; set; }

		// cDmactArrEndDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cDmactArrEndDT")]
		public string cDmactArrEndDT { get; set; }

		// cDmactArrStopDT，char，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cDmactArrStopDT")]
		public string cDmactArrStopDT { get; set; }
	}
}
