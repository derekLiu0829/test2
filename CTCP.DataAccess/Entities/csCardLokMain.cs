using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using CTCP.DataAccess.Helpers;


namespace CTCPDB.Migration.Core.Models.Entities {

	public class csCardLokMain {
		// cCardLokNo，nvarchar，10，IS_NULLABLE : NO
		[MaxLength(10)]
		[Column(Name = "cCardLokNo")]
		public string cCardLokNo{ get; set; }

		// cIdNo，nvarchar，20，IS_NULLABLE : YES
		[MaxLength(20)]
		[Column(Name = "cIdNo")]
		public string cIdNo{ get; set; }

		// cName，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cName")]
		public string cName{ get; set; }

		// cCrdtLmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cCrdtLmt")]
		public decimal? cCrdtLmt{ get; set; }

		// cFixLmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cFixLmt")]
		public decimal? cFixLmt{ get; set; }

		// cBillingCycle，int，，IS_NULLABLE : YES
		[Column(Name = "cBillingCycle")]
		public int? cBillingCycle{ get; set; }

		// cBal，decimal，，IS_NULLABLE : YES
		[Column(Name = "cBal")]
		public decimal? cBal{ get; set; }

		// cArreSts，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cArreSts")]
		public string cArreSts{ get; set; }

		// cLstPamtDT，nvarchar，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cLstPamtDT")]
		public string cLstPamtDT{ get; set; }

		// cLstPamtAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cLstPamtAmt")]
		public decimal? cLstPamtAmt{ get; set; }

		// cServiceUnit，nvarchar，30，IS_NULLABLE : YES
		[MaxLength(30)]
		[Column(Name = "cServiceUnit")]
		public string cServiceUnit{ get; set; }

		// cCusBlk，nvarchar，20，IS_NULLABLE : YES
		[MaxLength(20)]
		[Column(Name = "cCusBlk")]
		public string cCusBlk{ get; set; }

		// cNgtSts，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cNgtSts")]
		public string cNgtSts{ get; set; }

		// cBCardNum，int，，IS_NULLABLE : YES
		[Column(Name = "cBCardNum")]
		public int? cBCardNum{ get; set; }

		// cBCardAmt，decimal，，IS_NULLABLE : YES
		[Column(Name = "cBCardAmt")]
		public decimal? cBCardAmt{ get; set; }

		// cSuspendDT，datetime，，IS_NULLABLE : YES
		[Column(Name = "cSuspendDT")]
		public DateTime? cSuspendDT{ get; set; }

		// cSuspendYears，int，，IS_NULLABLE : YES
		[Column(Name = "cSuspendYears")]
		public int? cSuspendYears{ get; set; }

		// cApplDT，nchar，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cApplDT")]
		public string cApplDT{ get; set; }

		// cApplItem，nchar，1，IS_NULLABLE : YES
		[MaxLength(1)]
		[Column(Name = "cApplItem")]
		public string cApplItem{ get; set; }

		// cApplMebr，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cApplMebr")]
		public string cApplMebr{ get; set; }

		// cAPPFlag，nchar，1，IS_NULLABLE : YES
		[MaxLength(1)]
		[Column(Name = "cAPPFlag")]
		public string cAPPFlag{ get; set; }

		// cAdjUrgency，nchar，2，IS_NULLABLE : YES
		[MaxLength(2)]
		[Column(Name = "cAdjUrgency")]
		public string cAdjUrgency{ get; set; }

		// cMail，nchar，4，IS_NULLABLE : YES
		[MaxLength(4)]
		[Column(Name = "cMail")]
		public string cMail{ get; set; }

		// cSendAddr，nchar，4，IS_NULLABLE : YES
		[MaxLength(4)]
		[Column(Name = "cSendAddr")]
		public string cSendAddr{ get; set; }

		// cABPPred，bit，，IS_NULLABLE : YES
		[Column(Name = "cABPPred")]
		public bool? cABPPred{ get; set; }

		// cCRG，nchar，1，IS_NULLABLE : YES
		[MaxLength(1)]
		[Column(Name = "cCRG")]
		public string cCRG{ get; set; }

		// cGetCardDT，nchar，8，IS_NULLABLE : YES
		[MaxLength(8)]
		[Column(Name = "cGetCardDT")]
		public string cGetCardDT{ get; set; }

		// cPamtLevl，nchar，2，IS_NULLABLE : YES
		[MaxLength(2)]
		[Column(Name = "cPamtLevl")]
		public string cPamtLevl{ get; set; }

		// cOver，bit，，IS_NULLABLE : YES
		[Column(Name = "cOver")]
		public bool? cOver{ get; set; }

		// cExcpCons，bit，，IS_NULLABLE : YES
		[Column(Name = "cExcpCons")]
		public bool? cExcpCons{ get; set; }

		// cBankName，nvarchar，20，IS_NULLABLE : YES
		[MaxLength(20)]
		[Column(Name = "cBankName")]
		public string cBankName{ get; set; }

		// cIsForCmpyAcct，bit，，IS_NULLABLE : YES
		[Column(Name = "cIsForCmpyAcct")]
		public bool? cIsForCmpyAcct{ get; set; }

		// cCmpyName，nvarchar，30，IS_NULLABLE : YES
		[MaxLength(30)]
		[Column(Name = "cCmpyName")]
		public string cCmpyName{ get; set; }

		// cChFileSour，nchar，2，IS_NULLABLE : YES
		[MaxLength(2)]
		[Column(Name = "cChFileSour")]
		public string cChFileSour{ get; set; }

		// cIsClr，nchar，1，IS_NULLABLE : YES
		[MaxLength(1)]
		[Column(Name = "cIsClr")]
		public string cIsClr{ get; set; }

		// cChFileStip，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cChFileStip")]
		public string cChFileStip{ get; set; }

		// cClerAttt，bit，，IS_NULLABLE : YES
		[Column(Name = "cClerAttt")]
		public bool? cClerAttt{ get; set; }

		// cBankDepo，bit，，IS_NULLABLE : YES
		[Column(Name = "cBankDepo")]
		public bool? cBankDepo{ get; set; }

		// cDeCtrlDece，bit，，IS_NULLABLE : YES
		[Column(Name = "cDeCtrlDece")]
		public bool? cDeCtrlDece{ get; set; }

		// cJobAttt，bit，，IS_NULLABLE : YES
		[Column(Name = "cJobAttt")]
		public bool? cJobAttt{ get; set; }

		// cPamtAttt，bit，，IS_NULLABLE : YES
		[Column(Name = "cPamtAttt")]
		public bool? cPamtAttt{ get; set; }

		// cBiop，bit，，IS_NULLABLE : YES
		[Column(Name = "cBiop")]
		public bool? cBiop{ get; set; }

		// cBiopDesc，nvarchar，100，IS_NULLABLE : YES
		[MaxLength(100)]
		[Column(Name = "cBiopDesc")]
		public string cBiopDesc{ get; set; }

		// cGrantNo，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cGrantNo")]
		public string cGrantNo{ get; set; }

		// cColtNo，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cColtNo")]
		public string cColtNo{ get; set; }

		// cColtRec，nvarchar，200，IS_NULLABLE : YES
		[MaxLength(200)]
		[Column(Name = "cColtRec")]
		public string cColtRec{ get; set; }

		// cAplyMang，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cAplyMang")]
		public string cAplyMang{ get; set; }

		// cAplySts，nchar，1，IS_NULLABLE : YES
		[MaxLength(1)]
		[Column(Name = "cAplySts")]
		public string cAplySts{ get; set; }

		// cAplyDT，datetime，，IS_NULLABLE : YES
		[Column(Name = "cAplyDT")]
		public DateTime? cAplyDT{ get; set; }

		// cClsNo，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cClsNo")]
		public string cClsNo{ get; set; }

		// cClsRsn，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cClsRsn")]
		public string cClsRsn{ get; set; }

		// cClsDesc，nvarchar，100，IS_NULLABLE : YES
		[MaxLength(100)]
		[Column(Name = "cClsDesc")]
		public string cClsDesc{ get; set; }

		// cCaseSts，nchar，4，IS_NULLABLE : YES
		[MaxLength(4)]
		[Column(Name = "cCaseSts")]
		public string cCaseSts{ get; set; }

		// cMesgeRepy，nvarchar，4，IS_NULLABLE : YES
		[MaxLength(4)]
		[Column(Name = "cMesgeRepy")]
		public string cMesgeRepy{ get; set; }

		// cMesgeErrDesc，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cMesgeErrDesc")]
		public string cMesgeErrDesc{ get; set; }

		// cCretMebrNo，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cCretMebrNo")]
		public string cCretMebrNo{ get; set; }

		// cCretDT，datetime，，IS_NULLABLE : YES
		[Column(Name = "cCretDT")]
		public DateTime? cCretDT{ get; set; }

		// cMantMebrNo，nvarchar，50，IS_NULLABLE : YES
		[MaxLength(50)]
		[Column(Name = "cMantMebrNo")]
		public string cMantMebrNo{ get; set; }

		// cMantDT，datetime，，IS_NULLABLE : YES
		[Column(Name = "cMantDT")]
		public DateTime? cMantDT{ get; set; }

		// SNO，int，，IS_NULLABLE : NO
		[Column(Name = "SNO")]
		public int SNO{ get; set; }

		// cCaseGrp，char，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cCaseGrp")]
		public string cCaseGrp{ get; set; }

		// cCaseGrpDetl，char，10，IS_NULLABLE : YES
		[MaxLength(10)]
		[Column(Name = "cCaseGrpDetl")]
		public string cCaseGrpDetl{ get; set; }

}

}