﻿using CTCP.DataAccess.Models.Kendo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTCP.DataAccess.Models.Demo {

	public class Demo5SearchModel : KendoGridSearchModel {
		public string Cond1 { get; set; }
		public string Cond2 { get; set; }
		public int Cond3 { get; set; }
	}
}
