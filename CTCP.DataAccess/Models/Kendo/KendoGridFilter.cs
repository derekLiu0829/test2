﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTCP.DataAccess.Models.Kendo {

	public class KendoGridFilter {
		public string Field { get; set; }
		public string Operator { get; set; }
		public string Value { get; set; }
		public string Logic { get; set; }
		public List<KendoGridFilter> Filters { get; set; }
	}
}
