﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTCP.DataAccess.Models.Kendo {

	public class KendoGridFilterLogic {
		public List<KendoGridFilter> Filters { get; set; }
		public string Logic { get; set; }
	}
}
