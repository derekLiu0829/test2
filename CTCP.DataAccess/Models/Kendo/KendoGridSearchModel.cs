﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTCP.DataAccess.Models.Kendo {

	public class KendoGridSearchModel {
		public int Take { get; set; }
		public int Skip { get; set; }
		public int Page { get; set; }
		public int PageSize { get; set; }
		public List<KendoGridSort> Sort { get; set; }
		public KendoGridFilterLogic Filter { get; set; }
	}
}
