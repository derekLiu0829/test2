﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTCP.DataAccess.Models.Kendo {

	public class KendoGridSort {
		public string Field { get; set; }
		public string Dir { get; set; }
	}
}
