﻿using CTCP.DataAccess.Entities;
using CTCP.DataAccess.Models.Demo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTCP.DataAccess.Repositories {

	public interface ICsAgreStore {
		Tuple<List<csAgre>, int> List(Demo5SearchModel sm);
		Tuple<List<csAgre>, int> List_slow(Demo5SearchModel sm);
	}
}
