﻿using System;
using CTCP.DataAccess.Entities;
using CTCP.DataAccess.Models.Demo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTCPDB.Migration.Core.Models.Entities;

namespace CTCP.DataAccess.Repositories {
	public interface ICsCardLokMain {
		Tuple<List<csCardLokMain>, int> List(Page20SearchModel sm);
		Tuple<List<csCardLokMain>, int> List_slow(Page20SearchModel sm);
	}
}
