﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CTCP.WEB.Controllers {

	public class BaseController : Controller {
		protected readonly string error = "error";
		protected readonly string success = "success";

		protected override void Initialize(RequestContext requestContext) {
			base.Initialize(requestContext);

			requestContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			requestContext.HttpContext.Response.Cache.SetExpires(DateTime.MinValue);
			requestContext.HttpContext.Response.Cache.SetNoStore();
		}

		protected string GuidToString(Guid? guid) {
			if (guid.HasValue) {
				return guid.Value.ToString();
			}
			return string.Empty;
		}
	}

}