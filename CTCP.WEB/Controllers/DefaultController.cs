﻿using CTCP.DataAccess.Entities;
using CTCP.DataAccess.Models.Demo;
using CTCP.DataAccess.Repositories;
using CTCP.WEB.Helpers;
using CTCP.WEB.Models;
using CTCP.WEB.Models.Demo9;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CTCP.WEB.Controllers {

	public class DefaultController : Controller {
		private ICsAgreStore csAgreStore;

		public DefaultController() {

		}

		public DefaultController(ICsAgreStore csAgreStore) {
			this.csAgreStore = csAgreStore;
		}		

		public ActionResult Index() {
			// var sec = Session["User"] as Security;
			// Session[SessionKeys.SITE_MAP_DATA] = SiteMapHelper.GetMenus(sec);

			return View();
		}

		public ActionResult Demo1() {
			return View();
		}

		public ActionResult Demo2() {
			return View();
		}

		public ActionResult Demo3() {
			return View();
		}

		public ActionResult Demo4() {
			return View();
		}

		/// <summary>
		/// Grid simple sample
		/// </summary>
		/// <returns></returns>
		public ActionResult Demo5() {
			return View();
		}

		[HttpPost]
		public ActionResult Demo5_Read(Demo5SearchModel sm) {
			var tuple = csAgreStore.List(sm);

			return Json(new { data = tuple.Item1, pageSize = tuple.Item2 });
		}

		public ActionResult Demo6() {
			return View();
		}

		public ActionResult Demo7() {
			return View();
		}

		public ActionResult Demo8() {
			return View();
		}

		#region 工作清單

		public ActionResult Demo9() {
			return View();
		}

		[HttpPost]
		public ActionResult Demo9_Read(Demo5SearchModel sm) {
			var data = new List<object> {
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 12000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 15000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 15000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
				new { c1 = "A123456789", c2 = "王測試", c3 = "信用卡、小額貸款", c4 = 16000, c5=56655, c6 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c7  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c8  = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c9  = "良好", c10 = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), c11 = "R3", c12 = "禎慧崔", c13 = 88123},
			};

			return Json(new {
				data,
				pageSize = data.Count });
		}

		/// <summary>
		/// 讀取所有欄位
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult Demo9_LoadGridInfo() {
			return Json(new {
				columns = MakeGridColumns(),
				model = MakeGridModel()
			});
		}

		private List<Demo9Column> MakeGridColumns() {
			var list = new List<Demo9Column>() {
				new Demo9Column { field = "c1", title = "統一編號", locked = true, lockable = true },
				new Demo9Column { field = "c2", title = "客戶姓名", },
				new Demo9Column { field = "c3", title = "產品別", },
				new Demo9Column { field = "c4", title = "應收合計", },
				new Demo9Column { field = "c5", title = "呆帳餘額", },
				new Demo9Column { field = "c6", title = "最近聯絡日", format = "{0:yyyy/MM/dd}", parseFormats ="yyyy/MM/dd hh:mm:ss", filterable = new Demo9ColumnFilter { extra = true} },
				new Demo9Column { field = "c7", title = "Callback", format = "{0:yyyy/MM/dd}", parseFormats ="yyyy/MM/dd hh:mm:ss", filterable = new Demo9ColumnFilter { extra = true}  },
				new Demo9Column { field = "c8", title = "最近繳款日", format = "{0:yyyy/MM/dd}", parseFormats ="yyyy/MM/dd hh:mm:ss", filterable = new Demo9ColumnFilter { extra = true}  },
				new Demo9Column { field = "c9", title = "還款態樣", },
				new Demo9Column { field = "c10", title = "分案日", format = "{0:yyyy/MM/dd}", parseFormats ="yyyy/MM/dd hh:mm:ss", filterable = new Demo9ColumnFilter { extra = true} },
				new Demo9Column { field = "c11", title = "SubSegment", },
				new Demo9Column { field = "c12", title = "催收員", },
				new Demo9Column { field = "c13", title = "分機", },
			};

			var extraColumns = GetExtraColumns();
			list.AddRange(GetExtraColumns());

			if (extraColumns.Count > 0) {
				list.Add(new Demo9Column { command = new List<string> { "edit" }, title = "動作", width = 100 });
			}

			var hiddenColumns = GetHiddenColumns();
			list.ForEach(c => {
				c.hidden = hiddenColumns.Any(h => h == c.field);
			});

			return list;
		}

		private Demo9GridModel MakeGridModel() {
			var model =  new Demo9GridModel();
			return model;
		}

		/// <summary>
		/// 儲存自定義欄位
		/// </summary>
		[HttpPost]
		public ActionResult Demo9_SaveExtraColumn(string title) {
			var extraColumns = GetExtraColumns();
			extraColumns.Add(new Demo9Column() { title = title, field = $"guid_{Guid.NewGuid().ToString("N")}" });

			Session["Demo9_ExtraColumns"] = extraColumns;
			return Json(new { result = "success" });
		}

		/// <summary>
		/// 讀取自定義欄位
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult Demo9_ListExtraColumns() {
			var extraColumns = GetExtraColumns();
			return Json(extraColumns);
		}

		/// <summary>
		/// 刪除自定義欄位
		/// </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult Demo9_RemoveExtraColumn(string field) {
			var extraColumns = GetExtraColumns();
			var column = extraColumns.Where(c => c.field == field).FirstOrDefault();
			extraColumns.Remove(column);
			Session["Demo9_ExtraColumns"] = extraColumns;
			return Json(new { });
		}

		/// <summary>
		/// 儲存要顯示/隱藏的欄位
		/// </summary>
		[HttpPost]
		public ActionResult Demo9_SaveHiddenColumn(string field, bool isHidden) {
			var list = GetHiddenColumns();

			var hasData = list.Any(c => c == field);
			if (isHidden && !hasData) {
				list.Add(field);

			} else if(!isHidden && hasData) {
				list.Remove(list.Where(c => c == field).FirstOrDefault());
			}

			Session["Demo9_HiddenColumns"] = list;
			return Json(new { result = "success" });
		}

		/// <summary>
		/// 科、組、員下拉式選單
		/// </summary>
		[HttpPost]
		public ActionResult Demo9_GetItems(string parentId, int level) {
			var list = GetItems().Where(i => i.Level == level).ToList();
			if (level > 1) {
				list = list.Where(i => i.ParentId == parentId).ToList();
			}
			return Json(list);
		}

		private List<string> GetHiddenColumns() {
			var list = (List<string>)Session["Demo9_HiddenColumns"];
			if (list == null) {
				list = new List<string>();
			}
			return list;
		}

		private List<Demo9Column> GetExtraColumns() {
			var list = (List<Demo9Column>)Session["Demo9_ExtraColumns"];
			if (list == null) {
				list = new List<Demo9Column>();
			}
			return list;
		}

		private List<Demo9Item> GetItems() {
			var items = new List<Demo9Item>();
			items.Add(new Demo9Item { Id = "001", Level = 1, Title = "集作一科" });
			items.Add(new Demo9Item { Id = "001_1", Level = 2, Title = "集作一組", ParentId = "001" });
			items.Add(new Demo9Item { Id = "001_1_1", Level = 3, Title = "陳小威", ParentId = "001_1" });
			items.Add(new Demo9Item { Id = "001_1_2", Level = 3, Title = "酐慧崔", ParentId = "001_1" });
			items.Add(new Demo9Item { Id = "001_2", Level = 2, Title = "集作二組", ParentId = "001" });
			items.Add(new Demo9Item { Id = "001_2_1", Level = 3, Title = "步想崔", ParentId = "001_2" });
			items.Add(new Demo9Item { Id = "001_2_2", Level = 3, Title = "夏次崔", ParentId = "001_2" });

			items.Add(new Demo9Item { Id = "002", Level = 1, Title = "呆帳回收四科" });
			items.Add(new Demo9Item { Id = "002_1", Level = 2, Title = "呆帳回收四科一組", ParentId = "002" });
			items.Add(new Demo9Item { Id = "002_1_1", Level = 3, Title = "催崔崔", ParentId = "002_1" });
			items.Add(new Demo9Item { Id = "002_1_2", Level = 3, Title = "禎慧崔", ParentId = "002_1" });
			items.Add(new Demo9Item { Id = "002_2", Level = 2, Title = "呆帳回收四科二組", ParentId = "002" });
			items.Add(new Demo9Item { Id = "002_2_1", Level = 3, Title = "崔崔", ParentId = "002_2" });
			items.Add(new Demo9Item { Id = "002_2_2", Level = 3, Title = "林大催", ParentId = "002_2" });

			items.Add(new Demo9Item { Id = "003", Level = 1, Title = "理債一科" });
			items.Add(new Demo9Item { Id = "003_1", Level = 2, Title = "理債一科一組", ParentId = "003" });
			items.Add(new Demo9Item { Id = "003_1_1", Level = 3, Title = "劉崔崔", ParentId = "003_1" });
			items.Add(new Demo9Item { Id = "003_1_2", Level = 3, Title = "吳小催", ParentId = "003_1" });
			items.Add(new Demo9Item { Id = "003_2", Level = 2, Title = "理債一科二組", ParentId = "003" });
			items.Add(new Demo9Item { Id = "003_2_1", Level = 3, Title = "張崔崔", ParentId = "003_2" });
			items.Add(new Demo9Item { Id = "003_2_2", Level = 3, Title = "鄭小催", ParentId = "003_2" });

			return items;
		}

		#endregion

		/// <summary>
		/// Grid keyword search
		/// Inline Edit
		/// </summary>
		public ActionResult Demo10() {
			return View();
		}

		[HttpPost]
		public ActionResult Demo10_Read(Demo5SearchModel sm) {
			var tuple = csAgreStore.List(sm);

			return Json(new { data = tuple.Item1, pageSize = tuple.Item2 });
		}

		[HttpPost]
		public ActionResult Demo10_Update(csAgre m) {
			m.cPtyId = "666";
			var items = new List<csAgre>() { m };

			return Json(new { Message = "更新成功" });
			// return Json(new { Message = "更新失敗" });
		}

		/// <summary>
		/// Grid, endless scrolling of remote data
		/// </summary>
		public ActionResult Demo11() {
			return View();
		}

		[HttpPost]
		public ActionResult Demo11_Read(Demo5SearchModel sm) {
			var tuple = csAgreStore.List(sm);

			return Json(new { data = tuple.Item1, pageSize = tuple.Item2 });
		}

		/// <summary>
		/// Grid, Context menu
		/// </summary>
		/// <returns></returns>
		public ActionResult Demo12() {
			return View();
		}

		[HttpPost]
		public ActionResult Demo12_Read(Demo5SearchModel sm) {
			var tuple = csAgreStore.List(sm);

			return Json(new { data = tuple.Item1, pageSize = tuple.Item2 });
		}
	}
}