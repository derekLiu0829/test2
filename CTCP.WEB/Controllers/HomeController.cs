﻿using CTCP.WEB.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CTCP.WEB.Controllers {

	public class HomeController : BaseController {

		public HomeController() {
		}

		public ActionResult Index() {

			//var sec = (Security)Session[SessionKeys.USER];
			//if (sec != null) {
			//	Session[SessionKeys.SITE_MAP_DATA] = SiteMapHelper.GetMenus(sec);
			//} else {
				Session[SessionKeys.SITE_MAP_DATA] = SiteMapHelper.MakeDemoMenu();
			//}

			return View();
		}


	}
}