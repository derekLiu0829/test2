﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Helpers {

	public class SessionKeys {
		public static string SSO_USER_DATA = "SSO_USER_DATA";
		public static string SITE_MAP_DATA = "SITE_MAP_DATA";
		public static string USER_ID = "USER_ID";
		public static string USER_NAME_ALIAS = "USER_NAME_ALIAS";
		public static string USER_LAST_SUCCESS_LOGIN = "USER_LAST_SUCCESS_LOGIN";
		public static string USER_LAST_FAILED_LOGIN = "USER_LAST_FAILED_LOGIN";
		public static string FAVE_ITEMS = "FAVE_ITEMS";

		public static string SHUFFLE_QUEUE_STATE = "ShuffleQueueState";
		public static string USER = "user";
		public static string LOGIN = "Login";
		public static string LOC = "Loc";
		public static string FILE_NO = "fileNo";
		public static string USER_UNIT_NAME = "USER_UNIT_NAME";
		public static string USER_NAME = "USER_NAME";
	}
}