﻿using CTCP.WEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Helpers {

	public class SiteMapHelper {		

		private static List<MenuVM> GetMenuItems(DataTable dataTable, string parentSno) {
			var list = new List<MenuVM>();

			var view = new DataView(dataTable);
			view.RowFilter = "ParentSno='" + parentSno + "'";

			foreach (DataRowView row in view) {
				var funName = row["FunName"].ToString();
				var sno = row["sno"].ToString();
				var url = row["URL"].ToString();

				list.Add(new MenuVM {
					Id = sno,
					Name = funName,
					Controller = "Default",
					Action = "Hello",
					Area = string.Empty,
					Url = url,
					Items = GetMenuItems(dataTable, sno)

				});
			}
			return list;
		}

		public static List<MenuVM> MakeDemoMenu() {
			var list = new List<MenuVM>();

			list.Add(new MenuVM {
				Name = "Demo功能",
				Items = new List<MenuVM> {
					new MenuVM {
						Name = "首頁",
						Url = "/Demo/Page6_1"
					},
					new MenuVM {
						Name = "工作清單",
						Url = "/Demo/Page13"
					},
					new MenuVM {
						Name = "催收主畫面",
						Url = "/Demo/Page7"
					},
					new MenuVM {
						Name = "查找資訊",
						Url = "/Demo/Page8"
					},
					new MenuVM {
						Name = "影像檔1",
						Url = "/Demo/Page9"
					},
					new MenuVM {
						Name = "影像檔2",
						Url = "/Demo/Page10"
					},
					new MenuVM {
						Name = "主管Campaign",
						Url = "/Demo/Page11"
					},
					new MenuVM {
						Name = "主管Campaign查詢",
						Url = "/Demo/Page12"
					},
					new MenuVM {
						Name = "CSI Compaign",
						Url = "/Demo/Page14"
					},
					new MenuVM {
						Name = "繳款明細查詢",
						Url = "/Demo/Page15"
					},

					new MenuVM {
						Name = "測試",
						Items = new List<MenuVM> {
								new MenuVM {
								Name = "測試首頁",
								Url = "/Front/login.aspx"
							},
							new MenuVM {
								Name = "測試催收主畫面",
								Url = "/Front/login.aspx"
							},
						}
					},

					new MenuVM {
						Name = "FN001",
						Items = new List<MenuVM> {
								new MenuVM {
								Name = "個人首頁",
								Url = "/FN001/FN00101"
							},
							new MenuVM {
								Name = "催收主畫面",
								Url = "/FN001/FN00102"
							},
							new MenuVM {
								Name = "工作清單",
								Url = "/FN001/FN00103"
							},
							new MenuVM {
								Name = "主管Campaign",
								Url = "/FN001/FN00104"
							},
						}
					}

				}
			});

			return list;
		}
	}
}