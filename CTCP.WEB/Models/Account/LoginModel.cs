﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Models.Account {

	public class LoginModel {

		/// <summary>
		/// 帳號 LDAP
		/// </summary>
		public string UserNameLDAP { get; set; }

		/// <summary>
		/// 密碼 LDAP
		/// </summary>
		public string StextLDAP { get; set; }

		/// <summary>
		/// 帳號 RCAF
		/// </summary>
		public string UserNameRCAF { get; set; }

		/// <summary>
		/// 密碼 RCAF
		/// </summary>
		public string StextRCAF { get; set; }

		/// <summary>
		/// 分行別 RCAF
		/// </summary>
		public string BranchRCAF { get; set; }

		/// <summary>
		/// 工作選項
		/// 個人Q或是ShareWorkQ
		/// </summary>
		public string WorkOption { get; set; }

		/// <summary>
		/// 工作平台
		/// 前台 = loc1
		/// 後台 = loc2
		/// </summary>
		public string Platform { get; set; }

	}
}