﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Models.Demo9 {

	public class Demo9Column {
		public List<string> command { get; set; }
		public string field { get; set; }
		public string title { get; set; }
		public int width { get; set; } = 150;
		public bool hidden { get; set; } = false;
		public bool locked { get; set; } = false;
		public bool lockable { get; set; } = true;
		public string format { get; set; }
		public string parseFormats { get; set; }
		public string headerTemplate { get; set; }
		public Demo9ColumnFilter filterable { get; set; }
		public HeaderAttributes headerAttributes { get; set; }

	}

	public class HeaderAttributes {
		public string style { get; set; }
	}
}