﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Models.Demo9 {

	public class Demo9GridField {

		public bool editable { get; set; } = false;
		public string type { get; set; } = "string";
	}
}