﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Models.Demo9 {

	public class Demo9GridFields {

		public Demo9GridField c1 { get; set; } = new Demo9GridField();
		public Demo9GridField c2 { get; set; } = new Demo9GridField();
		public Demo9GridField c3 { get; set; } = new Demo9GridField();
		public Demo9GridField c4 { get; set; } = new Demo9GridField { type = "number" };
		public Demo9GridField c5 { get; set; } = new Demo9GridField { type = "number" };
		public Demo9GridField c6 { get; set; } = new Demo9GridField { type = "date" };
		public Demo9GridField c7 { get; set; } = new Demo9GridField { type = "date" };
		public Demo9GridField c8 { get; set; } = new Demo9GridField { type = "date" };
		public Demo9GridField c9 { get; set; } = new Demo9GridField();
		public Demo9GridField c10 { get; set; } = new Demo9GridField { type = "date" };
		public Demo9GridField c11 { get; set; } = new Demo9GridField();
		public Demo9GridField c12 { get; set; } = new Demo9GridField();
		public Demo9GridField c13 { get; set; } = new Demo9GridField { type = "number" };
	}
}