﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Models.Demo9 {

	public class Demo9GridModel {
		public string id { get; set; } = "c1";
		public Demo9GridFields fields { get; set; } = new Demo9GridFields();
	}
}