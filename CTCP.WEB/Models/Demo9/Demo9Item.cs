﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Models.Demo9 {

	public class Demo9Item {
		public string Id { get; set; }
		public string ParentId { get; set; }
		public string Title { get; set; }
		public int Level { get; set; }
	}
}