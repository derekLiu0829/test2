﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTCP.WEB.Models {

	public class MenuVM {

		public string Id { get; set; }
		public string Name { get; set; }
		public string Action { get; set; }
		public string Area { get; set; }
		public string Controller { get; set; }
		public string Url { get; set; }
		public List<MenuVM> Items { get; set; } = new List<MenuVM>();
	}
}