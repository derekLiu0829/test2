﻿// JScript 文件
function bindKeyEnterDownToQuery(eventSourceId, menuId) {
    if ($(eventSourceId)) {
        if(window.addEventListener) {
            //for not ie such as firefox
            $(eventSourceId).addEventListener('keydown', function(){
                                                            var keyCode = event.which; 
                                                            if (keyCode == 13) {
                                                                eval('setButtoinClickedFor_' + menuId + '(4);');
                                                                event.returnValue = false;
                                                            }   
                                                            }, false);     
        }else {
            // for ie
            $(eventSourceId).attachEvent('onkeydown', function(){
                                                            var keyCode = event.keyCode;  
                                                            if (keyCode == 13) {
                                                                eval('setButtoinClickedFor_' + menuId + '(4);');
                                                                event.returnValue = false;
                                                            }
                                                            });           
        }
    }
}

function locateQueryArea(){
          var o=document.getElementsByTagName("table"); 
         for(i=0;i<o.length;i++){  
          if(o[i].id.indexOf('aspQueryTable') >=0){
              var inputs = document.getElementById(o[i].id).getElementsByTagName("input"); 
                    for(var j=0;j<inputs.length;j++)
                    {
                        if(inputs[j].type=="text")
                        {
                            bindKeyEnterDownToQuery(inputs[j].id,o[i].getAttribute('title'));
                        }
                    }
                    
              var selects = document.getElementById(o[i].id).getElementsByTagName("select"); 
                    for(var j=0;j<selects.length;j++)
                    {
                        if(selects[j].type=="select-one")
                        {
                            bindKeyEnterDownToQuery(selects[j].id,o[i].getAttribute('title'));
                        }
                    }
             }
          }
    }
