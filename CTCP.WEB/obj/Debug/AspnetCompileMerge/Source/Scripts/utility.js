﻿
function isIEBrowser() {
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");
	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
		return true;
	}
	return false;
}

function validateEmail(value) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(value);
}

function validateNumeric(value) {
	var re = /^\d+$/;
	return re.test(value);
}

function isInt(v) {
	var re = /^-?[0-9]+$/;
	return re.test(v);
}

function isFloat(v) {
	if (isInt(v)) {
		return true;
	}
	var re = /^[-+]?[0-9]+\.[0-9]+$/;
	return re.test(v);
}

function formatDegree(value) {
	///<summary>将度转换成为度分秒</summary>
	value = Math.abs(value);
	var v1 = Math.floor(value);//度
	var v2 = padding1(Math.floor((value - v1) * 60), 2);//分
	var v3 = padding1(((value - v1) * 3600 % 60).toFixed(2), 5);//秒

	return v1 + '' + v2 + '' + v3;
};

function padding1(num, length) {
	for (var len = (num + "").length; len < length; len = num.length) {
		num = "0" + num;
	}
	return num;
}

function formatDegreeCheck(value, type) {

	try {

		console.log("formatDegreeCheck value:" + value);
		console.log("formatDegreeCheck type:" + type);
		//如果有度分秒格式我就當是tgos來的，如果是沒有這度分秒格式的我就去做轉換(目前先這樣判斷，之後有空再加上後端度分秒判斷比較安全)
		var template;

		if (type == 'Lat') {
			template = /^[0-8][0-9]{5}.[0-9]{2}/g;

			if (value.length != 9) {
				return null;
			}
		}

		if (type == 'Lng') {
			template = /^[0-1][0-7][0-9]{5}.[0-9]{2}/g;

			if (value.length != 10) {
				return null;
			}
		}

		var result = template.exec(value);  

		console.log("formatDegreeCheck template:" + template);
		console.log("formatDegreeCheck result:" + result);

		if (result) {
			return value;
		} else {
			return null;
		}
	} catch (error) {
		console.log(error.message);
		return null;
	}
}


function post(path, params, method) {
	method = method || 'post';

	var form = document.createElement('form');
	form.setAttribute('method', method);
	form.setAttribute('action', path);

	for (var key in params) {
		if (params.hasOwnProperty(key)) {
			var hiddenField = document.createElement('input');
			hiddenField.setAttribute('type', 'hidden');
			hiddenField.setAttribute('name', key);
			hiddenField.setAttribute('value', params[key]);
			form.appendChild(hiddenField);
		}
	}

	document.body.appendChild(form);
	form.submit();
}

function pagination(page, take, count, callback) {
	var pageNumber = page;
	var numberOfRowsPerPage = take;
	var totalPageCount = Math.ceil(count / numberOfRowsPerPage);

	var div = $('#pagination');
	var showCount = 0;
	var pages = [];
	for (var j = 0; j < 5; j++) {
		if (showCount >= 5) {
			break;
		}
		for (var i = 0; i < totalPageCount; i++) {
			if (Math.abs(i - pageNumber) != j) {
				continue;
			}
			showCount++;
			pages.push(i);
		}
	}
	pages.sort(sortInt);
	// Show first page
	var showFirstPage = true;
	for (var i = 0; i < pages.length; i++) {
		if (pages[i] == 0) {
			showFirstPage = false;
		}
	}
	if (showFirstPage && totalPageCount > 5) {
		div.append("<li style=\"cursor:pointer;\"><a page='0'>1</a></li>");
	}
	// Show prev page
	if (pageNumber > 0) {
		div.append("<li style=\"cursor:pointer;\"><a page='" + (pageNumber - 1) + "'>" + "&laquo;" + "</a></li>");
	}
	// Show nearest 5 pages
	if (pages.length > 1) {
		for (var i = 0; i < pages.length; i++) {
			if (pages[i] == pageNumber) {
				div.append("<li class='page-item active' style=\"cursor:pointer;\"><a page='" + pages[i] + "'>" + (pages[i] + 1) + "</a></li>");
			} else {
				div.append("<li class='page-item' style=\"cursor:pointer;\"><a page='" + pages[i] + "'>" + (pages[i] + 1) + "</a></li>");
			}
		}
	}
	// Show next page
	if (pageNumber < totalPageCount - 1) {
		div.append("<li style=\"cursor:pointer;\"><a page='" + (pageNumber + 1) + "'>" + "&raquo;" + "</a></li>");
	}
	// Show last page
	var showLastPage = true;
	for (var i = 0; i < pages.length; i++) {
		if (pages[i] == totalPageCount - 1) {
			showLastPage = false;
		}
	}
	if (showLastPage && totalPageCount > 5) {
		div.append("<li style=\"cursor:pointer;\"><a page='" + (totalPageCount - 1) + "'>" + totalPageCount + "</a></li>");
	}
	// Pagination event
	$('#pagination li a').click(function (e) {
		var page = $(this).attr('page');
		if (callback) {
			callback(page);
		}
	});
}

function sortInt(a, b) {
	return a - b;
}

function shareFacebook() {
	window.open('https://www.facebook.com/sharer/sharer.php?u='.concat(encodeURIComponent(location.href)), '_blank', 'toolbar=0,status=0');
}

function shareGoogle() {
	window.open('https://plus.google.com/share?url='.concat(encodeURIComponent(location.href)), '_blank', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

function shareLine() {
	window.open('https://line.naver.jp/R/msg/text/?u='.concat(encodeURIComponent(location.href)));
}

function shareTwitter() {
	window.open('https://twitter.com/home/?status='.concat(encodeURIComponent(document.title)).concat(' ').concat(encodeURIComponent(location.href)));
}

if (!String.prototype.startsWith) {
	String.prototype.startsWith = function (searchString, position) {
		position = position || 0;
		return this.indexOf(searchString, position) === position;
	};
}

String.prototype.replaceAll = function (search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

function delayHideLoading() {
	setTimeout(function () {
		$('body').loading('stop');
	}, 500);
}

function showError(id, message) {
	var ele = $(id).closest('.form-group').addClass('has-error');
	ele.find('.help-block').text(message);
	$([document.documentElement, document.body]).animate({
		scrollTop: ele.offset().top
	}, 500);
}
function validateUserId(id) {
	tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
	A1 = new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
	A2 = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
	Mx = new Array(9, 8, 7, 6, 5, 4, 3, 2, 1, 1);

	if (id.length != 10) return false;
	i = tab.indexOf(id.charAt(0));
	if (i == -1) return false;
	sum = A1[i] + A2[i] * 9;

	for (i = 1; i < 10; i++) {
		v = parseInt(id.charAt(i));
		if (isNaN(v)) return false;
		sum = sum + v * Mx[i];
	}
	if (sum % 10 != 0) return false;
	return true;
}
function validateForeignUserId(id) {
	var idHeader = "ABCDEFGHJKLMNPQRSTUVXYWZIO"; //按照轉換後權數的大小進行排序
	//這邊把身分證字號轉換成準備要對應的
	id = (idHeader.indexOf(id.substring(0, 1)) + 10) +
		'' + ((idHeader.indexOf(id.substr(1, 1)) + 10) % 10) + '' + id.substr(2, 8);
	//開始進行身分證數字的相乘與累加，依照順序乘上1987654321

	s = parseInt(id.substr(0, 1)) +
		parseInt(id.substr(1, 1)) * 9 +
		parseInt(id.substr(2, 1)) * 8 +
		parseInt(id.substr(3, 1)) * 7 +
		parseInt(id.substr(4, 1)) * 6 +
		parseInt(id.substr(5, 1)) * 5 +
		parseInt(id.substr(6, 1)) * 4 +
		parseInt(id.substr(7, 1)) * 3 +
		parseInt(id.substr(8, 1)) * 2 +
		parseInt(id.substr(9, 1));

	//檢查號碼 = 10 - 相乘後個位數相加總和之尾數。
	checkNum = parseInt(id.substr(10, 1));
	//模數 - 總和/模數(10)之餘數若等於第九碼的檢查碼，則驗證成功
	///若餘數為0，檢查碼就是0
	if ((s % 10) == 0 || (10 - s % 10) == checkNum) {
		return true;
	}
	else {
		return false;
	}
}
function validateCompanyId(taxId) {
	var invalidList = "00000000,11111111";
	if (/^\d{8}$/.test(taxId) == false || invalidList.indexOf(taxId) != -1) {
		return false;
	}

	var validateOperator = [1, 2, 1, 2, 1, 2, 4, 1],
		sum = 0,
		calculate = function (product) {
			var ones = product % 10,
				tens = (product - ones) / 10;
			return ones + tens;
		};
	for (var i = 0; i < validateOperator.length; i++) {
		sum += calculate(taxId[i] * validateOperator[i]);
	}

	return sum % 10 == 0 || (taxId[6] == "7" && (sum + 1) % 10 == 0);
};