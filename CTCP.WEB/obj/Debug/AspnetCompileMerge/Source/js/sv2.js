﻿
// JScript 檔
function changeEnd(sel)
{ }
// 當Ajax:TabControl改變顯示的TabPanel時，就用隱藏欄位記錄下來。
//function TabAreaActiveTabChanged(sender, e)
//{
//if(sender == null) return;

//    var activeTabIndex = sender._activeTabIndex;
//    document.getElementById('aspHiddenFieldTabControlIndex').value = activeTabIndex;
//}
//function Tab2AreaActiveTabChanged(sender, e)
//{
//if(sender == null) return;

//    var activeTabIndex = sender._activeTabIndex;
//    var txt = document.getElementById('aspHiddenFieldTabControlIndexValue').value;
//    var index = parseInt(document.getElementById('aspHiddenFieldTabControlIndex2').value);
//    if(activeTabIndex==2 && txt == "DEBT" )
//    {
//        showJSMessageFor_aspSysMessageMaster(msgDebt);
//        sender.set_activeTabIndex(index);

//    }
//    else if(activeTabIndex==1 && txt == "GRID" )
//    {        
//        showJSMessageFor_aspSysMessageMaster(msgGrid);
//        sender.set_activeTabIndex(index);      
//    }
//    document.getElementById('aspHiddenFieldTabControlIndex2').value = sender._activeTabIndex;

//}


function IsEmpty(objText) {

    var blSymbol = false;
    //	var strEmpty = objText.value;
    var strEmpty = objText;
    if (strEmpty.length == 0 || strEmpty == "0") {
        blSymbol = true;
    }
    else {
        var j = 0;
        for (var i = 0; i < strEmpty.length; i++) {
            if (strEmpty.charAt(i) == " ") {
                //showJSMessageFor_aspSysMessageMaster("1111");
                j = j + 1;
            }
        }
        if (strEmpty.length == j) {
            blSymbol = true;
        }
    }
    return blSymbol;
}


function showRecor(b) {

    //催收主畫面IR 表頭會鎖定 Added By Weilin 2010/11/04 Start
    InitRecPos();
    //催收主畫面IR 表頭會鎖定 Added By Weilin 2010/11/04 End

    var divR = document.getElementById('divREC');
    if (divR != null)
        if (divR.style.display == 'block') {
            divR.style.display = 'none';
            var dc = document.getElementById("divRecord");
            var djj1 = document.getElementById('aspgirdRecord_dvBody');
            var bheight = djj1.offsetHeight;

            if (bheight == 285) {
                djj1.style.height = entryZoomHeight1;
            }
            else {
                djj1.style.height = 160;
            }
            //djj1.style.height = bheight + 38;
        }
        else {
            divR.style.display = 'block';
            var dc = document.getElementById("divRecord");
            var djj1 = document.getElementById('aspgirdRecord_dvBody');
            var bheight = djj1.offsetHeight;
            var dheight = dc.offsetHeight;
            if (bheight == 330) {
                djj1.style.height = entryZoomHeight1 - 45;
                //dc.style.height = dheight - 38 ;
            }
            else {
                djj1.style.height = entryZoomHeight2 - 45;
                //dc.style.height = dheight - 38 ;
            }

        }

}


function showClick(rb, num) {
    var dAcc = document.getElementById('divACC');
    if (dAcc != null) {
        dAcc.style.display = 'none';
        if (num == 1) {
            if (rb.checked)
            { dAcc.style.display = 'block' }
        }
    }
}

function checkinboundid() {
    var item = document.getElementById('aspTextBoxIDIn');
    if (CheckID(item))
        return true;
    else {
        showJSMessageFor_aspSysMessageMaster(msgIdNoError);
        item.select();
        return false;
    }
}


function checkEn() {
    var obj1 = document.getElementById('aspRadioButtonListFChoice');
    var obj2 = document.getElementById('aspDropDownListDdlAccCode');
    var obj3 = document.getElementById('aspDropDownListDdlMidAccCode');
    var obj4 = document.getElementById('aspDropDownListDdlDetlAccCode');
    var obj5 = document.getElementById('aspAutoComboTextActConPeo');
    var obj6 = document.getElementById('HiddenFieldConnTypeId');
    var obj7 = document.getElementById('aspCheckBoxFCTypeAddr');
    var obj8 = document.getElementById('aspCheckBoxFCTypeTel');
    //PTP
    var obj9 = document.getElementById('aspTextBoxDtPay1');
    var obj10 = document.getElementById('aspTextBoxTxtPtpAmt');
    var obj11 = document.getElementById('aspDropDownListDdlPtpPamttype');
    //CallBack
    var obj12 = document.getElementById('aspTextBoxDtPay2');
    var obj13 = document.getElementById('aspTextBoxTxtCBTime');
    var today = new Date();
    var month = today.getMonth() + 1;
    var H = today.getHours().toString();
    var M = today.getMinutes().toString();
    if (today.getHours().toString().length == 1)
        H = "0" + today.getHours().toString();
    if (today.getMinutes().toString().length == 1)
        M = "0" + today.getMinutes().toString();
    var time = H + ":" + M;
    today = today.getYear() + "/" + month + "/" + today.getDate();

    var todayAfter1y = new Date();
    todayAfter1y = (parseInt(todayAfter1y.getYear(), 10) + 1) + "/" + month + "/" + todayAfter1y.getDate();


    if (obj1 != null && !obj1.disabled && obj1.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode0); return false;
    }
    if (obj5 != null && !obj5.disabled && obj5.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectTarget); return false;
    }
    if ((obj7 != null && !obj7.disabled && obj8 != null && !obj8.disabled && obj6 != null && obj6.value == "") || ((obj7 != null && !obj7.disabled && !obj7.checked) && (obj8 != null && !obj8.disabled && !obj8.checked))) {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectType); return false;
    }

    if (obj2 != null && obj2.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode1); return false;
    }
    if (obj3 != null && obj3.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode2); return false;
    }
    if (obj4 != null && obj4.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode3); return false;
    }
    var check_ptp = false;
    //PTP
    if (obj9 != null && trim(obj9.value) != "") {
        if (!isDate(obj9.value, "/"))
        { showJSMessageFor_aspSysMessageMaster(msgPTP1); return false; }
        else {
            if (Date.parse(obj9.value) < Date.parse(today)) {
                showJSMessageFor_aspSysMessageMaster(msgPTP6); return false;
            }
            if (Date.parse(obj9.value) > Date.parse(todayAfter1y)) {
                showJSMessageFor_aspSysMessageMaster(msgPTP7); return false;
            }
        }
        check_ptp = true;
    }
    if (obj10 != null && trim(obj10.value) != "") {
        if (!IsNumber(obj10, "int"))
        { showJSMessageFor_aspSysMessageMaster(msgPTP2); return false; }
        check_ptp = true;
    }
    if (obj11 != null && obj11.value != "0") {
        check_ptp = true;
    }
    if (check_ptp) {
        if (obj9 != null && trim(obj9.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgPTP4); return false;
        }
        if (obj10 != null && trim(obj10.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgPTP5); return false;
        }
        if (obj11 != null && obj11.value == "0") {
            showJSMessageFor_aspSysMessageMaster(msgPTP3); return false;
        }
    }
    //CallBack
    var check_CallBack = false;
    if (obj12 != null && trim(obj12.value) != "") {
        if (!isDate(obj12.value, "/"))
        { showJSMessageFor_aspSysMessageMaster(msgCallBack1); return false; }
        else {
            if (Date.parse(obj12.value) < Date.parse(today)) {
                showJSMessageFor_aspSysMessageMaster(msgCallBack6); return false;
            }
            if (Date.parse(obj12.value) > Date.parse(todayAfter1y)) {
                showJSMessageFor_aspSysMessageMaster(msgCallBack7); return false;
            }
        }
        check_CallBack = true;
    }
    if (obj13 != null && trim(obj13.value) != "") {
        if (!isTime(obj13.value))
        { showJSMessageFor_aspSysMessageMaster(msgCallBack2); return false; }
        else {
            var _t1 = obj13.value.split(":");
            var regexs = /^(-?[0-9]+[0-9]*|0|00)$/;
            if (!(parseInt(_t1[0], 10) <= 22 && parseInt(_t1[1], 10) >= 0) || !(parseInt(_t1[1], 10) <= 59 && parseInt(_t1[0], 10) >= 7)) {
                showJSMessageFor_aspSysMessageMaster(msgCallBack5); return false;
            }
        }
        check_CallBack = true;
    }
    if (check_CallBack) {
        if (obj12 != null && trim(obj12.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgCallBack3); return false;
        }
        if (obj13 != null && trim(obj13.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgCallBack4); return false;
        }
        if ((Date.parse(obj12.value) == Date.parse(today)) && obj13.value < time) {
            showJSMessageFor_aspSysMessageMaster(msgCallBack6); return false;
        }
    }
    var btn = document.getElementById('aspButtonBtn1');
    btn.disabled = true;
    __doPostBack('aspButtonBtn1', '');
    //    return true;
}

function isTime(t1) {
    var _t1 = t1.split(":");
    var regexs = /^(-?[0-9]+[0-9]*|0|00)$/;
    if (trim(t1) != '' && (_t1.length != 2 || _t1[0].length != 2 || _t1[1].length != 2 || !regexs.test(_t1[0]) || !regexs.test(_t1[1]) || !(parseInt(_t1[0], 10) <= 23 && parseInt(_t1[1], 10) >= 0) || !(parseInt(_t1[1], 10) <= 59 && parseInt(_t1[0], 10) >= 0))) {
        return false;
    }
    return true;
}
function reEndReason(sel) {
    if (document.getElementById('aspDropDownListDdlEndAcc') == null)
        return false;
    var txt = sel.options(sel.selectedIndex).value;
    var txt1 = sel.options(sel.selectedIndex).text;
    document.getElementById('aspHiddenField4').value = txt;
    document.getElementById('aspHiddenField5').value = txt1;
    switch (txt.split(",")[1]) {
        case "Y":
            document.getElementById('aspDropDownListDdlEndAcc').disabled = "disabled";
            break;
        case "N":
            document.getElementById('aspDropDownListDdlEndAcc').disabled = false;
            break;
    }
}

function checkEnd() {

    var seAcc = document.getElementById('aspDropDownListDdlEndAcc')
    var seEnd = document.getElementById('aspDropDownListDdlEnd')
    var seReson = document.getElementById('aspSelectDdlt')
    if (seAcc == null)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectcAcctStop); return false; }
    if (!seAcc.disabled) {
        var len = seAcc.length;
        var checked = false;
        for (i = 0; i < seAcc.all.tags('input').length; i++) {
            if (seAcc.all.tags('input')[i].type == 'checkbox' && seAcc.all.tags('input')[i].checked) {
                checked = true;
                break;
            }
        }

        if (!checked)
        { showJSMessageFor_aspSysMessageMaster(msgSv1SelectAccount); return false; }
    }
    if (seEnd.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectStop); return false; }
    if (seReson.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectStopCause); return false; }

    var btn = document.getElementById('aspButton1');
    btn.disabled = true;
    __doPostBack('aspButton1', '');
    //   return true;
}
function checkLega() {
    var seAcc = document.getElementById('CheckBoxList1')
    var seLega = document.getElementById('CheckBoxList2')
    var seReson = document.getElementById('aspSelectDdlt')
    if (!seAcc)
        return false;
    var len = seAcc.length;
    var checked = false;
    for (i = 0; i < seAcc.all.tags('input').length; i++) {
        if (seAcc.all.tags('input')[i].type == 'checkbox' && seAcc.all.tags('input')[i].checked) {
            checked = true;
            break;
        }
    }

    if (!checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectAccount); return false; }
    checked = false;
    var len = seLega.length;
    for (i = 0; i < seLega.all.tags('input').length; i++) {
        if (seLega.all.tags('input')[i].type == 'checkbox' && seLega.all.tags('input')[i].checked) {
            checked = true;
            break;
        }
    }

    if (!checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectLega); return false; }

    return true;
}
function checkRecord() {

    //催收主畫面IR 表頭會鎖定 Added By Weilin 2010/11/03 Start
    InitRecPos();
    //催收主畫面IR 表頭會鎖定 Added By Weilin 2010/11/03 End

    var count = 0;
    var se1 = document.getElementById('aspCheckBoxCbNote');
    var se2 = document.getElementById('aspCheckBoxCbMess');
    var se3 = document.getElementById('aspCheckBoxCbLetter');
    var se4 = document.getElementById('aspCheckBoxCbFind');
    var se5 = document.getElementById('aspCheckBoxCbVisit');
    var se6 = document.getElementById('aspCheckBoxCbLegaProc');
    var se7 = document.getElementById('aspCheckBoxCbPP');
    var se8 = document.getElementById('aspCheckBoxCbCard');
    var se9 = document.getElementById('aspCheckBoxCbStopCall');
    var se10 = document.getElementById('aspCheckBoxCbAMNote');
    var se11 = document.getElementById('aspCheckBoxCbOut');
    var checked = false;
    if (se1 && se1.checked) {
        checked = true;
        count = count + 1;
    }
    if (se2 && se2.checked) {
        checked = true;
        count = count + 1;
    }
    if (se3 && se3.checked) {
        checked = true;
        count = count + 1;
    }
    if (se4 && se4.checked) {
        checked = true;
        count = count + 1;
    }
    if (se5 && se5.checked) {
        checked = true;
        count = count + 1;
    }
    if (se6 && se6.checked) {
        checked = true;
        count = count + 1;
    }
    if (se7 && se7.checked) {
        checked = true;
        count = count + 1;
    }
    if (se8 && se8.checked) {
        checked = true;
        count = count + 1;
    }
    if (se9 && se9.checked) {
        checked = true;
        count = count + 1;
    }
    if (se10 && se10.checked) {
        checked = true;
        count = count + 1;
    }
    if (se11 && se11.checked) {
        checked = true;
        count = count + 1;
    }
    if (se1 && se1.checked && se10 && se10.checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectRecord1); return false; }
    if (count > 3)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectRecord2); return false; }
    if (!checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectRecord); return false; }

    return true;
}
function checkPtP() {
    var se = document.getElementById('aspDropDownListDdlAcc')
    if (se.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectAccount); return false; }
    se = document.getElementById('aspDropDownListDdlPtpPamttype')
    if (se.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectFashion1); return false; }
    if (IsEmpty(document.getElementById('aspTextBoxDtPay7')))
    { showJSMessageFor_aspSysMessageMaster(msgSv1FillInDate); return false; }
    if (IsEmpty(document.getElementById('aspTextBoxTxtPtpTime')))
    { showJSMessageFor_aspSysMessageMaster(msgSv1FillInTime); return false; }
    if (IsEmpty(document.getElementById('aspTextBoxTxtPtpAmt')))
    { showJSMessageFor_aspSysMessageMaster(msgSv1FillInMoney); return false; }
    return true;
}

function getS(con, ind)
{ document.getElementById(con).options[ind].selected = true; }

function showDataDetl() {
    var d = document.getElementById('divDataDetl')
    if (d.style.display == 'none')
    { d.style.display = 'block'; }
    else
    { d.style.display = 'none'; }

}

function rTime(txt) {
    var str = txt.value
    if (str.length == 4) {
        txt.value = str.substring(0, 2) + ':' + str.substring(2, 4)
    }
}

function EndRequestHandler() {
    var rtn = true;
    try {
        rtn = WriteToFile('HiddenFieldCTI=' + document.getElementById('HiddenFieldCTI').value);
        if (document.getElementById('HiddenFieldCTI').value == "send") {

            var b1 = document.getElementById('HiddenFieldb1').value
            var b2 = document.getElementById('HiddenFieldb2').value
            var b3 = document.getElementById('HiddenFieldb3').value
            var b4 = document.getElementById('HiddenFieldb4').value
            var b5 = document.getElementById('HiddenFieldb5').value

            var b7 = document.getElementById('HiddenFieldb7').value
            var b8 = document.getElementById('HiddenFieldb8').value
            var b9 = document.getElementById('HiddenFieldb9').value
            var b10 = document.getElementById('HiddenFieldb10').value
            var b11 = document.getElementById('HiddenFieldb11').value

            var b6 = document.getElementById('HiddenFieldb6').value
            var b12 = document.getElementById('HiddenFieldb12').value
            var b13 = document.getElementById('HiddenFieldb13').value
            var b14 = document.getElementById('HiddenFieldb14').value
            document.getElementById('HiddenFieldCTI').value = "";

            rtn = WriteToFile(b1 + '-' + b2 + '-' + b3 + '-' + b4 + '-' + b5 + '-' + b7 + '-' + b8 + '-' + b9 + '-' + b10 + '-' + b11 + '-' + b6 + '-' + b12 + '-' + b13 + '-' + b14 + " : InsertContactLog finish!");
            window.external.InsertContactLog(b1, b2, b3, b4, b5, b7, b8, b9, b10, b11, b6, b12, b13, b14);
            //showJSMessageFor_aspSysMessageMaster(msgCTIS3);
            //showJSMessageFor_aspSysMessageMaster(msgCTIS1);
        }
    }
    catch (err) {
        //        try {
        rtn = WriteToFile('InsertContactLog Error:' + err.message);
        window.external.InsertContactLog(b1, b2, b3, b4, b5, b7, b8, b9, b10, b11, b6, b12, b13, b14);
        return false;
        //      showJSMessageFor_aspSysMessageMaster(msgCTIS2);
        //      showJSMessageFor_aspSysMessageMaster(msgCTIS1);
        //        } catch (err) {
        //            rtn = WriteToFile('InsertContactLog Error2:' + err.message);
        //        }
    }
}

function showSV() {

    var txt = document.getElementById('aspHiddenFieldShowSV1');
    var count = document.getElementById('aspHiddenFieldShowCase');
    var txt1 = document.getElementById('aspHiddenFieldShowSV1ShareWork');
    var txtBound = document.getElementById('aspHiddenFieldShowSV1ShareWorkBound');
    var message = '切換ID將會關閉所有的作業\r並進入' + txt.value + '的案件\r確定切換嗎？';
    if (txtBound && txtBound.value == "Inbound") {
        if (!confirm(message)) {
            return false;
        }
    }
    if (txt.value != "") {
        var head = parent.parent.document.frames.item('FrameHead');
        var custId = head.document.getElementById("hid_CustID");
        var Bound = head.document.getElementById("hid_Bound");
        custId.value = txt.value;
        Bound.value = txtBound.value;
        var txtURL = head.document.getElementById("hid_URL");
        var txtURLNAME = head.document.getElementById("hid_URLNAME");

        head.document.form1.submit();
        //        var URL = "sv1.aspx" ;
        //        var URLNAME = "催收主畫面" ;
        //        txtURL = URL;
        //        txtURLNAME = URLNAME;

        //        var funname = "ChangeID('" + URL + "','" + URLNAME + "')";
        //        parent.execScript(funname);
        //切換工作清單下一筆
        parent.execScript("changeNext('" + txt.value + "','" + count.value + "')");
        txt.value = "";
    }
    else if (txt1.value != "") {
        var head = parent.parent.document.frames.item('FrameHead');
        var custId = head.document.getElementById("hid_CustID");
        var txtURL = head.document.getElementById("hid_URL");
        var txtURLNAME = head.document.getElementById("hid_URLNAME");
        custId.value = txt1.value;
        head.document.form1.submit();
        //        var URL = "sv1.aspx" ;
        //        var URLNAME = "催收主畫面" ;
        //        txtURL = URL;
        //        txtURLNAME = URLNAME;
        //        var funname = "ChangeID('" + URL + "','" + URLNAME + "')";
        //        parent.execScript(funname);
        txt1.value = "";
    }
}

function showSave() {
    //Amy 0805 呆後規格 
    var txt1 = document.getElementById('aspHiddenFieldChoice').value;
    var txt2 = document.getElementById('aspHiddenFieldCode').value;
    var txt3 = document.getElementById('aspHiddenFieldMidCode').value;
    var txt4 = document.getElementById('aspHiddenFieldDetlCode').value;
    //star CR主畫面 增加當日催記可以修改功能.修改日期：2010/08/30 修改人：雷平
    var txt5 = document.getElementById('aspHiddenFieldColtDiryShow').value;
    //end CR主畫面 增加當日催記可以修改功能.修改日期：2010/08/30 修改人：雷平

    var item2 = document.getElementById('aspDropDownListDdlAccCode');
    var item3 = document.getElementById('aspDropDownListDdlMidAccCode');
    var item4 = document.getElementById('aspDropDownListDdlDetlAccCode');
    item2.options.length = 0;
    item2.options[0] = new Option('--請選擇--', 0);
    item2.disabled = true;

    item3.options.length = 0;
    item3.options[0] = new Option('--請選擇--', 0);
    item3.disabled = true;

    item4.options.length = 0;
    item4.options[0] = new Option('--請選擇--', 0);
    item4.disabled = true;
    if (txt1 != "") {
        try {
            changActionCode1(txt1);
            document.getElementById('aspRadioButtonListFChoice').value = txt1;
            document.getElementById('aspRadioButtonListFChoice').disabled = false;

            if (txt2 != "") {
                changActionCode2(txt1 + txt2);
                item2.value = txt2;
                item2.disabled = false;

                if (txt3 != "") {
                    changActionCode3(txt1 + txt2 + txt3);
                    item3.value = txt3;
                    item3.disabled = false;

                    if (txt4 != "") {
                        item4.value = txt4;
                        item4.disabled = false;
                    }
                }
            }

        }
        catch (err)
        { return false; }


    }
    if (item2.value == "0") {
        if (item2.options.length > 1) {
            try {
                item2.selectedIndex = 1;
                document.getElementById('aspHiddenFieldCode').value = item2.value
                txt2 = document.getElementById('aspHiddenFieldCode').value;
                changActionCode2(txt1 + txt2);
                item3.disabled = false;
            }
            catch (err)
            { return false; }
        }
    }
    document.getElementById('aspCheckBoxFCTypeTel').disabled = false;
    document.getElementById('aspCheckBoxFCTypeAddr').disabled = false;
    //把聯絡種類反灰
    if (txt1 == '04' || txt1 == '06' || txt1 == '07' || txt1 == '08' || txt1 == '09') {
        document.getElementById('aspCheckBoxFCTypeTel').disabled = true;
        document.getElementById('aspCheckBoxFCTypeAddr').disabled = true;
    }
    if (item2.options.length < 2)
        item2.disabled = true;
    if (item3.options.length < 2)
        item3.disabled = true;
    if (item4.options.length < 2)
        item4.disabled = true;
    //star CR主畫面 增加當日催記可以修改功能.修改日期：2010/08/30 修改人：雷平
    if (txt5 == '1') {
        document.getElementById('aspCheckBoxFCTypeAddr').disabled = true;
        document.getElementById('aspCheckBoxFCTypeTel').disabled = true;
        document.getElementById('aspRadioButtonListFChoice').disabled = true;
        item2.disabled = true;
        item3.disabled = true;
        item4.disabled = true;
    }
    document.getElementById('aspHiddenFieldColtDiryShow').value = '0'
    //end CR主畫面 增加當日催記可以修改功能.修改日期：2010/08/30 修改人：雷平
    //結退案
    //     var item = document.getElementById('aspTabPanelTPEndCase').innerHTML;
    //     var sel = document.getElementById('aspDropDownListDdlEnd'); 
    //     
    //     if(item!=null && item.disabled==false)
    //     {changeEnd(sel);

    //         var txt = document.getElementById('aspHiddenField4').value;
    //         if(txt!="")
    //            {document.getElementById('aspSelectDdlt').value=txt;
    //            reEndReason(document.getElementById('aspSelectDdlt'));}
    //     }

}

var divExist = false;
//關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 start
var divRelExist = false;
var divDisplay = false;
//關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 end
//Show All CIF Information
function popup(type) {
    //關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 start
    var reltype = document.getElementById("RelatedType").value  //關係別類型
    //關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 end

    //var tabPanel = $("aspTabContainer")
    switch (event.button) {
        case 1:
            //alert("你用滑鼠左鍵！");
            break;
        case 2:
            //alert("你用滑鼠右鍵！");
            break;
        case 4:
            //alert("你用滑鼠中鍵！");
            break;
        default:
            //alert("未知的滑鼠鍵！");
            //判斷是否已有該Popup Div
            //關系人查詢  CR T-D4-201011-013 modified by luyan 2010/12/02 start
            //            if (document.getElementById("cloneId")) {
            //                if (document.getElementById("cloneId").style.visibility == "hidden") {
            //                    hideSelect(type);
            //                    document.getElementById("cloneId").style.visibility = "visible";
            //                    hideShowSelect(false);
            //                } else {
            //                    document.getElementById("cloneId").style.visibility = "hidden";
            //                    hideShowSelect(true);
            //                    return false;
            //                }
            //            }
            if (type == "0" || reltype == "0") {
                //關系人查詢  CR T-D4-201011-013 modified by luyan 2010/12/02 end
                var aTitle = new Array("基本資料", "電話", "地址");
                if (!divExist) {
                    ClickDIVCIF();
                    //    		  var tabCont = $(aspTabContainer); //要複製的頁籤
                    var tabCont = document.getElementById("tabCont1")
                    var clone = tabCont.cloneNode(true); //複製頁籤

                    var element = document.createElement("DIV"); //建立一個新的DIV
                    element.id = "cloneId";
                    document.body.appendChild(element); //放入DIV
                    element.style.position = "absolute";
                    element.style.width = "98%";
                    element.style.height = "500px";
                    element.style.top = "2px";
                    element.style.left = "2px";
                    element.style.padding = "10px";
                    element.style.overflowY = "auto";
                    element.style.overflowX = "hidden";
                    element.innerHTML = ""
                    element.ondblclick = function () { //按兩下關掉
                        document.getElementById("cloneId").style.visibility = "hidden";
                        //關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 start
                        divDisplay = false;
                        //關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 end
                        hideShowSelect(true);
                        return false;
                    }
                    var index = 0;
                    //element.appendChild(clone);
                    var divs = clone.getElementsByTagName("DIV"); //抓取所有複製頁籤內的DIV
                    var j = 0;
                    for (var i = 0; i < divs.length; i++) {
                        if (divs[i].className == "BlockY") { //抓取特定的DIV
                            divs[i].style.height = "auto";
                            divs[i].style.display = 'block';
                            element.innerHTML += aTitle[j]; //放入標題
                            j++;
                            var ttt = divs[i].getElementsByTagName('table'); //抓取DIV內的TABLE
                            ttt[0].id = 'grid' + index; //GridView的ID會被鬼隱，所以另給ID
                            index++;
                            element.appendChild(divs[i].cloneNode(true)); //複製Div
                            element.innerHTML += "<br>";

                        }
                    }
                    element.style.backgroundColor = "#BDCFBD";  //99cc99,D8F3C9,CCCC99 
                    element.style.border = "5px outset";
                    element.zIndex = 100;
                    divExist = true;
                    hideShowSelect(false);
                }

                //關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 start
            } else {
                var aTitle = new Array("電話", "地址");
                if (!divRelExist) {
                    ClickDIVCIF2();
                    //    		  var tabCont = $(aspTabContainer); //要複製的頁籤
                    var tabCont = document.getElementById("tabCont2")
                    var clone = tabCont.cloneNode(true); //複製頁籤

                    var element = document.createElement("DIV"); //建立一個新的DIV
                    element.id = "RelId";
                    document.body.appendChild(element); //放入DIV
                    element.style.position = "absolute";
                    element.style.width = "95%";
                    element.style.height = "500px";
                    element.style.top = "2px";
                    element.style.left = "2px";
                    element.style.padding = "10px";
                    element.style.overflowY = "auto";
                    element.style.overflowX = "hidden";
                    element.innerHTML = ""
                    element.ondblclick = function () { //按兩下關掉
                        document.getElementById("RelId").style.visibility = "hidden";
                        divDisplay = false;
                        hideShowSelect(true);
                        return false;
                    }
                    var index = 0;
                    //element.appendChild(clone);
                    var divs = clone.getElementsByTagName("DIV"); //抓取所有複製頁籤內的DIV
                    var j = 0;
                    for (var i = 0; i < divs.length; i++) {
                        if (divs[i].className == "BlockY") { //抓取特定的DIV
                            divs[i].style.height = "auto";
                            divs[i].style.display = 'block';
                            element.innerHTML += aTitle[j]; //放入標題
                            j++;
                            var ttt = divs[i].getElementsByTagName('table'); //抓取DIV內的TABLE
                            ttt[0].id = 'gridRel' + index; //GridView的ID會被鬼隱，所以另給ID
                            index++;
                            element.appendChild(divs[i].cloneNode(true)); //複製Div
                            element.innerHTML += "<br>";

                        }
                    }
                    element.style.backgroundColor = "#BDCFBD";  //99cc99,D8F3C9,CCCC99 
                    element.style.border = "5px outset";
                    element.zIndex = 101;
                    divRelExist = true;
                    hideShowSelect(false);
                }
            }

            if (divDisplay) {
                if (document.getElementById("cloneId")) {
                    document.getElementById("cloneId").style.visibility = "hidden";
                }

                if (document.getElementById("RelId")) {
                    document.getElementById("RelId").style.visibility = "hidden";
                }
                hideShowSelect(true);
                divDisplay = false;
                return false;
            } else {
                if (type == "0" || reltype == "0") {//顯示本人
                    if (document.getElementById("cloneId")) {
                        hideSelect(type);
                        document.getElementById("cloneId").style.visibility = "visible";
                        hideShowSelect(false);
                    }
                } else {//顯示關係人
                    if (document.getElementById("RelId")) {
                        hideSelect2(type);
                        document.getElementById("RelId").style.visibility = "visible";
                        hideShowSelect(false);
                    }
                }

                divDisplay = true;
            }
            //關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 end

            break;
    }
    //關系人查詢  CR T-D4-201011-013 delete by luyan 2010/12/02 start
    //hideSelect(type);
    //關系人查詢  CR T-D4-201011-013 delete by luyan 2010/12/02 end
}

//關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 start
function hideSelect2(type) {
    //判斷RadioButton是否顯示
    var grid1 = "gridRel0";
    var grid2 = "gridRel1";
    var txt1 = document.getElementById(grid1);
    var txt2 = document.getElementById(grid2);
    if (txt1 != null) {
        var dbl = txt1.cells[1].children[0].firstChild.cells.length / 10;  //計算RadioButton在Table的位置
        for (var i = 0; i < dbl; i++) {
            var index = 10 * i;
            switch (type) {
                case "0":  //全部隱藏
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "1": //電話
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'block';
                    break;
                case "2": //地址
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
            }
        }
    }
    if (txt2 != null) {
        var dbl = txt2.cells[1].children[0].firstChild.cells.length / 10;   //計算RadioButton在Table的位置
        for (var i = 0; i < dbl; i++) {
            var index = 10 * i;
            switch (type) {
                case "0": //全部隱藏
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "1": //電話
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "2": //地址
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'block';
                    break;
            }
        }
    }
}
//關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 end

function hideSelect(type) {
    //判斷RadioButton是否顯示
    var grid1 = "grid1";
    var grid2 = "grid2";
    var txt1 = document.getElementById(grid1);
    var txt2 = document.getElementById(grid2);
    if (txt1 != null) {
        var dbl = txt1.cells[1].children[0].firstChild.cells.length / 13;  //計算RadioButton在Table的位置
        for (var i = 0; i < dbl; i++) {
            var index = 13 * i;
            switch (type) {
                case "0":  //全部隱藏
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "1": //電話
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'block';
                    break;
                case "2": //地址
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
            }
        }
    }
    if (txt2 != null) {
        var dbl = txt2.cells[1].children[0].firstChild.cells.length / 13;   //計算RadioButton在Table的位置
        for (var i = 0; i < dbl; i++) {
            var index = 13 * i;
            switch (type) {
                case "0": //全部隱藏
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "1": //電話
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "2": //地址
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'block';
                    break;
            }
        }
    }
}

function ClickDIVCIF() {
    try {
        var item = document.getElementById('HiddenFieldConnTypeId');
        var aft_onclick_event = "";
        //        var onclick_event="aspgirdTel_clearRadChoice();";
        //        onclick_event += "aspgirdTel_ShareGridView_ctl02_RadioChoice.checked=true;";
        //        onclick_event += "GirdViewchangeColorCheckBox(this);";
        //        aft_onclick_event = "document.getElementById('aspgirdTel_ShareGridView_ctl02_RadioChoice').checked=true; "
        //        aft_onclick_event = "document.getElementById('HiddenFieldConnTypeId').value='0';";
        //關系人查詢  CR T-D4-201011-013 modified by luyan 2010/12/02 start
        //aft_onclick_event = "if (document.getElementById('cloneId')){document.getElementById('cloneId').style.visibility='hidden'; hideShowSelect(true);}  ";
        aft_onclick_event = "if (document.getElementById('cloneId')){document.getElementById('cloneId').style.visibility='hidden'; divDisplay = false; hideShowSelect(true);}  ";
        //關系人查詢  CR T-D4-201011-013 modified by luyan 2010/12/02 end
        //判斷RadioButton是否顯示
        var grid1 = "aspgirdTel_ShareGridView";
        var grid2 = "aspgridAdd_ShareGridView";
        var txt1 = document.getElementById(grid1);
        var txt2 = document.getElementById(grid2);
        if (txt1 != null) {
            var dbl = txt1.rows.length;  //計算RadioButton在Table的位置
            for (var i = 1; i < dbl; i++) {
                var id = txt1.rows(i).cells(0).getElementsByTagName('input')(0).id;
                var onclick_event = txt1.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value;
                onclick_event = onclick_event + "document.getElementById('" + id + "').checked=true; document.getElementById('HiddenFieldConnTypeId').value='" + (i - 1) + "';" + aft_onclick_event;
                txt1.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value = onclick_event;
            }
        }
        if (txt2 != null) {
            var dbl = txt2.rows.length;  //計算RadioButton在Table的位置
            for (var i = 1; i < dbl; i++) {
                var id = txt2.rows(i).cells(0).getElementsByTagName('input')(0).id;
                var onclick_event = txt2.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value;
                onclick_event = onclick_event + "document.getElementById('" + id + "').checked=true; document.getElementById('HiddenFieldConnTypeId').value='" + (i - 1) + "';" + aft_onclick_event;
                txt2.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value = onclick_event;
            }
        }
    } catch (err) { return false; }
}

//關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 start
function ClickDIVCIF2() {
    try {
        var aft_onclick_event2 = "";
        aft_onclick_event2 = "if (document.getElementById('RelId')){document.getElementById('RelId').style.visibility='hidden'; divDisplay = false; hideShowSelect(true);}  ";
        //判斷RadioButton是否顯示
        var gridRel1 = "aspgirdTel2_ShareGridView";
        var gridRel2 = "aspgridAdd2_ShareGridView";
        var txtRel1 = document.getElementById(gridRel1);
        var txtRel2 = document.getElementById(gridRel2);
        if (txtRel1 != null) {
            var dbl = txtRel1.rows.length;  //計算RadioButton在Table的位置
            for (var i = 1; i < dbl; i++) {
                var id = txtRel1.rows(i).cells(0).getElementsByTagName('input')(0).id;
                var onclick_event = txtRel1.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value;
                onclick_event = onclick_event + "document.getElementById('" + id + "').checked=true; document.getElementById('HiddenFieldConnTypeId').value='" + (i - 1) + "';" + aft_onclick_event2;
                txtRel1.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value = onclick_event;
            }
        }
        if (txtRel2 != null) {
            var dbl = txtRel2.rows.length;  //計算RadioButton在Table的位置
            for (var i = 1; i < dbl; i++) {
                var id = txtRel2.rows(i).cells(0).getElementsByTagName('input')(0).id;
                var onclick_event = txtRel2.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value;
                onclick_event = onclick_event + "document.getElementById('" + id + "').checked=true; document.getElementById('HiddenFieldConnTypeId').value='" + (i - 1) + "';" + aft_onclick_event2;
                txtRel2.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value = onclick_event;
            }
        }
    } catch (err) { return false; }
}
//關系人查詢  CR T-D4-201011-013 add by luyan 2010/12/02 end

var div2Exist = false;
//Show All 帳務 Information
function popup2() {
    //判斷是否已有該Popup Div
    if (document.getElementById("clone2Id")) {
        if (document.getElementById("clone2Id").style.visibility == "hidden") {
            document.getElementById("clone2Id").style.visibility = "visible";
            hideShowSelect(false);
        } else {
            document.getElementById("clone2Id").style.visibility = "hidden";
            hideShowSelect(true);
            return false;
        }
    }
    var aTitle = new Array("綜合帳務", "延滯帳務", "呆帳帳務");
    if (!div2Exist) {
        var tabCont = $(aspTabContainer2);
        var clone = tabCont.cloneNode(true);
        var element = document.createElement("DIV");
        element.id = "clone2Id";
        document.body.appendChild(element);
        element.style.position = "absolute";
        element.style.width = "97%";
        switch (screen.width) {
            case 1024:
                element.style.width = "93%";
        }
        element.style.height = "auto";
        element.style.top = "2px";
        element.style.left = "2px";
        element.style.padding = "10px"
        element.innerHTML = ""
        element.ondblclick = function () {
            document.getElementById("clone2Id").style.visibility = "hidden";
            hideShowSelect(true);
            return false;
        }
        var links = clone.getElementsByTagName("A");
        for (var i = 0; i < links.length; i++) {
            links[i].disabled = true;
        }
        var divs = clone.getElementsByTagName("DIV");
        var j = 0;
        for (var i = 0; i < divs.length; i++) {
            if (divs[i].className == "BlockXY") {
                //alert(j+"|"+i);  
                if (document.getElementById('aspHiddenFieldTabControlIndexValue').value == "GRID" && (j == 1 || j == 0)) {  //NO呆前
                    j++
                    continue;
                }
                if (document.getElementById('aspHiddenFieldTabControlIndexValue').value == "DEBT" && j == 2)  //NO呆帳
                    continue;
                divs[i].style.height = "auto";
                divs[i].style.overflowX = "auto";
                element.innerHTML += aTitle[j];

                j++;
                element.appendChild(divs[i].cloneNode(true));
                element.innerHTML += "<br>";
            }
        }
        element.style.backgroundColor = "#BDCFBD";  //99cc99,D8F3C9,CCCC99 
        element.style.border = "5px outset";
        element.zIndex = 100;
        div2Exist = true;
        hideShowSelect(false);
    }
}


function hideShowSelect(swtch) {
    //    var tabCont = $(aspTabContainer1);
    //    var tabCont = document.getElementById("tabCont1");
    //    var sels = tabCont.getElementsByTagName("select");
    var sels = document.getElementsByTagName("select");
    var j = 0;
    for (var i = 0; i < sels.length; i++) {
        if (swtch) {
            sels[i].style.visibility = "visible";
        } else {
            sels[i].style.visibility = "hidden";
        }
    }
}
/* star 催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
//var entryZoomHeight = "393";
//var entryNormalHeight = "225";

//var entryZoomHeight1 = "340";
//var entryZoomHeight2 = "173";
/* star 催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
var entryZoomHeight = "378";
var entryNormalHeight = "210";

var entryZoomHeight1 = "330";
var entryZoomHeight2 = "160";
var entryZoomHeight3 = "378";
var entryZoomHeight4 = "126";
var multWidth = 0.945;
switch (screen.width) {
    case 1024:
        multWidth = 0.93;
}
var entryZoomWidth = parent.document.body.clientWidth * multWidth;


function showDb() {
    var db = document.getElementById("divENTRY");
    var dc = document.getElementById("divRecord");
    var dd = document.getElementById("divCSI");
    var de = document.getElementById("divGura");/* modify by 徐蕓 保證債務人頁簽*/
    var div12 = document.getElementById("div12")
    var df = document.getElementById("divJCICQuery");
    var dg = document.getElementById("divFinishCase");
    var dh = document.getElementById("divLA");
    var di = document.getElementById("divJCICApply");
    var dj = document.getElementById("divsend0");
    var dk = document.getElementById("divsend1");
    var dl = document.getElementById("divsend2");
    var dbtxt = document.getElementById("aspHiddenFieldShowDb").value;
        var oDiv = document.getElementById("divSrch");
    var djj = document.getElementById('divRECG');
    /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 Start */
    var djj1 = document.getElementById('aspgirdRecord_dvBody');
    /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 End */
    /* star 催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
    var dpb = document.getElementById("divPTPCallBack");
    var dgpd = document.getElementById("divgridPTPCallBack");
    /* end  催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
    var djinbound = document.getElementById('divInbound');
    if (oDiv.style.display == '' || oDiv.style.display == 'block') {
        oDiv.style.display = 'none';
        document.getElementById("aspHiddenFieldShowDb").value = 'none';
        if (db != null) {
            db.style.height = entryZoomHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryZoomHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryZoomHeight;
            dd.style.width = entryZoomWidth;
        }    
        if (de != null) {
            de.style.height = entryZoomHeight;
            de.style.width = entryZoomWidth;
        }   
         if (div12 != null) {
            div12.style.height = entryZoomHeight;
            div12.style.width = entryZoomWidth;
        }   
        
        if (df != null) {
            df.style.height = entryZoomHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryZoomHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryZoomHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryZoomHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryZoomHeight;
            dj.style.width = entryZoomWidth;
        }
        if (dk != null) {
            dk.style.height = entryZoomHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryZoomHeight;
            dl.style.width = entryZoomWidth;
        }
        if (djj != null)
        { djj.style.height = entryZoomHeight1; }
        /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 Start */
        if (djj1 != null) {
            // 催收主畫 IR Added By Weilin 2010/11/03 Start
            djj1.scrollTop = 0;
            // 催收主畫 IR Added By Weilin 2010/11/03 End

            var divR = document.getElementById('divREC');
            var bheight = djj1.offsetHeight;
            if (divR.style.display == 'block') {

                if (bheight == 115) {
                    djj1.style.height = 285;


                }
                else if (bheight == 160) {
                    djj1.style.height = 330;
                }
            }
            else {
                if (bheight == 115) {
                    djj1.style.height = 285;
                }
                else if (bheight == 160) {
                    djj1.style.height = 330;
                }

            }
            //djj1.scrollTop=0; 
            //djj1.style.height = entryZoomHeight1;
        }
        /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 End */
        /* star 催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
        if (dpb != null) {
            dpb.style.height = entryZoomHeight;
            dpb.style.width = entryZoomWidth;
        }
        if (dgpd != null)
        { dgpd.style.height = entryZoomHeight1; }
        /* end  催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
        if (djinbound != null) {
            djinbound.style.height = entryZoomHeight;
            djinbound.style.width = entryZoomWidth;
        }
    } else {
        oDiv.style.display = 'block';
        document.getElementById("aspHiddenFieldShowDb").value = 'block';
        if (db != null) {
            db.style.height = entryNormalHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryNormalHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryNormalHeight;
            dd.style.width = entryZoomWidth;
        }  
        if (de != null) {
            de.style.height = entryNormalHeight;
            de.style.width = entryZoomWidth;
        }      
           if (div12 != null) {
            div12.style.height = entryNormalHeight;
            div12.style.width = entryZoomWidth;
        }      
        
        if (df != null) {
            df.style.height = entryNormalHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryNormalHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryNormalHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryNormalHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryNormalHeight;
            dj.style.width = entryZoomWidth;
        }
        if (dk != null) {
            dk.style.height = entryNormalHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryNormalHeight;
            dl.style.width = entryZoomWidth;
        }
        if (djj != null)
        { djj.style.height = entryZoomHeight2; }
        /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 Start */
        if (djj1 != null) {
            // 催收主畫 IR Added By Weilin 2010/11/03 Start
            djj1.scrollTop = 0;
            // 催收主畫 IR Added By Weilin 2010/11/03 End

            var divR = document.getElementById('divREC');
            var bheight = djj1.offsetHeight;
            if (divR.style.display == 'block') {
                if (bheight == 285) {
                    djj1.style.height = 115;

                }
                else if (bheight == 330) {
                    djj1.style.height = 160;
                }
            }
            else {
                if (bheight == 330) {
                    djj1.style.height = 160;
                }
                else if (bheight == 285) {
                    djj1.style.height = 115;
                }

            }
            djj1.scrollTop = 0;
            //djj1.style.height = entryZoomHeight1;
        }
        /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 End */
        /* star 催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
        if (dpb != null) {
            dpb.style.height = entryNormalHeight;
            dpb.style.width = entryZoomWidth;
        }
        if (dgpd != null)
        { dgpd.style.height = entryZoomHeight2; }
        /* end  催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */
        if (djinbound != null) {
            djinbound.style.height = entryNormalHeight;
            djinbound.style.width = entryZoomWidth;
        }
    }
}

function showDb1() {

    var db = document.getElementById("divENTRY");
    var dc = document.getElementById("divRecord");
    var dd = document.getElementById("divCSI");
    var de = document.getElementById("divGura");/* modify by 徐蕓 保證債務人頁簽*/
    var div12 = document.getElementById("div12")
    var df = document.getElementById("divJCICQuery");
    var dg = document.getElementById("divFinishCase");
    var dh = document.getElementById("divLA");
    var di = document.getElementById("divJCICApply");
    var dj = document.getElementById("divsend0");
    var dk = document.getElementById("divsend1");
    var dl = document.getElementById("divsend2");
//    var djj = document.getElementById('divRECG');
    var dbtxt = document.getElementById("aspHiddenFieldShowDb").value;
    var oDiv = document.getElementById("divSrch");
    if (dbtxt == 'none') {
        if (oDiv != null && oDiv != undefined) {
            oDiv.style.display = 'none';
        }
        document.getElementById("aspHiddenFieldShowDb").value = 'none';
        if (db != null) {
            db.style.height = entryZoomHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryZoomHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryZoomHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryZoomHeight;
            de.style.width = entryZoomWidth;
        }   
             if (div12 != null) {
            div12.style.height = entryZoomHeight;
            div12.style.width = entryZoomWidth;
        }  
        
        if (df != null) {
            df.style.height = entryZoomHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryZoomHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryZoomHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryZoomHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryZoomHeight;
            dj.style.width = entryZoomWidth;
        }

        if (dk != null) {
            dk.style.height = entryZoomHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryZoomHeight;
            dl.style.width = entryZoomWidth;
        }
        //        if (djinbound != null) {
        //            djinbound.style.height = entryZoomHeight;
        //            djinbound.style.width = entryZoomWidth;
        //        }

        //        if (dpb != null) {
        //            dpb.style.height = entryZoomHeight;
        //            dpb.style.width = entryZoomWidth;
        //        }
        //        if (dgpd != null)
        //        { dgpd.style.height = entryZoomHeight1; }
        /* end  催收主畫面CR 新增Call Back  & PTP 歷史查詢頁籤.修改日期：2010/08/25 修改人：雷平 */

    } else {
        if (oDiv != null && oDiv != undefined) {
            oDiv.style.display = 'block';
        }
        document.getElementById("aspHiddenFieldShowDb").value = 'block';
        if (db != null) {
            db.style.height = entryNormalHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryNormalHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryNormalHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryNormalHeight;
            de.style.width = entryZoomWidth;
        }    
        
           if (div12 != null) {
            div12.style.height = entryNormalHeight;
            div12.style.width = entryZoomWidth;
        }     
          
        if (df != null) {
            df.style.height = entryNormalHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryNormalHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryNormalHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryNormalHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryNormalHeight;
            dj.style.width = entryZoomWidth;
        }
        if (dk != null) {
            dk.style.height = entryNormalHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryNormalHeight;
            dl.style.width = entryZoomWidth;
        }
    }
}

function addWidth() {
    try {
        var multWidth1 = 0.945; //0.96;
        switch (screen.width) {
            case 1024:
                multWidth1 = 0.93; //3; 
        }
        if (document.getElementById("divRecord").style.width != '' && document.getElementById("divRecord").style.width != 0 && document.getElementById("divRecord").style.width.substring(document.getElementById("divRecord").style.width.length, document.getElementById("divRecord").style.width.length - 1) != '%')

            document.getElementById('divRECG').style.width = entryZoomWidth * 0.995;
        /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 Start */
        document.getElementById('aspgirdRecord_dvBody').style.width = entryZoomWidth * 0.995;
        /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/09/13 End */
        document.getElementById('divA').style.width = parent.document.body.clientWidth * multWidth1;
        document.getElementById('divE').style.width = parent.document.body.clientWidth * multWidth1;
        document.getElementById('divC').style.width = parent.document.body.clientWidth * multWidth1;
    }
    catch (err)
    { return false; }
}
function nowrap() {
    addWidth();
    //    //催收
    var txt = document.getElementById('aspgirdRecord_ShareGridView');
    //    if(txt!=null){
    //    for(var i=0;i<txt.cells.length;i++)
    //    {
    //        document.getElementById('aspgirdRecord_ShareGridView').cells(i).noWrap = 'noWrap';
    //    }}
    //延滯
    txt = document.getElementById('aspbillGird_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspbillGird_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }

    txt = document.getElementById('aspgridCard_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspgridCard_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }
    //呆帳
    txt = document.getElementById('aspgridDebtN_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspgridDebtN_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }

    txt = document.getElementById('aspgridDebtY_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspgridDebtY_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }
    //綜合帳務
    txt = document.getElementById('aspBankGird_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspBankGird_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }

    txt = document.getElementById('aspCardGird_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspCardGird_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }
}

//讓Gridview內容不換行
//function noWrap(tableID)
//{
//    var tableObj=document.getElementById(tableID);
//    if(tableObj!=null){
//        for (var i=0; i < tableObj.cells.length; i++) {
//            tableObj.cells(i).noWrap = true;
//        }
//    }
//}

function FrameSizeChange() {
    multWidth = 0.945;
    switch (screen.width) {
        case 1024:
            multWidth = 0.93;
    }
    entryZoomWidth = parent.document.body.clientWidth * multWidth;
    var db = document.getElementById("divENTRY");
    var dc = document.getElementById("divRecord");
    var dd = document.getElementById("divCSI");
    var de = document.getElementById("divCumstormComplain");
    var dg = document.getElementById("divFinishCase");
    var dh = document.getElementById("divLA");
    var dj = document.getElementById("divsend0");
    var dk = document.getElementById("divsend1");
    var dl = document.getElementById("divsend2");
    var CIF = document.getElementById("DivCIFBasicData");
    var TEL = document.getElementById("divCIFTel");
    var ADDR = document.getElementById("divCIFAddr");
//    var Inbound = document.getElementById("divInbound");
    var divSrch1 = document.getElementById("divSrch1");
    var blockAll = document.getElementById("blockAll");
    if (db != null)
    { db.style.width = entryZoomWidth; }
    if (dc != null)
    { dc.style.width = entryZoomWidth; }
    if (dd != null)
    { dd.style.width = entryZoomWidth; }
    if (de != null)
    { de.style.width = entryZoomWidth; }
    if (dg != null)
    { dg.style.width = entryZoomWidth; }
    if (dh != null)
    { dh.style.width = entryZoomWidth; }
    if (dj != null)
    { dj.style.width = entryZoomWidth; }
    if (dk != null)
    { dk.style.width = entryZoomWidth; }
    if (dl != null)
    { dl.style.width = entryZoomWidth; }
    if (CIF != null)
    { CIF.style.width = entryZoomWidth; }
    if (TEL != null)
    { TEL.style.width = entryZoomWidth; }
    if (ADDR != null)
    { ADDR.style.width = entryZoomWidth; }


    if (divSrch1 != null)
    { divSrch1.style.width = entryZoomWidth * 1.01; }
    if (blockAll != null)
    { blockAll.style.width = entryZoomWidth * 1.01; }
//    if (Inbound != null)
//    { Inbound.style.width = entryZoomWidth * 1.01; }
    nowrap();
}
function ReloadHead() {
    var txt = document.getElementById('aspHiddenFieldReloadHead');
    if (txt.value != "") {
        txt.value = "";
        var head = parent.parent.document.frames.item('FrameHead');
        try {
            head.document.form1.submit();
        }
        catch (err)
        { return false; }

    }
}
function Flash() {
    var UserFileNo = document.getElementById('aspHiddenFieldReloadDebt');
    if (UserFileNo.value == 3)
        document.getElementById('btnNoFlash').click();
    UserFileNo.value = "";
}

//gridview radiobutton 
function sBankAcct(arg, txt, debt) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHLink');
    oTxAcc1.value = arg;
    var oTxAcc2 = document.getElementById('aspHiddenFieldDetail');
    oTxAcc2.value = txt;
    var oTxAcc3 = document.getElementById('aspHiddenFieldDebt');
    oTxAcc3.value = debt;
    document.getElementById('btnNoFlash').click();
}

function sCardAcct(arg, txt, debt) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHLink');
    oTxAcc1.value = arg;
    var oTxAcc2 = document.getElementById('aspHiddenFieldDetail');
    oTxAcc2.value = txt;
    var oTxAcc3 = document.getElementById('aspHiddenFieldDebt');
    oTxAcc3.value = debt;
    document.getElementById('btnNoFlash').click();
}
//star CR主畫面 增加當日催記可以修改功能.修改日期：2010/08/30 修改人：雷平
function sColtAcct(arg, txt, debt, ColtDiryType, ColtDiryType2) {

    var oTxAcc2 = document.getElementById('aspHiddenFieldDetail');
    oTxAcc2.value = txt;

    var oTxAcc3 = document.getElementById('aspHiddenFieldColtDirySno');
    oTxAcc3.value = debt;

    var oTxAcc1 = document.getElementById('aspHiddenFieldColtDiryType');
    oTxAcc1.value = ColtDiryType;

    var oTxAcc4 = document.getElementById('aspHiddenFieldColtDiryType2');
    oTxAcc4.value = ColtDiryType2;
    if (arg == '1') {
        showJSMessageFor_aspSysMessageMaster(msgSv1ColtDiry6); return false;
    }

    document.getElementById('btnNoFlash').click();
}
//end CR主畫面 增加當日催記可以修改功能.修改日期：2010/08/30 修改人：雷平

function sLinkBAcct(txt, grid1, grid2) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHAcc');
    oTxAcc1.value = txt;
    var t1 = checkRadChoice(grid1, 'BAcct');
    var t2 = checkRadChoice(grid2, 'BAcct');
    if (t1 == 'none' || t1 == 'Bill')
    { clearRadChoice(grid1, 'BAcct'); }
    if (t2 == 'none' || t2 == 'Bill')
    { clearRadChoice(grid2, 'BAcct'); }
    document.getElementById('btnNoFlash').click();
}

function sLinkCAcct(txt, grid1, grid2) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHCard');
    oTxAcc1.value = txt;
    var t1 = checkRadChoice(grid1, 'CAcct');
    var t2 = checkRadChoice(grid2, 'CAcct');
    if (t1 == 'none' || t1 == 'Card')
    { clearRadChoice(grid1, 'CAcct'); }
    if (t2 == 'none' || t2 == 'Card')
    { clearRadChoice(grid2, 'CAcct'); }
    document.getElementById('btnNoFlash').click();
}

var checkflag = false;
function clearRadChoice(grid, type) {
    if (grid == null || grid == '')
        return true;
    var ele = document.getElementsByTagName('input');
    for (var i = 0; i < ele.length - 1; i++) {
        if (ele[i].type == 'radio') {
            if (ele[i].id.indexOf('RadioChoice') >= 0 && ele[i].id.indexOf(grid) >= 0) {
                ele[i].checked = checkflag;
                GirdViewchangeColorCheckBox(ele[i]);
            }
        }
    }
}

function checkRadChoice(grid, type) {
    if (grid != 'aspgridDebtN') { return 'none'; } //只有無擔需要判斷銀或卡
    var checkitem = "";
    var txt = document.getElementById(grid + '_ShareGridView');
    if (txt != null && txt.cells(14) != null) {
        var ele = document.getElementsByTagName('input');
        for (var i = 0; i < ele.length - 1; i++) {
            if (ele[i].type == 'radio') {
                if (ele[i].id.indexOf('RadioChoice') >= 0 && ele[i].id.indexOf(grid) >= 0) {
                    if (ele[i].checked) {
                        var index = (parseInt(ele[i].id.split('_')[2].substring(3), 10) - 1) * 13 - 2 * (parseInt(ele[i].id.split('_')[2].substring(3), 10) - 2);
                        if (txt.cells(index).innerText == '信用卡')
                        { if (type == "CAcct") ele[i].checked = false; }
                        else { if (type == "BAcct") ele[i].checked = false; }
                    }
                }
            }
        }
    }
    return '';
}


function sNLinkBAcct(grid1) {
    var t1 = checkRadChoice(grid1, 'BAcct');
    if (t1 == 'none' || t1 == 'Bill')
    { clearRadChoice(grid1, 'BAcct'); }
}

function sNLinkCAcct(grid1) {
    var t1 = checkRadChoice(grid1, 'CAcct');
    if (t1 == 'none' || t1 == 'Card')
    { clearRadChoice(grid1, 'CAcct'); }
}

function hidDiry() {
    var d1 = document.getElementById('trigger_dtPay1');
    var d2 = document.getElementById('trigger_dtPay2');
    var dd1 = document.getElementById('aspTextBoxDtPay1');
    var dd2 = document.getElementById('aspTextBoxDtPay2');
    if (dd1 != null && d1 != null) {
        if (dd1.disabled)
            d1.disabled = "disabled";
    }
    if (dd2 != null && d2 != null) {
        if (dd2.disabled)
            d2.disabled = "disabled";
    }

}

function shred() {
    if (document.getElementById('HiddenFieldShRed') != null && document.getElementById('HiddenFieldShRed').value == "show")
        document.getElementById('TPinfo').innerHTML = '<font color=red>客訴 &amp; 重要情報</font>';
}

function checkChoice() {
    document.getElementById('aspCheckBoxFCTypeTel').disabled = false;
    document.getElementById('aspCheckBoxFCTypeAddr').disabled = false;
    var txt = document.getElementById('aspHiddenFieldChoice').value;
    if (txt == '02' || txt == '04' || txt == '06' || txt == '07' || txt == '08' || txt == '09') {
        document.getElementById('aspCheckBoxFCTypeTel').checked = false;
        document.getElementById('aspCheckBoxFCTypeAddr').checked = false;
        document.getElementById('aspCheckBoxFCTypeTel').disabled = true;
        document.getElementById('aspCheckBoxFCTypeAddr').disabled = true;
    }
}


function changeTap(obj) {
    if (obj != null)
        if (obj.className != "selected") {
            switch (obj.id) {
                case "CIFTel":
                    document.getElementById("divCIFBasicData").style.display = "none";
                    document.getElementById("divCIFTel").style.display = "block";
                    document.getElementById("divCIFAddr").style.display = "none";
                    document.getElementById("CIFBasicData").className = "";
                    document.getElementById("CIFAddr").className = "";
                    break;
                case "CIFAddr":
                    document.getElementById("divCIFBasicData").style.display = "none";
                    document.getElementById("divCIFTel").style.display = "none";
                    document.getElementById("divCIFAddr").style.display = "block";
                    document.getElementById("CIFBasicData").className = "";
                    document.getElementById("CIFTel").className = "";
                    break;
                default:
                    document.getElementById("divCIFBasicData").style.display = "block";
                    document.getElementById("divCIFTel").style.display = "none";
                    document.getElementById("divCIFAddr").style.display = "none";
                    document.getElementById("CIFAddr").className = "";
                    document.getElementById("CIFTel").className = "";
                    break;
            }
            obj.className = "selected";
            document.getElementById('aspHiddenFieldTabControlIndex2').value = obj.id;
        }
    return false;
}
function changeTapAmt(obj) {

    //催收主畫面IR 表頭會鎖定 Added By Weilin 2010/11/04 Start
    InitRecPos();
    //催收主畫面IR 表頭會鎖定 Added By Weilin 2010/11/04 Start

    if (obj.className == "selected")
        return false;
    else {
        var txt = document.getElementById('aspHiddenFieldTabControlIndexValue').value;
        switch (obj.id) {
            case "AMTArr":
                if (txt == "GRID") {
                    showJSMessageFor_aspSysMessageMaster(msgGrid);
                    return false;
                }
                else {
                    return true;
                }
                break;
            case "AMTDebt":
                if (txt == "DEBT") {
                    showJSMessageFor_aspSysMessageMaster(msgDebt);
                    return false;
                }
                else {
                    return true;
                }
                break;
            default:
                return true;
                break;
        }
    }
}
function changeTapEntry(obj) {

    /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/11/04 Start */
    InitRecPos();
    /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/11/04 End */

    if (obj.className == "selected")
        return false;
}

function Click_SysMessage() {
    var y = document.documentElement.offsetHeight;
    var txt = document.getElementById('aspHiddenFieldClick_SysMessage');
    if (txt != null) {
        if (txt.value == "") {
            //往上縮
            window.scrollTo(0, y);
            txt.value = "Click";
        }
        else {
            //回復
            window.scrollTo(0, 0);
            txt.value = "";
        }
    }
}
function InitRecPos() {
    /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/11/04 Start */
    var djj1 = document.getElementById('aspgirdRecord_dvBody');
    if (djj1 != null) {
        djj1.scrollTop = 0;
    }
    /* 催收主畫面CR 鎖定表頭功能 Modified By Weilin 2010/11/04 End */
}

/*add by 徐蕓 打開shareQ 頁面20130105 begion */
function showShareQ() {
    var win = window.showModalDialog("ShareQ.aspx", window, "dialogWidth=460px;dialogHeight=200px;scroll=no;status=no;help=no");
    if (win != '') {
        if (win != undefined) {
            //                window.location = win;
        }
        else {
            showShareQ();
        }
    }
}
/*add by 徐蕓 打開shareQ 頁面20130105 end */
/*add by zhuqianqian start*/
function showDbNewStart() {
    var da = document.getElementById("divDetail1");
    var db = document.getElementById("divDetail2");
    var dc = document.getElementById("divDetail3");
    var dd = document.getElementById("divDetail4");
    var de = document.getElementById("divDetail5");
    var df = document.getElementById("divDetail6");
    var dg = document.getElementById("divDetail7");
    var dh = document.getElementById("divDetail8");
    var di = document.getElementById("divDetail9");
    var dj = document.getElementById("divDetail10");
    var dk = document.getElementById("divDetail11");
    var dl = document.getElementById("divDetail12");
    var dm = document.getElementById("divDetail13");
    var dn = document.getElementById("divDetail14");
    var dp = document.getElementById("divDetail15");
    var dq = document.getElementById("divDetail16");
    var dr = document.getElementById("divDetail17");
    var dbtxt = document.getElementById("aspHiddenFieldShowDb").value;
    var oDiv = document.getElementById("divSrch");
    if (oDiv.style.display == '' || oDiv.style.display == 'block') {
        oDiv.style.display = 'none';
        document.getElementById("aspHiddenFieldShowDb").value = 'none';  
        if (oDiv != null && oDiv != undefined) {
            oDiv.style.display = 'none';
        }
        document.getElementById("aspHiddenFieldShowDb").value = 'none';
        if (da != null) {
            da.style.height = entryZoomHeight;
            da.style.width = entryZoomWidth;
        }
        if (db != null) {
            db.style.height = entryZoomHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryZoomHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryZoomHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryZoomHeight;
            de.style.width = entryZoomWidth;
        }      

        if (df != null) {
            df.style.height = entryZoomHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryZoomHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryZoomHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryZoomHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryZoomHeight;
            dj.style.width = entryZoomWidth;
        }

        if (dk != null) {
            dk.style.height = entryZoomHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryZoomHeight;
            dl.style.width = entryZoomWidth;
        }
        if (dm != null) {
            dm.style.height = entryZoomHeight;
            dm.style.width = entryZoomWidth;
        }
        if (dn != null) {
            dn.style.height = entryZoomHeight;
            dn.style.width = entryZoomWidth;
        }
        if (dp != null) {
            dp.style.height = entryZoomHeight;
            dp.style.width = entryZoomWidth;
        }

        if (dq != null) {
            dq.style.height = entryZoomHeight;
            dq.style.width = entryZoomWidth;
        }
        if (dr != null) {
            dr.style.height = entryZoomHeight;
            dr.style.width = entryZoomWidth;
        }
    } else {
        if (oDiv != null && oDiv != undefined) {
            oDiv.style.display = 'block';
        }
        document.getElementById("aspHiddenFieldShowDb").value = 'block';
        if (da != null) {
            da.style.height = entryNormalHeight;
            da.style.width = entryZoomWidth;
        }
        if (db != null) {
            db.style.height = entryNormalHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryNormalHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryNormalHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryNormalHeight;
            de.style.width = entryZoomWidth;
        }
        if (df != null) {
            df.style.height = entryNormalHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryNormalHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryNormalHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryNormalHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryNormalHeight;
            dj.style.width = entryZoomWidth;
        }
        if (dk != null) {
            dk.style.height = entryNormalHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryNormalHeight;
            dl.style.width = entryZoomWidth;
        }
        if (dm != null) {
            dm.style.height = entryNormalHeight;
            dm.style.width = entryZoomWidth;
        }
        if (dn != null) {
            dn.style.height = entryNormalHeight;
            dn.style.width = entryZoomWidth;
        }
        if (dp != null) {
            dp.style.height = entryNormalHeight;
            dp.style.width = entryZoomWidth;
        }
        if (dq != null) {
            dq.style.height = entryNormalHeight;
            dq.style.width = entryZoomWidth;
        }
        if (dr != null) {
            dr.style.height = entryNormalHeight;
            dr.style.width = entryZoomWidth;
        }
    }
}
function showDbNew() {
    var da = document.getElementById("divDetail1");
    var db = document.getElementById("divDetail2");
    var dc = document.getElementById("divDetail3");
    var dd = document.getElementById("divDetail4");
    var de = document.getElementById("divDetail5");
    var df = document.getElementById("divDetail6");
    var dg = document.getElementById("divDetail7");
    var dh = document.getElementById("divDetail8");
    var di = document.getElementById("divDetail9");
    var dj = document.getElementById("divDetail10");
    var dk = document.getElementById("divDetail11");
    var dl = document.getElementById("divDetail12");
    var dm = document.getElementById("divDetail13");
    var dn = document.getElementById("divDetail14");
    var dp = document.getElementById("divDetail15");
    var dq = document.getElementById("divDetail16");
    var dr = document.getElementById("divDetail17");
    var dbtxt = document.getElementById("aspHiddenFieldShowDb").value;
    var oDiv = document.getElementById("divSrch");
    if (dbtxt == 'none') {
        if (oDiv != null && oDiv != undefined) {
            oDiv.style.display = 'none';
        }
        document.getElementById("aspHiddenFieldShowDb").value = 'none';
        if (da != null) {
            da.style.height = entryZoomHeight;
            da.style.width = entryZoomWidth;
        }
        if (db != null) {
            db.style.height = entryZoomHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryZoomHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryZoomHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryZoomHeight;
            de.style.width = entryZoomWidth;
        }

        if (df != null) {
            df.style.height = entryZoomHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryZoomHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryZoomHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryZoomHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryZoomHeight;
            dj.style.width = entryZoomWidth;
        }

        if (dk != null) {
            dk.style.height = entryZoomHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryZoomHeight;
            dl.style.width = entryZoomWidth;
        }
        if (dm != null) {
            dm.style.height = entryZoomHeight;
            dm.style.width = entryZoomWidth;
        }
        if (dn != null) {
            dn.style.height = entryZoomHeight;
            dn.style.width = entryZoomWidth;
        }
        if (dp != null) {
            dp.style.height = entryZoomHeight;
            dp.style.width = entryZoomWidth;
        }

        if (dq != null) {
            dq.style.height = entryZoomHeight;
            dq.style.width = entryZoomWidth;
        }
        if (dr != null) {
            dr.style.height = entryZoomHeight;
            dr.style.width = entryZoomWidth;
        }
    } else {
        if (oDiv != null && oDiv != undefined) {
            oDiv.style.display = 'block';
        }
        document.getElementById("aspHiddenFieldShowDb").value = 'block';
        if (da != null) {
            da.style.height = entryNormalHeight;
            da.style.width = entryZoomWidth;
        }
        if (db != null) {
            db.style.height = entryNormalHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryNormalHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryNormalHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryNormalHeight;
            de.style.width = entryZoomWidth;
        }
        if (df != null) {
            df.style.height = entryNormalHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryNormalHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryNormalHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryNormalHeight;
            di.style.width = entryZoomWidth;
        }
        if (dj != null) {
            dj.style.height = entryNormalHeight;
            dj.style.width = entryZoomWidth;
        }
        if (dk != null) {
            dk.style.height = entryNormalHeight;
            dk.style.width = entryZoomWidth;
        }
        if (dl != null) {
            dl.style.height = entryNormalHeight;
            dl.style.width = entryZoomWidth;
        }
        if (dm != null) {
            dm.style.height = entryNormalHeight;
            dm.style.width = entryZoomWidth;
        }
        if (dn != null) {
            dn.style.height = entryNormalHeight;
            dn.style.width = entryZoomWidth;
        }
        if (dp != null) {
            dp.style.height = entryNormalHeight;
            dp.style.width = entryZoomWidth;
        }
        if (dq != null) {
            dq.style.height = entryNormalHeight;
            dq.style.width = entryZoomWidth;
        }
        if (dr != null) {
            dr.style.height = entryNormalHeight;
            dr.style.width = entryZoomWidth;
        }
    }
}
function FrameSizeChangeNew() {
    multWidth = 0.945;
    switch (screen.width) {
        case 1024:
            multWidth = 0.93;
    }
    entryZoomWidth = parent.document.body.clientWidth * multWidth;
    var da = document.getElementById("divDetail1");
    var db = document.getElementById("divDetail2");
    var dc = document.getElementById("divDetail3");
    var dd = document.getElementById("divDetail4");
    var de = document.getElementById("divDetail5");
    var df = document.getElementById("divDetail6");
    var dg = document.getElementById("divDetail7");
    var dh = document.getElementById("divDetail8");
    var di = document.getElementById("divDetail9");
    var dj = document.getElementById("divDetail10");
    var dk = document.getElementById("divDetail11");
    var dl = document.getElementById("divDetail12");
    var dm = document.getElementById("divDetail13");
    var dn = document.getElementById("divDetail14");
    var dp = document.getElementById("divDetail15");
    var dq = document.getElementById("divDetail16");
    var dr = document.getElementById("divDetail17");
    //    var Inbound = document.getElementById("divInbound");
    var divSrch1 = document.getElementById("divSrch1");
    //var blockAll = document.getElementById("blockAll");
    if (da != null)
    { da.style.width = entryZoomWidth; }
    if (db != null)
    { db.style.width = entryZoomWidth; }
    if (dc != null)
    { dc.style.width = entryZoomWidth; }
    if (dd != null)
    { dd.style.width = entryZoomWidth; }
    if (de != null)
    { de.style.width = entryZoomWidth; }
    if (df != null)
    { df.style.width = entryZoomWidth; }
    if (dg != null)
    { dg.style.width = entryZoomWidth; }
    if (dh != null)
    { dh.style.width = entryZoomWidth; }
    if (di != null)
    { di.style.width = entryZoomWidth; }
    if (dj != null)
    { dj.style.width = entryZoomWidth; }
    if (dk != null)
    { dk.style.width = entryZoomWidth; }
    if (dl != null)
    { dl.style.width = entryZoomWidth; }
    if (dm != null)
    { dm.style.width = entryZoomWidth; }
    if (dn != null)
    { dn.style.width = entryZoomWidth; }
    if (dp != null)
    { dp.style.width = entryZoomWidth; }
    if (dq != null)
    { dq.style.width = entryZoomWidth; }
    if (dr != null)
    { dr.style.width = entryZoomWidth; }
    

    if (divSrch1 != null)
    { divSrch1.style.width = entryZoomWidth * 1.01; }
//    if (blockAll != null)
//    { blockAll.style.width = entryZoomWidth * 1.01; }
    
    nowrap();
}
/*add by zhuqianqian end*/