﻿//半型字元轉全型字元
function changeOneToDobuleByte(changeValue)
{
    //無法轉換的符號有一個  \
    var MapOneByteChar=['0','1','2','3','4','5','6','7','8','9'
        ,'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','s','y','z'
        ,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','S','Y','Z']
    var MapDobuleByteChar=['０','１','２','３','４','５','６','７','８','９'
        ,'ａ','ｂ','ｃ','ｄ','ｅ','ｆ','ｇ','ｈ','ｉ','ｊ','ｋ','ｌ','ｍ','ｎ','ｏ','ｐ','ｑ','ｒ','ｓ','ｔ','ｕ','ｖ','ｗ','ｘ','ｙ','ｚ'
        ,'Ａ','Ｂ','Ｃ','Ｄ','Ｅ','Ｆ','Ｇ','Ｈ','Ｉ','Ｊ','Ｋ','Ｌ','Ｍ','Ｎ','Ｏ','Ｐ','Ｑ','Ｒ','Ｓ','Ｔ','Ｕ','Ｖ','Ｗ','Ｓ','Ｙ','Ｚ']
    var MapOneByteChar2=['`','~','!','@','#','$','%','&','(',')','*','-','_','=','+','[','{',']','}','|',';',':',"'",'"',',','<','.','>','/','?',' ']
    var MapDobuleByteChar2=['‵','～','！','＠','＃','＄','％','＆','（','）','＊','－','＿','＝','＋','〔','｛','｝','〕','｜','；','：','、','＂','，','＜','。','＞','／','？','　']

    for (var j=0; j<MapOneByteChar.length; j++)
    {
        changeValue=changeValue.replace(new RegExp(MapOneByteChar[j],'gm'),MapDobuleByteChar[j]);
    }
    for (var j=0; j<MapOneByteChar2.length; j++)
    {
        for (var k=0; k<changeValue.length; k++)
        {
            changeValue=changeValue.replace(MapOneByteChar2[j] , MapDobuleByteChar2[j]);
        }
    }
    return changeValue;
}

//驗證欄位值是否符合規定
function validateFields(parentControl)
{
    //var elements = document.forms["form1"].elements;
    var elements = document.getElementById(parentControl).all;
    
    for (var i=0; i<elements.length; i++)
    //for (var i=1; i<2; i++)
    {
        //alert("id="+ elements[i].id +":value="+ elements[i].value +":className="+ elements[i].className);

        //檢查欄位是否為全型==>changeToDobuleByte
        if (/(^|)changeToDobuleByte(|$)/.test(elements[i].className))
        {
            elements[i].focus();
            alert("請輸入全型");
            return false;
        }
        
        //檢查欄位是否空白==>checkRequired
        if (/(^|)checkRequired(|$)/.test(elements[i].className) && IsEmpty(elements[i]))
        {
            elements[i].focus();
            alert("請輸入內容");
            return false;
        }
        else
        {
            try
            {
                if (IsEmpty(elements[i]))
                {
                    continue;       //欄位非強迫輸入,故不再判斷輸入格式是正確與否
                }
            }
            catch (ex)
            {}
        }
        
        //檢查EMail是否正確==>checkEmail
        if (/(^|)checkEmail(|$)/.test(elements[i].className) && !CheckEmail(elements[i]))
        {
            elements[i].focus();
            alert("請輸入有效EMail");
            return false;
        }
        
        //檢查身份證字號是否有效==>CheckID
        if (/(^|)CheckID(|$)/.test(elements[i].className) && !CheckID(elements[i]))
        {
            elements[i].focus();
            alert("請輸入有效身分證字號");
            return false;
        }
        
        //檢查手機號碼是否合法==>isMobile, 適用:TextBox
        if (/(^|)isMobile(|$)/.test(elements[i].className) && !isMobile(elements[i]))
        {
            elements[i].focus();
            alert("請輸入合法的手機號碼");
            return false;
        }
        
        //檢查手機號碼是否合法==>isMobileDLL, 適用:DropDownList
        if (/(^|)isMobileDLL(|$)/.test(elements[i].className) && !isMobileDLL(elements[i]))
        {
            elements[i].focus();
            alert("請輸入合法的手機號碼");
            return false;
        }
        
        //檢查欄位是否為數值==>IsNumberint 或 IsNumberfloat 或 IsPositiveNumberint 或 IsPositiveNumberfloat
        if (/(^|)IsNumberint(|$)/.test(elements[i].className) && !IsNumber(elements[i],"int")
            || /(^|)IsNumberfloat(|$)/.test(elements[i].className) && !IsNumber(elements[i],"float")
            || /(^|)IsPositiveNumberint(|$)/.test(elements[i].className) && !IsNumber(elements[i],"+int")
            || /(^|)IsPositiveNumberfloat(|$)/.test(elements[i].className) && !IsNumber(elements[i],"+float"))
        {
            elements[i].focus();
            alert("請輸入有效數值");
            return false;
        }
        
        //檢查欄位長度是否過長==>checkLength(N)
        if (/(^|)checkLength(|$)/.test(elements[i].className))
        {
            var s=elements[i].className;
            var l1=s.indexOf("checkLength(")+"checkLength(".length;
            var l2=s.indexOf(")",l1);            
            if (GetLengthFromText(elements[i])>(s.substr(l1,l2-l1)))
            {
                elements[i].focus();
                alert("請輸入有效長度(最長"+ s.substr(l1,l2-l1) +"碼)");
                return false;
            }
        }
        
         //檢查是否為有效民國年
        if (/(^|)isCDate(|$)/.test(elements[i].className) )
        {
            var s=elements[i].className;                             
            var l1=s.indexOf("isCDate(")+"isCDate(".length;
            var l2=s.indexOf(")",l1);      
            var parm= s.substring(l1,l2);
            parm=trim(parm);
            
            if (parm.length==0 && !isCDate(elements[i].value ,'' ) )
            {
                elements[i].focus();
                alert("請輸入有效民國年");
                return false;
            }
            else if (parm.length==1 && !isCDate(elements[i].value , parm ))
            {
                elements[i].focus();
                alert("請輸入有效民國年");
                return false;
            }
        }
        
         //檢查是否為有效西元年
        if (/(^|)isDate(|$)/.test(elements[i].className) )
        {
            var s=elements[i].className;                             
            var l1=s.indexOf("isDate(")+"isDate(".length;
            var l2=s.indexOf(")",l1);      
            var parm= s.substring(l1,l2);
            parm=trim(parm);

            if (parm.length==0 && !isDate(elements[i].value ,'' ) )
            {
                elements[i].focus();
                alert("請輸入有效西元年");
                return false;
            }
            else if (parm.length==1 && !isDate(elements[i].value , parm ))
            {
                elements[i].focus();
                alert("請輸入有效西元年");
                return false;
            }
        } 
    }
    return true;
}


function validateFields2(parentControl, callbackName)
{
//alert(1);
    //var elements = document.forms["form1"].elements;
    var elements = document.getElementById(parentControl).all;
    
    for (var i=0; i<elements.length; i++)
    //for (var i=1; i<2; i++)
    {
        //alert("id="+ elements[i].id +":value="+ elements[i].value +":className="+ elements[i].className);

        //檢查欄位是否為全型==>changeToDobuleByte
        if (/(^|)changeToDobuleByte(|$)/.test(elements[i].className))
        {
            elements[i].focus();
            eval(callbackName + '("請輸入全型")');
            return false;
        }
        
        //檢查欄位是否空白==>checkRequired
        if (/(^|)checkRequired(|$)/.test(elements[i].className) && IsEmpty(elements[i]))
        {
            elements[i].focus();
            eval(callbackName + '("請輸入內容")');
            return false;
        }
        else
        {
            try
            {
                if (IsEmpty(elements[i]))
                {
                    continue;       //欄位非強迫輸入,故不再判斷輸入格式是正確與否
                }
            }
            catch (ex)
            {}
        }
        
        //檢查EMail是否正確==>checkEmail
        if (/(^|)checkEmail(|$)/.test(elements[i].className) && !CheckEmail(elements[i]))
        {
            elements[i].focus();
            eval(callbackName + '("請輸入有效EMail")');
            return false;
        }
        
        //檢查身份證字號是否有效==>CheckID
        if (/(^|)CheckID(|$)/.test(elements[i].className) && !CheckID(elements[i]))
        {
            elements[i].focus();
            eval(callbackName + '("請輸入有效身分證字號")');
            return false;
        }
        
        //檢查手機號碼是否合法==>isMobile, 適用:TextBox
        if (/(^|)isMobile(|$)/.test(elements[i].className) && !isMobile(elements[i]))
        {
            elements[i].focus();
            eval(callbackName + '("請輸入合法的手機號碼")');
            return false;
        }
        
        //檢查手機號碼是否合法==>isMobileDLL, 適用:DropDownList
        if (/(^|)isMobileDLL(|$)/.test(elements[i].className) && !isMobileDLL(elements[i]))
        {
            elements[i].focus();
            eval(callbackName + '("請輸入合法的手機號碼")');
            return false;
        }
        
        //檢查欄位是否為數值==>IsNumberint 或 IsNumberfloat 或 IsPositiveNumberint 或 IsPositiveNumberfloat
        if (/(^|)IsNumberint(|$)/.test(elements[i].className) && !IsNumber(elements[i],"int")
            || /(^|)IsNumberfloat(|$)/.test(elements[i].className) && !IsNumber(elements[i],"float")
            || /(^|)IsPositiveNumberint(|$)/.test(elements[i].className) && !IsNumber(elements[i],"+int")
            || /(^|)IsPositiveNumberfloat(|$)/.test(elements[i].className) && !IsNumber(elements[i],"+float"))
        {
            elements[i].focus();
            eval(callbackName + '("請輸入有效數值")');
            return false;
        }
        
        //檢查欄位長度是否過長==>checkLength(N)
        if (/(^|)checkLength(|$)/.test(elements[i].className))
        {
            var s=elements[i].className;
            var l1=s.indexOf("checkLength(")+"checkLength(".length;
            var l2=s.indexOf(")",l1);            
            if (GetLengthFromText(elements[i])>(s.substr(l1,l2-l1)))
            {
                elements[i].focus();
                eval(callbackName + '("請輸入有效長度(最長"' + s.substr(l1,l2-l1) + '"碼)")');
                return false;
            }
        }
        
         //檢查是否為有效民國年
        if (/(^|)isCDate(|$)/.test(elements[i].className) )
        {
            var s=elements[i].className;                             
            var l1=s.indexOf("isCDate(")+"isCDate(".length;
            var l2=s.indexOf(")",l1);      
            var parm= s.substring(l1,l2);
            parm=trim(parm);
            
            if (parm.length==0 && !isCDate(elements[i].value ,'' ) )
            {
                elements[i].focus();
                eval(callbackName + '("請輸入有效民國年")');
                return false;
            }
            else if (parm.length==1 && !isCDate(elements[i].value , parm ))
            {
                elements[i].focus();
                eval(callbackName + '("請輸入有效民國年")');
                return false;
            }
        }
        
         //檢查是否為有效西元年
        if (/(^|)isDate(|$)/.test(elements[i].className) )
        {
            var s=elements[i].className;                             
            var l1=s.indexOf("isDate(")+"isDate(".length;
            var l2=s.indexOf(")",l1);      
            var parm= s.substring(l1,l2);
            parm=trim(parm);

            if (parm.length==0 && !isDate(elements[i].value ,'' ) )
            {
                elements[i].focus();
                eval(callbackName + '("請輸入有效西元年")');
                return false;
            }
            else if (parm.length==1 && !isDate(elements[i].value , parm ))
            {
                elements[i].focus();
                eval(callbackName + '("請輸入有效西元年")');
                return false;
            }
        } 
    }
    return true;
}
