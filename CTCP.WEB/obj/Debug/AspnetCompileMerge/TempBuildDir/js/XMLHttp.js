/* XMLHttpRequest Object Pool */ 
var XMLHttp = {
	_objPool: [],

	_getInstance: function ()
	{
		for (var i = 0; i < this._objPool.length; i ++)
		{
			if (this._objPool[i].readyState == 0 || this._objPool[i].readyState == 4)
			{
				return this._objPool[i];
			}
		}

		// IE5中不支援push方法
		this._objPool[this._objPool.length] = this._createObj();

		return this._objPool[this._objPool.length - 1];
	},

	_createObj: function ()
	{
		if (window.XMLHttpRequest)
		{
			var objXMLHttp = new XMLHttpRequest();

		}
		else
		{
			var MSXML = ['MSXML2.XMLHTTP.5.0', 'MSXML2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP', 'Microsoft.XMLHTTP'];
			for(var n = 0; n < MSXML.length; n ++)
			{
				try
				{
					var objXMLHttp = new ActiveXObject(MSXML[n]);
					break;
				}
				catch(e)
				{
				}
			}
		 }		  

		// mozilla某些版本沒有readyState屬性
		if (objXMLHttp.readyState == null)
		{
			objXMLHttp.readyState = 0;

			objXMLHttp.addEventListener("load", function ()
				{
					objXMLHttp.readyState = 4;

					if (typeof objXMLHttp.onreadystatechange == "function")
					{
						objXMLHttp.onreadystatechange();
					}
				},  false);
		}

		return objXMLHttp;
	},

	// 發送請求(方法[post,get], 網址, 數值, 回傳函數)
	sendReq: function (method, url, data, callback)
	{
		var objXMLHttp = this._getInstance();

		with(objXMLHttp)
		{
			try
			{
				// 增加亂數防止緩存
				if (url.indexOf("?") > 0)
				{
					url += "&randnum=" + Math.random();
				}
				else
				{
					url += "?randnum=" + Math.random();
				}

				open(method, url, true);

				// 設定請求編碼方式
				setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=Big5");
				send(data);
				onreadystatechange = function ()
				{
					if (objXMLHttp.readyState == 4 && (objXMLHttp.status == 200 || objXMLHttp.status == 304))
					{
						callback(objXMLHttp);
					}
				}
			}
			catch(e)
			{
				alert(e);
			}
		}
	}
}; 