﻿
//Generate Flower
if (document.form1){
    document.form1.onsubmit=function() {
        var elementDiv = document.createElement("DIV"); 
        elementDiv.id='divFlower';      //Has css
        //var path='';
        //if (location.hostname=='localhost')
        //    path='/web_dev';
        elementDiv.innerHTML='<span id="spanWait">請稍後…</span>';
        try {
            
            document.form1.appendChild(elementDiv);
            
        var element = document.createElement("DIV"); //建立一個新的DIV
            element.id="divLightBox"; 
            document.form1.appendChild(element);
            element.style.visibility="visible";
            return true;
        }
        catch(err){
            elementDiv=null;
        }
    }
   
} //else alert('no form1')


//防止CTRL+N
document.onkeydown = function() {
    if (event.ctrlKey && event.keyCode == 78){
        event.keyCode = 0;
        event.returnValue = false;
        }        
}

function $(element) {
  if (typeof element == "string") {
    return document.getElementById(element);
  }else {
    return element;
  }
}

function addJsCheckTrigger(id) {
    if ($(id)) {
        if(window.addEventListener) {
            //for not ie such as firefox
            $(id).addEventListener('click', function(){return __basepage_validateForm();}, false);     
        }else {
            // for ie
            $(id).attachEvent('onclick', function(){return __basepage_validateForm();});           
        }
    }
}

var sysMessageID;

function showMessageOnControl(id, message) {
    if (sysMessageID) {
        eval('showJSMessageFor_' + sysMessageID + '("' + message + '");');
    }else {
        alert(message);
    }
    $(id).focus();
    if ($(id).type == 'text') {
        $(id).select();
    }
}

function checkUcGridB4Delete(id) {
    var checkCountObj = $(id + '_txtCheckCount');
    if (checkCountObj) {
        if (checkCountObj.value == '0') {
            if (sysMessageID) {
                eval('showJSMessageFor_' + sysMessageID + '("請先勾選欲刪除之項目");');
            }else {
                alert('請先勾選欲刪除之項目');
            }
            return false;
        }else {
            return confirm('是否真的要刪除');        
        }
    }
    return true;
}
function checkEmpty(id, message) {
    //如果最后一個參數是trigger不命中當前事件觸發者的話，則默認返回true
    if (arguments.length == 3 && getEventTarget().id != arguments[2]) {
        return true;
    }
    if (!$(id)) {
        return true;
    }
        
    if ($(id).value == '') {
        showMessageOnControl(id, message);
        return false;
    }else {
        return true;    
    }
}

function checkInt(id, message) {
    //如果最后一個參數是trigger不命中當前事件觸發者的話，則默認返回true
    if (arguments.length == 3 && getEventTarget().id != arguments[2]) {
        return true;
    }
    if (!$(id) || $(id).value == '') {
        return true;
    }

    var regexs = /^(-?[1-9]+[0-9]*|0)$/;
    if (regexs.test($(id).value)) {        
        return true;
    }else {
        showMessageOnControl(id, message);
        return false;    
    }
}

function checkFloat(id, message) {
    //如果最后一個參數是trigger不命中當前事件觸發者的話，則默認返回true
    if (arguments.length == 3 && getEventTarget().id != arguments[2]) {
        return true;
    }
    if (!$(id) || $(id).value == '') {
        return true;
    }

    var regexs =  /^[+|-]?\d*\.?\d*$/;
    if (regexs.test($(id).value)) {        
        return true;
    }else {
        showMessageOnControl(id, message);
        return false;    
    }
}

function checkLength(id, length, message) {
    //如果最后一個參數是trigger不命中當前事件觸發者的話，則默認返回true
    if (arguments.length == 4 && getEventTarget().id != arguments[3]) {
        return true;
    }
    if (!$(id)) {
        return true;
    }
    
    $(id).value = $(id).value.replace(/(^\s*)|(\s*$)/g, "");
    
    if ($(id).value.length > length) {
        showMessageOnControl(id, message);
        return false;
    }else {
        return true;    
    }
}

function checkDate(id, message) {
    //如果最后一個參數是trigger不命中當前事件觸發者的話，則默認返回true
    if (arguments.length == 3 && getEventTarget().id != arguments[2]) {
        return true;
    }
    if (!$(id) || $(id).value == '') {
        return true;
    }
    
    if ($(id).value.length == 8 && $(id).value.indexOf('/') < 0) {
        $(id).value = $(id).value.substr(0, 4) + '/' + $(id).value.substr(4, 2) + '/' + $(id).value.substr(6, 2);
    } else if (($(id).value.length == 6 || $(id).value.length == 7) && $(id).value.indexOf('/') < 0)  {
        //民國年轉為公元年
        var tempYear;
        var tempMonthDay;
        if ($(id).value.length == 6) {
            tempYear = $(id).value.substr(0, 2);
            tempMonthDay = '/' + $(id).value.substr(2, 2) + '/' + $(id).value.substr(4, 2);
        }
        if ($(id).value.length == 7) {
            tempYear = $(id).value.substr(0, 3);
            tempMonthDay = '/' + $(id).value.substr(3, 2) + '/' + $(id).value.substr(5, 2);
        }
        if (checkInt(tempYear,message)) {
            tempYear = Number(tempYear) + 1911;
            $(id).value = tempYear + tempMonthDay;
        }  
    }
        
    if (!isDate($(id).value)) {
        showMessageOnControl(id, message);
        return false;    
    }else {
        return true;    
    }
}

function isDate(str){   
  if (!str.match(/^\d{4}\/\d\d?\/\d\d?$/)) {return   false;}
  var ar=str.replace(/\-0/g,"/").split("/");
  ar= new Array(parseInt(ar[0], 10),parseInt(ar[1], 10)-1,parseInt(ar[2], 10));   
  var d=new Date(ar[0],ar[1],ar[2]);   
  return d.getFullYear()==ar[0] && d.getMonth()==ar[1] && d.getDate()==ar[2];   
}

function getEventTarget() {
    return event.srcElement?event.srcElement:event.target;
}

function disableTabPanel(id, index)
{
    if ($find(id) && $find(id).get_tabs) {
        var TabPanel=$find(id).get_tabs()[index];
        $removeHandler(TabPanel._header,"click",TabPanel._header_onclick$delegate);    
    }else {
        setTimeout("disableTabPanel('" + id + "'," + index + ")", 500);
    }
}

function enableTabPanel(id, index)
{
    if ($find(id) && $find(id).get_tabs) {
        var TabPanel=$find(id).get_tabs()[index];
        $addHandler(TabPanel._header,"click",TabPanel._header_onclick$delegate);    
    }
}

function bindEnterDownToQuery(eventSourceId, menuId) {
    if ($(eventSourceId)) {
        if(window.addEventListener) {
            //for not ie such as firefox
            $(eventSourceId).addEventListener('keydown', function(){
                                                            var keyCode = event.which; 
                                                            if (keyCode == 13) {
                                                                eval('setButtoinClickedFor_' + menuId + '(4);');
                                                                event.returnValue = false;
                                                            }   
                                                            }, false);     
        }else {
            // for ie
            $(eventSourceId).attachEvent('onkeydown', function(){
                                                            var keyCode = event.keyCode;  
                                                            if (keyCode == 13) {
                                                                eval('setButtoinClickedFor_' + menuId + '(4);');
                                                                event.returnValue = false;
                                                            }
                                                            });           
        }
    }
}

//checkIdentityNo--傳回該字串是否為合法的身份證字號
//blnReturn：傳回值，為一布林值(true--合法,false--不合法)
function checkIdentityNo(id){
    var blnReturn = false;
    var strIdNo = $(id).value.toUpperCase();
    //strIdNo=strIdNo.replace(' ','');
    strIdNo = strIdNo.replace(/^[\s]*/gi,'').replace(/[\s]*$/gi,'');
    $(id).value = strIdNo;   
    var message = msg_invalidNo_Message;
          
    //身份字號長度為0返回true
    if(strIdNo.length == 0){ 
        blnReturn = true; 
		return blnReturn;
	}
   
    //只允许输入英文数字
    for(var i=0; i< strIdNo.length; i++)
    {
        var ch = strIdNo.charCodeAt(i);
        if(!(ch>=65 && ch<=90) && !(ch>=48 && ch<=57) && !(ch>=97 && ch<=122) && ! ch==32)
        {
            showMessageOnControl(id, message);
            return false;
        }
    }
    
    //若第一碼不為字母，返回true
    var reg = /^([A-Z|a-z])$/; 
    if(!reg.test(strIdNo.substring(0,1))){            
        blnReturn = true; 
		return blnReturn;
    }
    
    //若第一個字為英文字母且長度大於10時，擷取前10碼做檢核。 -- 改成大於10時返回true
    if(strIdNo.length > 10){        
//        strIdNo = strIdNo.substring(0,10);
        //showMessageOnControl(id, message);
        //return false;
        return true;
    } 
            
    //身份字號長度為10位
    if(strIdNo.length != 10){        
        showMessageOnControl(id, message);
		return blnReturn;
	} else {
        var myreg = /^([A-Z]\d{9})$/;               
        if(!myreg.test(strIdNo)){            
            showMessageOnControl(id, message);
            return blnReturn;
        }
    }
    //以下為檢查碼的check
	var arrayKey=new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
    var arrayValue=new Array("10","11","12","13","14","15","16","17","34","18","19","20","21","22","35","23","24","25","26","27","28","29","32","30","31","33");
    //身份證英文代號轉換成數字
    var value;    
    for(var i=0 ; i<arrayKey.length ; i++){
        if (arrayKey[i].toString() == strIdNo.charAt(0)){   
		    value=arrayValue[i].toString();
	        break;
	    }
    }

    //英文轉成的數字, 個位數乘９再加上十位數 
    //各數字從右到左依次乘１、２、３、４．．．．８ 
    var sum=parseInt(value.charAt(1)) * 9 + parseInt(value.charAt(0));
    for(var i=1 ; i<9 ; i++){
        sum = sum + parseInt(strIdNo.charAt(i)) * (9-i);     
    }    

    //求出总和除10後之餘數,用10減該餘數,結果就是檢查碼，若餘數為0，檢查碼就是 0。 
    var mod = sum % 10
    //檢查碼
    var validateBit;
    if (mod == 0){   
	    validateBit = 0;
    } else {
        validateBit = 10 - mod;	
    }
    
    if(validateBit == strIdNo.charAt(9)){
		blnReturn = true;
	} else {
	    showMessageOnControl(id, message);
	}	
    return blnReturn;
}

function checkPhoneNumber(id) {
    var reg = /^\d{2}-\d{8}$/; 
    if($(id) && $(id).value != '' && !reg.test($(id).value)){
        showMessageOnControl(id, '電話號碼不符規范');
        return false;
    }
    return true;
}

function showLoadingLogo() {
    if (new String(typeof($get)) != 'undefined') {
        if ($get('updateProgress1')) {
            $get('updateProgress1').style.display='block';
        }
    }
}

function hideLoadingLogo() {
    if (new String(typeof($get)) != 'undefined') {
        if ($get('updateProgress1')) {
            $get('updateProgress1').style.display='none';
        }
    }
}

function initAsyncLoadingLogo() {
     Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
            hideLoadingLogo();
        });
     Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(function (sender, args) {
            showLoadingLogo();
        });
     Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(function(sender, args) {
        var pageRequestManager = Sys.WebForms.PageRequestManager.getInstance();
        if (pageRequestManager.get_isInAsyncPostBack()) {
            args.set_cancel(true);
        }
     });
}