﻿function csChangeCust(ID,Account,Mode,CaseGroup){
    
	var rtn = true;
	try
    {
        //關閉所有頁籤
        CloseAllTap();
        rtn=WriteToFile(ID + " : CloseAllTap finish!");
        var obj = top.frames[3].window.document.getElementById('hid_CustID');
        obj.value=trim(ID);
        if(top.frames[3].window.document.getElementById('CTI')!=null) 
            top.frames[3].window.document.getElementById('CTI').value='Y';
        if(top.frames[3].window.document.getElementById('CTIAccount')!=null) 
            top.frames[3].window.document.getElementById('CTIAccount').value='';
        if(top.frames[3].window.document.getElementById('CTICaseGroup')!=null) 
            top.frames[3].window.document.getElementById('CTICaseGroup').value=CaseGroup;
        if(top.frames[3].window.document.getElementById('CTIMode')!=null) 
            top.frames[3].window.document.getElementById('CTIMode').value=Mode;
        if (rtn)
            rtn = WriteToFile("Set value finish!");
              
        top.frames[3].window.document.form1.submit();
        if (rtn)
            rtn = WriteToFile("frames(3) submit finish!");
      
        //呼叫小花
        top.frames[2].window.CreateLLFlower();
        if (rtn)
            rtn = WriteToFile("CreateLLFlower finish!");
        return true;
    }
    catch(err){
        if (rtn==true){
			var mesg = "csChangeCust_Error(ID:" + ID + ",Account: " + Account + ",Mode: " + Mode + ",CaseGroup: " + CaseGroup + ")" + " \n" + err.message;
			alert(mesg);
			return mesg;	
		}

    }

}

function CreateCoverDiv(obj)
{
    var element = obj.createElement("DIV"); //建立一個新的DIV
    element.id="divLightBox"; 
    obj.body.appendChild(element);

}

function csCreateCoverDiv(status)
{
    var Left_obj = top.frames[1].window.document;
    var Right_obj = top.frames[2].window.document;
    if(Right_obj.getElementById("divLightBox"))
        Right_obj.getElementById("divLightBox").style.visibility=status;
    else
    {
        CreateCoverDiv(Right_obj);
        Right_obj.getElementById("divLightBox").style.visibility=status;
    }
}

function csWait()
{
    try
    {
    //關閉所有頁籤
    top.frames[2].window.CloseAllTap();
    csCreateCoverDiv("visible");
    }
    catch (err) {
        return false;
    }
}
function CloseAllTap()
  {
    try
    {
        for (var i = top.frames[2].window.tabNo.length-1 ; i >=0 ; i--)
        {
            if (!isNaN(top.frames[2].window.tabNo[i]) && top.frames[2].window.tabNo[i]!=0)
            {
                if (new String(top.frames[2].window.tabFileName[i]).indexOf("case.aspx") > -1 ) continue;
                if (new String(top.frames[2].window.tabFileName[i]).indexOf("sv1.aspx") > -1 ) continue;
                top.frames[2].window.close(top.frames[2].window.tabNo[i]);
            }
        }
    }
    catch (err) {
        return false;
    }
  }
  
  
//************************************************************
// 目的：去除文字前後的空白
//************************************************************
    function trim(iStr) 
    {
    	while (iStr.substr(0,1) == ' ') 
	    {
		    iStr = iStr.substr(1,iStr.length-1);
	    }
	    while (iStr.substr(iStr.length-1,1) == ' ')
	    {
		    iStr = iStr.substr(0,iStr.length-1);
	    }
	    return iStr;
    }
    