//window.onload=function() { init(); }
//addLoadEvent(init_);
//alert('document.form1.name')

//alert(document.form1);
if (document.form1)
	addEvent(document.form1.SubCategorySno, "change", getModel_, false);

function goProd(url) {
	var frm = document.form1;
	if (frm.ModelSno.selectedIndex > 0)	{
		//alert(frm.ModelSno.options[frm.ModelSno.options.selectedIndex].value+frm.ModelSno.options[frm.ModelSno.options.selectedIndex].text);
		frm.action = url;
		frm.submit();
	} else
		alert('Please select a model first!');
}

function srchProd(url) {
	var frm = document.form1;
	//alert(frm.ModelSno.options[frm.ModelSno.options.selectedIndex].value+frm.ModelSno.options[frm.ModelSno.options.selectedIndex].text);
	frm.action = url;
	frm.submit();
}

function switchLoading(shw) {
	var img = document.getElementById("loading");
	if (img) {
		if (shw)
			img.style.display = 'inline';
		else
			img.style.display = 'none';
	}
}

function getModel_() {
	var sel2=document.form1.SubCategorySno;
	var k=sel2.options[sel2.options.selectedIndex].value;
	//alert(k);
	switchLoading(true);
	XMLHttp.sendReq("POST", "/asp/getModel.asp", "sno=" + escape(k) , handleResponse);
}

function init_(){
	//alert("init");
	document.form1.SubCategorySno.onchange=function(){
		var sel2=document.form1.SubCategorySno;
		var k=sel2.options[sel2.options.selectedIndex].value;
		//alert(k);
		switchLoading(true);
		XMLHttp.sendReq("POST", "/asp/getModel.asp", "sno=" + escape(k) , handleResponse);
	}
}	
function handleResponse(oXMLHttp) {
	var oXMLDom = oXMLHttp.responseXML;
	var sValue = '';
	var pos, na, sno;
	var oNodeOne, sTitle;
	var sel3=document.form1.ModelSno;
	//alert(oXMLDom.xml);
	if (oXMLDom.xml){		//IE
		oNode = oXMLDom.selectNodes("//ROOT/MODEL");
		if (oNode.length > 0) {
			for (i=0 ; i < oNode.length ; i++) {
				//sValue = sValue + oNode.item(j).attributes.getNamedItem("ID").nodeValue + ',';
				sValue = sValue + oNode.item(i).text + '|';
				//alert(oNode.item(j).selectSingleNode("LIST").text);
			}
		}
	} else {				//FireFox
		var oRoot = oXMLDom.getElementsByTagName("ROOT")[0];
		var oNode = oRoot.getElementsByTagName("MODEL");
		if (oNode.length > 0) {
			for (i = 0 ; i < oNode.length ; i++) {
				oNodeOne = oNode[i];
				//var sTitle = oNodeOne.getElementsByTagName("LIST")[0].firstChild.nodeValue;
				sTitle = oNodeOne.firstChild.nodeValue;
				sValue = sValue + sTitle + '|';
			}
		}
	}
	sValue = sValue.substr(0,sValue.length-1);
	//alert(sValue);
	for (i=(sel3.options.length-1); i>=0; i--)
		sel3.remove(i);
	if (sValue != ""){
		var aKey = sValue.split("|");
		//alert(aKey.length);
		sel3.length=aKey.length + 1;
		sel3.options[0] = new Option('Select..', 0, false, false);
		for (i=0 ; i<aKey.length ; i++) {
			pos = aKey[i].indexOf("-")
			sno = aKey[i].substr(0,pos)
			na  = aKey[i].substr(pos+1,aKey[i].length-pos)
			sel3.options[i+1] = new Option(na, sno, false, false);
			//if (ModelSno==sno)
			//	sel3.options[i+1].selected = true;
		}
	}
	switchLoading(false);
}

function addModel(){
	var objFrm = document.form1;
	var slRelSno = objFrm.RelSno;
	var ModelSno = objFrm.ModelSno;
	if (ModelSno.selectedIndex<=0){
		alert('Please select a model');
		return false;
	}
	slRelSno.options[slRelSno.options.length] = new Option(ModelSno.options[ModelSno.selectedIndex].text, ModelSno.options[ModelSno.selectedIndex].value, false, false);
}
function delModel(){
	var objFrm = document.form1;
	var slRelSno = objFrm.RelSno;
	if (slRelSno.selectedIndex<0){
		alert('Please select a item to delete');
		return false;
	}
	slRelSno.options[slRelSno.selectedIndex] = null;
}

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	}
	else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}

function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}
	else {
		elm['on' + evType] = fn;
	}
}