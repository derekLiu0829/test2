﻿//GetLengthFromText--傳回該Text值的長度
//objText：該Text的物件
//ttCounter：傳回值，傳回字元數
function GetLengthFromText(objText) {
	var ttCounter = 0;
	for(var iCount = 0; iCount < objText.value.length; iCount ++) {
		if (objText.value.charCodeAt(iCount) >= 255){
			ttCounter += 1;
		}
	}

	ttCounter += objText.value.length;
	return ttCounter;
}

//GetLengthFromStr--傳回該string值的長度
//str_name：該string的名稱
//ttCounter：傳回值，傳回字元數
function GetLengthFromStr(str_name) {
	var ttCounter = 0;

	for(var iCount = 0; iCount < str_name.length; iCount ++) {
		if (str_name.charCodeAt(iCount) >= 255){
			ttCounter += 1;
		}
	}

	ttCounter += str_name.length;
	return ttCounter;
}


//CheckSymbol--傳回該Text有無該Symbol
//objText：該Text的物件
//strSymbol：該Symbol的值，ex:"+"
//blSymbol：傳回值，為一布林值
function CheckSymbol(objText,strSymbol){
	var blSymbol = false;
	if (objText.value.indexOf(strSymbol) != -1){
		blSymbol = true;
	}
	return blSymbol;
}

//CheckSymbols--傳回該Text有無該串Symbol的任何一個
//objText：該Text的物件
//strSymbol：該Symbol的值，ex:"+-*/"
//blSymbol：傳回值，為一布林值
function CheckSymbols(objText,strSymbol){
	var blSymbol = false;
	for (var i=0; i<strSymbol.length; i++)	{
		if (CheckSymbol(objText,strSymbol.charAt(i))){
			blSymbol = true;
			break;
		}
	}
	return blSymbol;
}

//IsNumber--傳回該text是否為數值，且能判斷是整數或浮點數
//objText：該Text的物件
//numType：int代表整數，float代表浮點數
//checkNum：傳回值，為一布林值
function IsNumber(objText,numType){
	var checkNum = false;
	if(!IsEmpty(objText)){
		if(!isNaN(objText.value)){
			if(numType == "int"){
				if(objText.value.indexOf(".") == -1){
					checkNum = true;
				}
			}
			if(numType == "float"){
				if(objText.value.indexOf(".") != -1){
					checkNum = true;
				}
			}
			if(numType == "+int"){
				if(objText.value.indexOf(".") == -1 && objText.value>=0){
					checkNum = true;
				}
			}
			if(numType == "+float"){
				if(objText.value.indexOf(".") != -1 && objText.value>=0){
					checkNum = true;
				}
			}
		}
	}
	return checkNum;
}


//CheckEmail--傳回該Text是否為有效的Email
//objText：該Text的物件
function CheckEmail(objText) {
    var blSymbol = true;
    var emailStr = objText.value;

    var emailPat=/^(.+)@(.+)$/;
    var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
    var validChars="\[^\\s" + specialChars + "\]";
    var quotedUser="(\"[^\"]*\")";
    var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom=validChars + '+';
    var word="(" + atom + "|" + quotedUser + ")";
    var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
    var matchArray=emailStr.match(emailPat);
    if (matchArray==null) {
    //	return ("這個 Email address 不正確\n請重試一次");
    //	return ("這個 Email address 不正確\n請重試一次");
	    blSymbol = false;
    }
    var user=matchArray[1];
    var domain=matchArray[2];

    if (user.match(userPat)==null) {
    //	return ("username 似乎不對!\n請重試一次");
    //	return ("這個 Email address 不正確\n請重試一次");

	    blSymbol = false;
    }

    var IPArray=domain.match(ipDomainPat);
    if (IPArray!=null) {
	    // this is an IP address
	    for (var i=1;i<=4;i++) {
		    if (IPArray[i]>255) {
    //			return ("IP address 不對!\n請重試一次");
    //			return ("這個 Email address 不正確\n請重試一次");
			    blSymbol = false;

		    }
	    }
	    //return "";
    }

    var domainArray=domain.match(domainPat);
    if (domainArray==null) {
    //	return ("domain 名稱不對!\n請重試一次");
    //	return ("這個 Email address 不正確\n請重試一次");
	    blSymbol = false;
    }
    var atomPat=new RegExp(atom,"g");
    var domArr=domain.match(atomPat);
    var len=domArr.length;
    if (domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>3) {
	    // the address must end in a two letter or three letter word.
    //	return ("Email address 結尾應該是一個三個字母的 domain 名稱, 或是二個字母的國家名稱\n請重試一次");
    //	return ("這個 Email address 不正確\n請重試一次");
	    blSymbol = false;

    }

    // Make sure there's a host name preceding the domain.
    if (len<2) {
    //	var errStr="Email address 缺少 hostname!\n請重試一次";
    //	var errStr = "這個 Email address 不正確\n請重試一次";
    //	return (errStr);
	    blSymbol = false;

    }
    // If we've gotten this far, everything's valid!
    //return "";
	return blSymbol;
}

//CheckUniNumber--傳回該Text是否為有效的統一編號
//objText：該Text的物件//uninumCheck：傳回值，為一布林值function CheckUniNumber(objText) {
	var uninumCheck = true;
	if (!IsNumber(objText,"int")){
		uninumCheck = false;
	}
	else{
		var strUniNo = objText.value;
		var B = new Array(8), D = new Array(8);

		for (var i = 0; i <= strUniNo.length - 1; i ++) {
			if ((i == 0) || (i == 2) || (i == 4) || (i == 7))
				B[i] = parseInt(strUniNo.substr(i, 1))
			else if ((i == 1) || (i == 3) || (i == 5))
				B[i] = parseInt(strUniNo.substr(i, 1)) * 2
			else B[i] = parseInt(strUniNo.substr(i, 1)) * 4;

			D[i] = Math.floor(B[i] / 10) + (B[i] % 10);
		}

		if (((D[0]+D[1]+D[2]+D[3]+D[4]+D[5]+D[6]+D[7]) % 10) == 0)
			uninumCheck = true
		else if (((D[0]+D[1]+D[2]+D[3]+D[4]+D[5]+ 1 +D[7]) % 10) == 0)
			uninumCheck = true
		else uninumCheck = false;
	}
	return uninumCheck;
}

//CheckID--傳回該Text是否為有效的身份證字號//objText：該Text的物件function CheckID(objText){
    var blSymbol = true;
    var letter=new Array(34) //第一碼字母的識別庫
    letter[10]="A"
    letter[11]="B"
    letter[12]="C"
    letter[13]="D"
    letter[14]="E"
    letter[15]="F"
    letter[16]="G"
    letter[17]="H"
    letter[18]="J"
    letter[19]="K"
    letter[20]="L"
    letter[21]="M"
    letter[22]="N"
    letter[23]="P"
    letter[24]="Q"
    letter[25]="R"
    letter[26]="S"
    letter[27]="T"
    letter[28]="U"
    letter[29]="V"
    letter[30]="X"
    letter[31]="Y"
    letter[32]="W"
    letter[33]="Z"
    letter[34]="I"
    letter[35]="O"
    var idStr = objText.value.toUpperCase();
	var idChar = idStr.charAt(0);
	if (idChar >='0' && idChar <= '9')  //外國人ID不檢查
	    return true;
	if (idStr.trim() =='')  //空值不檢查
	    return true;
    idStr = idStr.trim();
    if(idStr=="" || GetLengthFromStr(idStr) > 10){
		 return true;
	}
	if(idStr=="" || GetLengthFromStr(idStr) != 10){
		blSymbol = false;
	}
	else{
		if( !((idChar >='A' && idChar <= 'Z') ) ){ //&& (idChar !='I') && (idChar !='O')
			blSymbol = false;
		}
		for(var i=1;i<idStr.length;i++){
			var idOtherChar = idStr.charAt(i);
			//alert(idOtherChar);
			if(!(idOtherChar >='0' && idOtherChar <= '9')){
				blSymbol = false;
				break;
			}
		}
		
		var num=new Array(10);              //11個數字的陣列
		var sum=0;                          //放乘好加總的值
		for (i=10;i<=35;i++)
        if(letter[i]==idStr.substring(0,1)){
			num[0]=Math.floor(eval(i/10));
			num[1]=eval(i%10);
        }
       
        for (i=1;i<=9;i++){
			num[i+1]=parseInt(idStr.substring(i,i+1));
        }                                                  //80
        for (i=0;i<=10;i++){
			if(i==0){
				sum=num[i];
			}
			else{
				sum=eval(sum+(num[i]*(10-i)));
			}
        }
         
		if (((parseInt(idStr.substring(9,10)))==(eval(10-(sum%10)))) || (((parseInt(idStr.substring(9,10)))==0) && (eval(10-(sum%10))))){
        
        }
        else{
			//alert("1111")
			blSymbol = false;
        }
	}
	return blSymbol;
}

//IsEmpty--傳回該Text的值是否為空值(或空白)
//objText：該Text的物件
function IsEmpty(objText){
	var blSymbol = false;
	var strEmpty = objText.value;
	if(strEmpty.length == 0){
		blSymbol = true; 
	}
	else{
		var j=0;
		for(var i=0; i<strEmpty.length; i++){
			if(strEmpty.charAt(i) == " "){
				//alert("1111");
				j=j+1;
			}
		}
		if(strEmpty.length == j){
			blSymbol = true; 
		}
	}
	return blSymbol; 
}

//GetBrowser--傳回使用者的瀏覽器類型，版本
//browserStr：傳回值，為一字串，(類型+","+版本)(用","分隔)
function GetBrowser(){
	var browserName = navigator.appName;
	var browserVersion = navigator.appVersion;
	var browserStr;
	
	if(browserName == "Microsoft Internet Explorer"){
		browserName="IE";
		var browserTypes = browserVersion.split(";");
		var browserType = browserTypes[1];
		browserVersion = browserType.substring(5);
	}
	
	if(browserName == "Netscape"){
		browserName="NS";
		browserVersion=parseFloat(browserVersion);
	}
		
	
	browserStr = browserName + ',' + browserVersion;
	return browserStr;
	
}

//IsOpenCookie--傳回使用者是否有打開cookie
//checkCookie：傳回值，為一布林值
function IsOpenCookie(){
	var checkCookie = navigator.cookieEnabled;
	//alert(checkCookie);
	return checkCookie;
}

//setHPage--設為首頁
//obj：傳入的物件
function setHPage(obj){
	var hUrl = window.location;
	//alert(jj);
	obj.style.behavior='url(#default#homepage)';
	obj.setHomePage(hUrl);
}

function getCookie(CookieName) {
  var blSymbol = '';
  var sCookieName = CookieName + "=";
  var dc = document.cookie;
  //alert(dc)
  var begin, end;
	//alert(cookiename);
  if (dc.length > 0) {
      begin = dc.indexOf(sCookieName);
      if (begin != -1) {
          begin += sCookieName.length;
          end = dc.indexOf(";", begin);
          if (end == -1) {
              end = dc.length;
          }

          blSymbol = unescape(dc.substring(begin, end));
      } 
  }
  return blSymbol;
}

function setCookie(CookieName, CookieValue, CookieExpire, CookieDomain, CookiePath){
	var sCookieName 	= CookieName;
	var sCookieValue 	= CookieValue;
	var sCookieExpire = CookieExpire;
	var sCookieDomain = CookieDomain;
	var sCookiePath 	= CookiePath;
	var expDay, setDay
	if(sCookieExpire!=''){
		sCookieExpire = eval(sCookieExpire);
		setDay = new Date();
		setDay.setTime(setDay.getTime()+(sCookieExpire*1000*60*60*24));
		expDay = setDay.toGMTString();
	}
	if(sCookiePath==''){
		sCookiePath = '/';
	}
  document.cookie = sCookieName + "=" + escape (sCookieValue) +
    ((sCookieExpire == '') ? "" : ("; expires=" + expDay)) +
    ((sCookiePath == '') ? "" : ("; path=" + sCookiePath)) +
    ((sCookieDomain == '') ? "" : ("; domain=" + sCookieDomain)) ;
}

//************************************************************
// 目的：檢查字串是否為空
//************************************************************
function isEmptyStr(str) 
{
    var len = str.length;	 
	var i = 0;
	var isEmpty = true;
	for (i=0; (i < len) && (isEmpty); i++) {
	    isEmpty = (str.charAt(i) == " ");
	}
	return isEmpty;        
}


//************************************************************
// 目的：檢查是否為潤年
// 傳回：true (潤年), false (非潤年)
//************************************************************
function isLeapYear(strYear)
{
	var blnLeapYear;	
	
	if ((strYear % 400)  == 0 ) {
		blnLeapYear = true;
	}
	else if((strYear % 4) == 0) {
		if((strYear % 100) == 0) {
			blnLeapYear = false
		}
		else {
			blnLeapYear = true;
		}
	}
	else {
		blnLeapYear = false;
	}

	return blnLeapYear;			
}


//************************************************************
// 目的：inRange(strInput, intLow, intUpper)
// 傳回：true (in range), false (out of range)
//************************************************************
function inRange(strInput, intLow, intUpper)
{
	var intNum = parseInt(strInput, 10);
 	
	if ((intNum < intLow) || (intNum > intUpper)) {
		return false;
	}
	else {
		return true;
	} 		
}


//************************************************************
// 目的：檢查是否為正確的日期（民國年）
// 傳回：true (valid), false (invalid)
//************************************************************  
function isCDate(sDate, sDelim){
	var sYear="", sMonth="", sDay="";
	var aryDate;
    var textcheck = /[^0-9]/;
    

		if (sDelim.length==1)
	    {
		        aryDate = sDate.split(sDelim);
		        if(aryDate.length ==3) 
		        {
			        sYear  = aryDate[0];
			        sMonth = aryDate[1];
			        sDay   = aryDate[2];
       			    return isValidCDate(sYear,sMonth,sDay);
		        }		     
		}	
		else if (sDate.length == 7 && sDelim =="" && !textcheck.test(sDate))
		{				   		   
			    sYear = sDate.charAt(0) + sDate.charAt(1) + sDate.charAt(2);
			    sMonth = sDate.charAt(3) + sDate.charAt(4);
			    sDay = sDate.charAt(5) + sDate.charAt(6);
			    return isValidCDate(sYear,sMonth,sDay);
	    } 		
		
	    return false;					
}



//************************************************************
// 目的：檢查是否為正確的日期（民國年）
// 傳回：true (valid), false (invalid)
//************************************************************  
function isValidCDate(strYear, strMonth, strDay) 
{
	var intYear = parseInt(strYear, 10) + 1911;
	strYear = "" + intYear;
	return isValidDate(strYear, strMonth, strDay);
}


//************************************************************
// 目的：檢查是否為正確的日期（西元年）
// 傳回：true (valid), false (invalid)
//************************************************************  
function isDate(sDate, sDelim){
	var sYear="", sMonth="", sDay="";
	var aryDate;
    var textcheck = /[^0-9]/;
    
    if(sDelim.length==1 )
    {
        aryDate = sDate.split(sDelim);
		if(aryDate.length ==3) 
		{
		     sYear  = aryDate[0];
			 sMonth = aryDate[1];
			 sDay   = aryDate[2];
    	     return isValidDate(sYear, sMonth, sDay);  
		}
    }  
   else  if (sDate.length == 8 && sDelim =="" && !textcheck.test(sDate))  
    {    
	     sYear = sDate.charAt(0) + sDate.charAt(1) + sDate.charAt(2)+sDate.charAt(3);
		 sMonth = sDate.charAt(4) + sDate.charAt(5);
		 sDay = sDate.charAt(6) + sDate.charAt(7);	  	  	     		  	  	    
	     return isValidDate(sYear, sMonth, sDay); 
    } 
	
	return false;				
	
}


//************************************************************
// 目的：檢查是否為正確的日期（西元年）
// 傳回：true (valid), false (invalid)
//************************************************************
function isValidDate(strYear, strMonth, strDay) 
{

	// empty string
	if (isEmptyStr(strYear) || isEmptyStr(strMonth) || isEmptyStr(strDay)) {
		return false;
	}
	
	// numeric
	if (isNaN(strYear) || isNaN(strMonth) || isNaN(strDay)) {
		return false;	
	}
	
	var intYear = parseInt(strYear, 10);
	var intMonth = parseInt(strMonth, 10);
	var intDay = parseInt(strDay, 10);
	
	if (!inRange(intMonth, 1, 12)) {
		return false;
	}
	
	var aryMonthDay = new Array(12);
		
	aryMonthDay[0] = 31; aryMonthDay[1] =  (isLeapYear(strYear) ? 29 : 28); aryMonthDay[2] = 31; aryMonthDay[3] = 30;
	aryMonthDay[4] = 31; aryMonthDay[5] = 30; aryMonthDay[6] = 31; aryMonthDay[7] = 31;
	aryMonthDay[8] = 30; aryMonthDay[9] = 31; aryMonthDay[10] = 30; aryMonthDay[11] = 31;
		 
	if (!inRange(intDay, 1, aryMonthDay[intMonth-1])) {
		return false;
	} 
	 
	return true;
}

//************************************************************
// 目的：檢查是否合法的手機號碼
// 傳回：true (valid), false (invalid)
// 適用控制項:TextBox
//************************************************************
function isMobile(objText)
{
    var checkMobile = true;
    var objTextValue=trim(objText.value);
    if(objTextValue.length!=0 && objTextValue.length!=10){
        checkMobile = false;
    }
    else{
        var myreg = /^(0\d{9})$/;
        if(!myreg.test(objTextValue)){
            checkMobile = false;
        }
    }
    return checkMobile;
}

//************************************************************
// 目的：檢查是否合法的手機號碼
// 傳回：true (valid), false (invalid)
// 適用控制項:DropDownList
//************************************************************
function isMobileDLL(objDDL)
{
    var checkMobile = true;
    var val = '';
    var objValue = '';
    
    for (i=0;i<objDDL.options.length;i++)	
    {
        var current = objDDL.options[i]; 
        if (current.selected)
        { 
		    val = current.value;
        }
    }
    if (val != '')
    {
        objValue=trim(val);
        if(objValue.length!=0 && objValue.length!=10){
            checkMobile = false;
        }
        else{
            var myreg = /^(0\d{9})$/;
            if(!myreg.test(objValue)){
                checkMobile = false;
            }
        }
    }
    else
    {// 沒有選擇任何項目(objDDL.SelectedIndex = -1時)
        checkMobile = false;
    }
    return checkMobile;
}

//************************************************************
// 目的：去除文字前後的空白
//************************************************************
    function trim(iStr) 
    {
    	while (iStr.substr(0,1) == ' ') 
	    {
		    iStr = iStr.substr(1,iStr.length-1);
	    }
	    while (iStr.substr(iStr.length-1,1) == ' ')
	    {
		    iStr = iStr.substr(0,iStr.length-1);
	    }
	    return iStr;
    }
    
//************************************************************
// 目的：去除文字的空白
//************************************************************
String.prototype.trim = function() 
{ 
return this.replace(/(^\s*)|(\s*$)/g, ""); 
} 

String.prototype.lTrim = function() 
{ 
return this.replace(/(^\s*)/g, ""); 
} 

String.prototype.rTrim = function() 
{ 
return this.replace(/(\s*$)/g, ""); 
} 

String.prototype.left = function(lngLen) 
{
    if (lngLen>0) 
        return this.substring(0,lngLen)
    else
        return null
}

String.prototype.right = function(lngLen) 
{
    if (this.length-lngLen>=0 && this.length>=0 && this.length-lngLen<=this.length) 
        return this.substring(this.length-lngLen,this.length)
    else
        return null
}

  function rDT(txt)
  { 
    var str=txt.value;
    str=str.replace(' ','')
    str=str.replace('/','').replace('/','')
    if (str.length==8)
     {txt.value=str.substring(0,4)+'/'+str.substring(4,6)+'/'+str.substring(6,8);}
    else if(str.length==7)
    { 
        var txtYear =parseInt(str.substring(0,3))+1911;
        txt.value=txtYear+'/'+str.substring(3,5)+'/'+str.substring(5,7);    
    }
    else if(str.length==6)
    { 
        var txtYear =parseInt(str.substring(0,2))+1911;
        txt.value=txtYear+'/'+str.substring(2,4)+'/'+str.substring(4,6);    
    }
  }
//寫入檔案js(C:\CTCP\js.log)
function WriteToFile(text) {

	try {
			var dateObj = new Date();
			var tempdate ='';
			var temptime ='';
			tempdate = tempdate + dateObj.getYear() +'/';
			tempdate = tempdate + ( dateObj.getMonth() + 1 ) +'/' ;
			tempdate = tempdate + dateObj.getDate() ;
			temptime = temptime + dateObj.getHours() + ":";
			temptime = temptime + dateObj.getMinutes()+ ":";
			temptime = temptime + dateObj.getSeconds();
			text=tempdate + ' ' + temptime + ' ' + text;
			var FolderUrl = "C:\\CTCP\\" ;
			var fileUrl = "js.log" ;
			var newUrl = FolderUrl + fileUrl;
			fso = new ActiveXObject("Scripting.FileSystemObject");
			
			if (fso.FileExists(newUrl)){
					var ForAppending = 8; 
				    var f2 = fso.OpenTextFile(newUrl,ForAppending, false); 
				    return WriteTxt(f2,text);			    	
		    }
		    else
		    {
				try
				{
					fso.CreateFolder(FolderUrl);
					var f1 = fso.CreateTextFile(newUrl, true);
					return WriteTxt(f1,text);
				}	
				catch(e)
				{
					var f1 = fso.CreateTextFile(newUrl, true);
					return WriteTxt(f1,text);				
				}
		    }
		}
		catch (e) {
			if (e.description.indexOf("伺服程式無法產生物件") != -1 ) {
                   alert("請調整IE瀏覽器的安全性\n網際網路選項\n安全性\n自訂層級\n「起始不標示為安全的ActiveX控制項」設定為啟用。");
				   return false
                }
			else  {
				return false
			}
		} 
}

//新增資料夾並寫入檔案
function CearFolder(fso,text,FolderUrl,newUrl) {
    
    try {
				fso.CreateFolder(FolderUrl);
				var f1 = fso.CreateTextFile(newUrl, true);
				return WriteTxt(f1,text);
		}
    catch (e) {
		return false
    }               
}
//寫入檔案
function WriteTxt(f2,text)
{
	try {
		if (f2 !=null ) 
		{
			f2.WriteLine(text) ; 
			f2.Close();
			return true  
		}
	}
    catch (e) 
	{
		return false
    }  
}

function checktoolbar(ToolBar) {
    try {
            if (typeof('setButtoinDisabledFor_' + ToolBar ) != "string")
            {
               WriteToFile('正常的javascript');	
            }
            else
            {
               WriteToFile('有問題的javascript');
            }
    }
    catch (e){

    }

}

String.prototype.endsWith = function(str) { return (this.match(str + "$") == str) }