﻿function GirdViewchangeMode_1(type,fieldsetID,btnConfirmID,btnCancelID,tabIndex,mode)
{
    var Detail = document.getElementById(fieldsetID);
    var Confirmbtn= document.getElementById(btnConfirmID);
    var Cancelbtn= document.getElementById(btnCancelID);
    Detail.disabled=false;
    Confirmbtn.style.visibility="visible";
    Cancelbtn.style.visibility="visible"; 
    if(mode=="add")
    {
        Confirmbtn.innerText="新增確認";
    }
    else
    {
        Confirmbtn.innerText="修改確認";
    }
    switch(type)
    {
        case 0:
            break;

//        case 1:
//            GirdViewshowTab_1(fieldsetID,tabIndex);
//            break;
        
        case 2:
            GirdViewshowTab_1(fieldsetID,tabIndex);
            break;
    }
}

function GirdViewshowTab_1(tabcont,tabIndex)
{
    var ctrl1 = $find(tabcont);
    ctrl1.set_activeTabIndex(tabIndex);
}

function GirdViewchangeColorCheckBox(control) {
    if (control.checked == true) {
        control.parentElement.parentElement.style.backgroundColor='#a0ffff';
        //control.parentElement.parentElement.style.backgroundImage.link='../Img/event_on.gif';
    }
    else {
        control.parentElement.parentElement.style.backgroundColor='';
    }
}

function setCheckBoxCount(obj, countObj)
{
    if (obj.checked)
        countObj.value= parseInt(countObj.value)+1;
    else
        countObj.value=parseInt(countObj.value)-1;
}   

function deleteConfirm(objName)
{
    var obj=document.getElementById(objName);
    if (obj!=null) {
        if (parseInt(obj.value) > 0)
        {
            return confirm('是否真的要刪除');
        }
        else
        {
            alert("請先勾選欲刪除之項目");
            return false;
        }
    }
}