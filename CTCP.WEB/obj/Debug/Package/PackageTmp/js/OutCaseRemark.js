// JScript 檔
function changeEnd(sel)
{ }
// 當Ajax:TabControl改變顯示的TabPanel時，就用隱藏欄位記錄下來。
//function TabAreaActiveTabChanged(sender, e)
//{
//if(sender == null) return;

//    var activeTabIndex = sender._activeTabIndex;
//    document.getElementById('aspHiddenFieldTabControlIndex').value = activeTabIndex;
//}
//function Tab2AreaActiveTabChanged(sender, e)
//{
//if(sender == null) return;

//    var activeTabIndex = sender._activeTabIndex;
//    var txt = document.getElementById('aspHiddenFieldTabControlIndexValue').value;
//    var index = parseInt(document.getElementById('aspHiddenFieldTabControlIndex2').value);
//    if(activeTabIndex==2 && txt == "DEBT" )
//    {
//        showJSMessageFor_aspSysMessageMaster(msgDebt);
//        sender.set_activeTabIndex(index);

//    }
//    else if(activeTabIndex==1 && txt == "GRID" )
//    {        
//        showJSMessageFor_aspSysMessageMaster(msgGrid);
//        sender.set_activeTabIndex(index);      
//    }
//    document.getElementById('aspHiddenFieldTabControlIndex2').value = sender._activeTabIndex;

//}


function IsEmpty(objText) {

    var blSymbol = false;
    //	var strEmpty = objText.value;
    var strEmpty = objText;
    if (strEmpty.length == 0 || strEmpty == "0") {
        blSymbol = true;
    }
    else {
        var j = 0;
        for (var i = 0; i < strEmpty.length; i++) {
            if (strEmpty.charAt(i) == " ") {
                //showJSMessageFor_aspSysMessageMaster("1111");
                j = j + 1;
            }
        }
        if (strEmpty.length == j) {
            blSymbol = true;
        }
    }
    return blSymbol;
}


function showRecor(b) {
    var divR = document.getElementById('divREC');
    if (divR != null)
        if (divR.style.display == 'block')
    { divR.style.display = 'none'; }
    else
    { divR.style.display = 'block'; }

}


function showClick(rb, num) {
    var dAcc = document.getElementById('divACC');
    if (dAcc != null) {
        dAcc.style.display = 'none';
        if (num == 1) {
            if (rb.checked)
            { dAcc.style.display = 'block' }
        }
    }
}

function checkinboundid() {
    var item = document.getElementById('aspTextBoxIDIn');
    if (CheckID(item))
        return true;
    else {
        showJSMessageFor_aspSysMessageMaster(msgIdNoError);
        item.select();
        return false;
    }
}


function checkEn() {
    var obj1 = document.getElementById('aspRadioButtonListFChoice');
    var obj2 = document.getElementById('aspDropDownListDdlAccCode');
    var obj3 = document.getElementById('aspDropDownListDdlMidAccCode');
    var obj4 = document.getElementById('aspDropDownListDdlDetlAccCode');
    var obj5 = document.getElementById('aspAutoComboTextActConPeo');
    var obj6 = document.getElementById('HiddenFieldConnTypeId');
    var obj7 = document.getElementById('aspCheckBoxFCTypeAddr');
    var obj8 = document.getElementById('aspCheckBoxFCTypeTel');
    //PTP
    var obj9 = document.getElementById('aspTextBoxDtPay1');
    var obj10 = document.getElementById('aspTextBoxTxtPtpAmt');
    var obj11 = document.getElementById('aspDropDownListDdlPtpPamttype');
    //CallBack
    var obj12 = document.getElementById('aspTextBoxDtPay2');
    var obj13 = document.getElementById('aspTextBoxTxtCBTime');
    var today = new Date();
    var month = today.getMonth() + 1;
    var H = today.getHours().toString();
    var M = today.getMinutes().toString();
    if (today.getHours().toString().length == 1)
        H = "0" + today.getHours().toString();
    if (today.getMinutes().toString().length == 1)
        M = "0" + today.getMinutes().toString();
    var time = H + ":" + M;
    today = today.getYear() + "/" + month + "/" + today.getDate();

    var todayAfter1y = new Date();
    todayAfter1y = (parseInt(todayAfter1y.getYear(), 10) + 1) + "/" + month + "/" + todayAfter1y.getDate();


    if (obj1 != null && !obj1.disabled && obj1.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode0); return false;
    }
    if (obj5 != null && !obj5.disabled && obj5.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectTarget); return false;
    }
    if ((obj7 != null && !obj7.disabled && obj8 != null && !obj8.disabled && obj6 != null && obj6.value == "") || ((obj7 != null && !obj7.disabled && !obj7.checked) && (obj8 != null && !obj8.disabled && !obj8.checked))) {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectType); return false;
    }

    if (obj2 != null && obj2.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode1); return false;
    }
    if (obj3 != null && obj3.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode2); return false;
    }
    if (obj4 != null && obj4.value == "0") {
        showJSMessageFor_aspSysMessageMaster(msgSv1SelectActionCode3); return false;
    }
    var check_ptp = false;
    //PTP
    if (obj9 != null && trim(obj9.value) != "") {
        if (!isDate(obj9.value, "/"))
        { showJSMessageFor_aspSysMessageMaster(msgPTP1); return false; }
        else {
            if (Date.parse(obj9.value) < Date.parse(today)) {
                showJSMessageFor_aspSysMessageMaster(msgPTP6); return false;
            }
            if (Date.parse(obj9.value) > Date.parse(todayAfter1y)) {
                showJSMessageFor_aspSysMessageMaster(msgPTP7); return false;
            }
        }
        check_ptp = true;
    }
    if (obj10 != null && trim(obj10.value) != "") {
        if (!IsNumber(obj10, "int"))
        { showJSMessageFor_aspSysMessageMaster(msgPTP2); return false; }
        check_ptp = true;
    }
    if (obj11 != null && obj11.value != "0") {
        check_ptp = true;
    }
    if (check_ptp) {
        if (obj9 != null && trim(obj9.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgPTP4); return false;
        }
        if (obj10 != null && trim(obj10.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgPTP5); return false;
        }
        if (obj11 != null && obj11.value == "0") {
            showJSMessageFor_aspSysMessageMaster(msgPTP3); return false;
        }
    }
    //CallBack
    var check_CallBack = false;
    if (obj12 != null && trim(obj12.value) != "") {
        if (!isDate(obj12.value, "/"))
        { showJSMessageFor_aspSysMessageMaster(msgCallBack1); return false; }
        else {
            if (Date.parse(obj12.value) < Date.parse(today)) {
                showJSMessageFor_aspSysMessageMaster(msgCallBack6); return false;
            }
            if (Date.parse(obj12.value) > Date.parse(todayAfter1y)) {
                showJSMessageFor_aspSysMessageMaster(msgCallBack7); return false;
            }
        }
        check_CallBack = true;
    }
    if (obj13 != null && trim(obj13.value) != "") {
        if (!isTime(obj13.value))
        { showJSMessageFor_aspSysMessageMaster(msgCallBack2); return false; }
        else {
            var _t1 = obj13.value.split(":");
            var regexs = /^(-?[0-9]+[0-9]*|0|00)$/;
            if (!(parseInt(_t1[0], 10) <= 22 && parseInt(_t1[1], 10) >= 0) || !(parseInt(_t1[1], 10) <= 59 && parseInt(_t1[0], 10) >= 7)) {
                showJSMessageFor_aspSysMessageMaster(msgCallBack5); return false;
            }
        }
        check_CallBack = true;
    }
    if (check_CallBack) {
        if (obj12 != null && trim(obj12.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgCallBack3); return false;
        }
        if (obj13 != null && trim(obj13.value) == "") {
            showJSMessageFor_aspSysMessageMaster(msgCallBack4); return false;
        }
        if ((Date.parse(obj12.value) == Date.parse(today)) && obj13.value < time) {
            showJSMessageFor_aspSysMessageMaster(msgCallBack6); return false;
        }
    }
    var btn = document.getElementById('aspButtonBtn1');
    btn.disabled = true;
    __doPostBack('aspButtonBtn1', '');
    //    return true;
}

function isTime(t1) {
    var _t1 = t1.split(":");
    var regexs = /^(-?[0-9]+[0-9]*|0|00)$/;
    if (trim(t1) != '' && (_t1.length != 2 || _t1[0].length != 2 || _t1[1].length != 2 || !regexs.test(_t1[0]) || !regexs.test(_t1[1]) || !(parseInt(_t1[0], 10) <= 23 && parseInt(_t1[1], 10) >= 0) || !(parseInt(_t1[1], 10) <= 59 && parseInt(_t1[0], 10) >= 0))) {
        return false;
    }
    return true;
}
function reEndReason(sel) {
    if (document.getElementById('aspDropDownListDdlEndAcc') == null)
        return false;
    var txt = sel.options(sel.selectedIndex).value;
    var txt1 = sel.options(sel.selectedIndex).text;
    document.getElementById('aspHiddenField4').value = txt;
    document.getElementById('aspHiddenField5').value = txt1;
    switch (txt.split(",")[1]) {
        case "Y":
            document.getElementById('aspDropDownListDdlEndAcc').disabled = "disabled";
            break;
        case "N":
            document.getElementById('aspDropDownListDdlEndAcc').disabled = false;
            break;
    }
}

function checkEnd() {

    var seAcc = document.getElementById('aspDropDownListDdlEndAcc')
    var seEnd = document.getElementById('aspDropDownListDdlEnd')
    var seReson = document.getElementById('aspSelectDdlt')
    if (seAcc == null)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectcAcctStop); return false; }
    if (!seAcc.disabled) {
        var len = seAcc.length;
        var checked = false;
        for (i = 0; i < seAcc.all.tags('input').length; i++) {
            if (seAcc.all.tags('input')[i].type == 'checkbox' && seAcc.all.tags('input')[i].checked) {
                checked = true;
                break;
            }
        }

        if (!checked)
        { showJSMessageFor_aspSysMessageMaster(msgSv1SelectAccount); return false; }
    }
    if (seEnd.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectStop); return false; }
    if (seReson.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectStopCause); return false; }

    var btn = document.getElementById('aspButton1');
    btn.disabled = true;
    __doPostBack('aspButton1', '');
    //   return true;
}
function checkLega() {
    var seAcc = document.getElementById('CheckBoxList1')
    var seLega = document.getElementById('CheckBoxList2')
    var seReson = document.getElementById('aspSelectDdlt')
    if (!seAcc)
        return false;
    var len = seAcc.length;
    var checked = false;
    for (i = 0; i < seAcc.all.tags('input').length; i++) {
        if (seAcc.all.tags('input')[i].type == 'checkbox' && seAcc.all.tags('input')[i].checked) {
            checked = true;
            break;
        }
    }

    if (!checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectAccount); return false; }
    checked = false;
    var len = seLega.length;
    for (i = 0; i < seLega.all.tags('input').length; i++) {
        if (seLega.all.tags('input')[i].type == 'checkbox' && seLega.all.tags('input')[i].checked) {
            checked = true;
            break;
        }
    }

    if (!checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectLega); return false; }

    return true;
}
function checkRecord() {
    var count = 0;
    var se1 = document.getElementById('aspCheckBoxCbNote');
    var se2 = document.getElementById('aspCheckBoxCbMess');
    var se3 = document.getElementById('aspCheckBoxCbLetter');
    var se4 = document.getElementById('aspCheckBoxCbFind');
    var se5 = document.getElementById('aspCheckBoxCbVisit');
    var se6 = document.getElementById('aspCheckBoxCbLegaProc');
    var se7 = document.getElementById('aspCheckBoxCbPP');
    var se8 = document.getElementById('aspCheckBoxCbCard');
    var se9 = document.getElementById('aspCheckBoxCbStopCall');
    var se10 = document.getElementById('aspCheckBoxCbAMNote');
    var se11 = document.getElementById('aspCheckBoxCbOut');
    var checked = false;
    if (se1 && se1.checked) {
        checked = true;
        count = count + 1;
    }
    if (se2 && se2.checked) {
        checked = true;
        count = count + 1;
    }
    if (se3 && se3.checked) {
        checked = true;
        count = count + 1;
    }
    if (se4 && se4.checked) {
        checked = true;
        count = count + 1;
    }
    if (se5 && se5.checked) {
        checked = true;
        count = count + 1;
    }
    if (se6 && se6.checked) {
        checked = true;
        count = count + 1;
    }
    if (se7 && se7.checked) {
        checked = true;
        count = count + 1;
    }
    if (se8 && se8.checked) {
        checked = true;
        count = count + 1;
    }
    if (se9 && se9.checked) {
        checked = true;
        count = count + 1;
    }
    if (se10 && se10.checked) {
        checked = true;
        count = count + 1;
    }
    if (se11 && se11.checked) {
        checked = true;
        count = count + 1;
    }
    if (se1 && se1.checked && se10 && se10.checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectRecord1); return false; }
    if (count > 3)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectRecord2); return false; }
    if (!checked)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectRecord); return false; }

    return true;
}
function checkPtP() {
    var se = document.getElementById('aspDropDownListDdlAcc')
    if (se.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectAccount); return false; }
    se = document.getElementById('aspDropDownListDdlPtpPamttype')
    if (se.selectedIndex == 0)
    { showJSMessageFor_aspSysMessageMaster(msgSv1SelectFashion1); return false; }
    if (IsEmpty(document.getElementById('aspTextBoxDtPay7')))
    { showJSMessageFor_aspSysMessageMaster(msgSv1FillInDate); return false; }
    if (IsEmpty(document.getElementById('aspTextBoxTxtPtpTime')))
    { showJSMessageFor_aspSysMessageMaster(msgSv1FillInTime); return false; }
    if (IsEmpty(document.getElementById('aspTextBoxTxtPtpAmt')))
    { showJSMessageFor_aspSysMessageMaster(msgSv1FillInMoney); return false; }
    return true;
}

function getS(con, ind)
{ document.getElementById(con).options[ind].selected = true; }

function showDataDetl() {
    var d = document.getElementById('divDataDetl')
    if (d.style.display == 'none')
    { d.style.display = 'block'; }
    else
    { d.style.display = 'none'; }

}

function rTime(txt) {
    var str = txt.value
    if (str.length == 4) {
        txt.value = str.substring(0, 2) + ':' + str.substring(2, 4)
    }
}

function EndRequestHandler() {
    var rtn = true;
    try {
        rtn = WriteToFile('HiddenFieldCTI=' + document.getElementById('HiddenFieldCTI').value);
        if (document.getElementById('HiddenFieldCTI').value == "send") {

            var b1 = document.getElementById('HiddenFieldb1').value
            var b2 = document.getElementById('HiddenFieldb2').value
            var b3 = document.getElementById('HiddenFieldb3').value
            var b4 = document.getElementById('HiddenFieldb4').value
            var b5 = document.getElementById('HiddenFieldb5').value

            var b7 = document.getElementById('HiddenFieldb7').value
            var b8 = document.getElementById('HiddenFieldb8').value
            var b9 = document.getElementById('HiddenFieldb9').value
            var b10 = document.getElementById('HiddenFieldb10').value
            var b11 = document.getElementById('HiddenFieldb11').value

            var b6 = document.getElementById('HiddenFieldb6').value
            var b12 = document.getElementById('HiddenFieldb12').value
            var b13 = document.getElementById('HiddenFieldb13').value
            var b14 = document.getElementById('HiddenFieldb14').value
            document.getElementById('HiddenFieldCTI').value = "";

            rtn = WriteToFile(b1 + '-' + b2 + '-' + b3 + '-' + b4 + '-' + b5 + '-' + b7 + '-' + b8 + '-' + b9 + '-' + b10 + '-' + b11 + '-' + b6 + '-' + b12 + '-' + b13 + '-' + b14 + " : InsertContactLog finish!");
            window.external.InsertContactLog(b1, b2, b3, b4, b5, b7, b8, b9, b10, b11, b6, b12, b13, b14);
            //showJSMessageFor_aspSysMessageMaster(msgCTIS3);
            //showJSMessageFor_aspSysMessageMaster(msgCTIS1);
        }
    }
    catch (err) {
        //        try {
        rtn = WriteToFile('InsertContactLog Error:' + err.message);
        window.external.InsertContactLog(b1, b2, b3, b4, b5, b7, b8, b9, b10, b11, b6, b12, b13, b14);
        return false;
        //      showJSMessageFor_aspSysMessageMaster(msgCTIS2);
        //      showJSMessageFor_aspSysMessageMaster(msgCTIS1);
        //        } catch (err) {
        //            rtn = WriteToFile('InsertContactLog Error2:' + err.message);
        //        }
    }
}

function showSV() {

    var txt = document.getElementById('aspHiddenFieldShowSV1');
    var count = document.getElementById('aspHiddenFieldShowCase');
    var txt1 = document.getElementById('aspHiddenFieldShowSV1ShareWork');
    var txtBound = document.getElementById('aspHiddenFieldShowSV1ShareWorkBound');
    var message = '切換ID將會關閉所有的作業\r並進入' + txt.value + '的案件\r確定切換嗎？';
    if (txtBound && txtBound.value == "Inbound") {
        if (!confirm(message)) {
            return false;
        }
    }
    if (txt.value != "") {
        var head = parent.parent.document.frames.item('FrameHead');
        var custId = head.document.getElementById("hid_CustID");
        var Bound = head.document.getElementById("hid_Bound");
        custId.value = txt.value;
        Bound.value = txtBound.value;
        var txtURL = head.document.getElementById("hid_URL");
        var txtURLNAME = head.document.getElementById("hid_URLNAME");

        head.document.form1.submit();
        //        var URL = "sv1.aspx" ;
        //        var URLNAME = "催收主畫面" ;
        //        txtURL = URL;
        //        txtURLNAME = URLNAME;

        //        var funname = "ChangeID('" + URL + "','" + URLNAME + "')";
        //        parent.execScript(funname);
        //切換工作清單下一筆
        parent.execScript("changeNext('" + txt.value + "','" + count.value + "')");
        txt.value = "";
    }
    else if (txt1.value != "") {
        var head = parent.parent.document.frames.item('FrameHead');
        var custId = head.document.getElementById("hid_CustID");
        var txtURL = head.document.getElementById("hid_URL");
        var txtURLNAME = head.document.getElementById("hid_URLNAME");
        custId.value = txt1.value;
        head.document.form1.submit();
        //        var URL = "sv1.aspx" ;
        //        var URLNAME = "催收主畫面" ;
        //        txtURL = URL;
        //        txtURLNAME = URLNAME;
        //        var funname = "ChangeID('" + URL + "','" + URLNAME + "')";
        //        parent.execScript(funname);
        txt1.value = "";
    }
}

function showSave() {
    //Amy 0805 呆後規格 
    var txt1 = document.getElementById('aspHiddenFieldChoice').value;
    var txt2 = document.getElementById('aspHiddenFieldCode').value;
    var txt3 = document.getElementById('aspHiddenFieldMidCode').value;
    var txt4 = document.getElementById('aspHiddenFieldDetlCode').value;

    var item2 = document.getElementById('aspDropDownListDdlAccCode');
    var item3 = document.getElementById('aspDropDownListDdlMidAccCode');
    var item4 = document.getElementById('aspDropDownListDdlDetlAccCode');
    item2.options.length = 0;
    item2.options[0] = new Option('--請選擇--', 0);
    item2.disabled = true;

    item3.options.length = 0;
    item3.options[0] = new Option('--請選擇--', 0);
    item3.disabled = true;

    item4.options.length = 0;
    item4.options[0] = new Option('--請選擇--', 0);
    item4.disabled = true;
    if (txt1 != "") {
        try {
            changActionCode1(txt1);
            document.getElementById('aspRadioButtonListFChoice').value = txt1;
            document.getElementById('aspRadioButtonListFChoice').disabled = false;

            if (txt2 != "") {
                changActionCode2(txt1 + txt2);
                item2.value = txt2;
                item2.disabled = false;

                if (txt3 != "") {
                    changActionCode3(txt1 + txt2 + txt3);
                    item3.value = txt3;
                    item3.disabled = false;

                    if (txt4 != "") {
                        item4.value = txt4;
                        item4.disabled = false;
                    }
                }
            }

        }
        catch (err)
        { return false; }


    }
    if (item2.value == "0") {
        if (item2.options.length > 1) {
            try {
                item2.selectedIndex = 1;
                document.getElementById('aspHiddenFieldCode').value = item2.value
                txt2 = document.getElementById('aspHiddenFieldCode').value;
                changActionCode2(txt1 + txt2);
                item3.disabled = false;
            }
            catch (err)
            { return false; }
        }
    }
    document.getElementById('aspCheckBoxFCTypeTel').disabled = false;
    document.getElementById('aspCheckBoxFCTypeAddr').disabled = false;
    //把聯絡種類反灰
    if (txt1 == '04' || txt1 == '06' || txt1 == '07' || txt1 == '08' || txt1 == '09') {
        document.getElementById('aspCheckBoxFCTypeTel').disabled = true;
        document.getElementById('aspCheckBoxFCTypeAddr').disabled = true;
    }
    if (item2.options.length < 2)
        item2.disabled = true;
    if (item3.options.length < 2)
        item3.disabled = true;
    if (item4.options.length < 2)
        item4.disabled = true;

    //結退案
    //     var item = document.getElementById('aspTabPanelTPEndCase').innerHTML;
    //     var sel = document.getElementById('aspDropDownListDdlEnd'); 
    //     
    //     if(item!=null && item.disabled==false)
    //     {changeEnd(sel);

    //         var txt = document.getElementById('aspHiddenField4').value;
    //         if(txt!="")
    //            {document.getElementById('aspSelectDdlt').value=txt;
    //            reEndReason(document.getElementById('aspSelectDdlt'));}
    //     }

}

var divExist = false;
//Show All CIF Information
function popup(type) {

    //var tabPanel = $("aspTabContainer")
    switch (event.button) {
        case 1:
            //alert("你用滑鼠左鍵！");
            break;
        case 2:
            //alert("你用滑鼠右鍵！");
            break;
        case 4:
            //alert("你用滑鼠中鍵！");
            break;
        default:
            //alert("未知的滑鼠鍵！");
            //判斷是否已有該Popup Div
            if (document.getElementById("cloneId")) {
                if (document.getElementById("cloneId").style.visibility == "hidden") {
                    hideSelect(type);
                    document.getElementById("cloneId").style.visibility = "visible";
                    hideShowSelect(false);
                } else {
                    document.getElementById("cloneId").style.visibility = "hidden";
                    hideShowSelect(true);
                    return false;
                }
            }
            var aTitle = new Array("基本資料", "電話", "地址");
            if (!divExist) {
                ClickDIVCIF();
                //    		  var tabCont = $(aspTabContainer); //要複製的頁籤
                var tabCont = document.getElementById("tabCont1")
                var clone = tabCont.cloneNode(true); //複製頁籤

                var element = document.createElement("DIV"); //建立一個新的DIV
                element.id = "cloneId";
                document.body.appendChild(element); //放入DIV
                element.style.position = "absolute";
                element.style.width = "95%";
                element.style.height = "500px";
                element.style.top = "2px";
                element.style.left = "2px";
                element.style.padding = "10px";
                element.style.overflowY = "auto";
                element.style.overflowX = "hidden";
                element.innerHTML = ""
                element.ondblclick = function() { //按兩下關掉
                    document.getElementById("cloneId").style.visibility = "hidden";
                    hideShowSelect(true);
                    return false;
                }
                var index = 0;
                //element.appendChild(clone);
                var divs = clone.getElementsByTagName("DIV"); //抓取所有複製頁籤內的DIV
                var j = 0;
                for (var i = 0; i < divs.length; i++) {
                    if (divs[i].className == "BlockY") { //抓取特定的DIV
                        divs[i].style.height = "auto";
                        divs[i].style.display = 'block';
                        element.innerHTML += aTitle[j]; //放入標題
                        j++;
                        var ttt = divs[i].getElementsByTagName('table'); //抓取DIV內的TABLE
                        ttt[0].id = 'grid' + index; //GridView的ID會被鬼隱，所以另給ID
                        index++;
                        element.appendChild(divs[i].cloneNode(true)); //複製Div
                        element.innerHTML += "<br>";

                    }
                }
                element.style.backgroundColor = "#BDCFBD";  //99cc99,D8F3C9,CCCC99 
                element.style.border = "5px outset";
                element.zIndex = 100;
                divExist = true;
                hideShowSelect(false);
            }
            break;
    }
    hideSelect(type);
}
function hideSelect(type) {
    //判斷RadioButton是否顯示
    var grid1 = "grid1";
    var grid2 = "grid2";
    var txt1 = document.getElementById(grid1);
    var txt2 = document.getElementById(grid2);
    if (txt1 != null) {
        var dbl = txt1.cells[1].children[0].firstChild.cells.length / 9;  //計算RadioButton在Table的位置
        for (var i = 0; i < dbl; i++) {
            var index = 9 * i;
            switch (type) {
                case "0":  //全部隱藏
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "1": //電話
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'block';
                    break;
                case "2": //地址
                    txt1.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
            }
        }
    }
    if (txt2 != null) {
        var dbl = txt2.cells[1].children[0].firstChild.cells.length / 9;   //計算RadioButton在Table的位置
        for (var i = 0; i < dbl; i++) {
            var index = 9 * i;
            switch (type) {
                case "0": //全部隱藏
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "1": //電話
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'none';
                    break;
                case "2": //地址
                    txt2.cells[1].children[0].firstChild.cells[index].style.display = 'block';
                    break;
            }
        }
    }
}
function ClickDIVCIF() {
    try {
        var item = document.getElementById('HiddenFieldConnTypeId');
        var aft_onclick_event = "";
        //        var onclick_event="aspgirdTel_clearRadChoice();";
        //        onclick_event += "aspgirdTel_ShareGridView_ctl02_RadioChoice.checked=true;";
        //        onclick_event += "GirdViewchangeColorCheckBox(this);";
        //        aft_onclick_event = "document.getElementById('aspgirdTel_ShareGridView_ctl02_RadioChoice').checked=true; "
        //        aft_onclick_event = "document.getElementById('HiddenFieldConnTypeId').value='0';";
        aft_onclick_event = "if (document.getElementById('cloneId')){document.getElementById('cloneId').style.visibility='hidden'; hideShowSelect(true);}  ";
        //判斷RadioButton是否顯示
        var grid1 = "aspgirdTel_ShareGridView";
        var grid2 = "aspgridAdd_ShareGridView";
        var txt1 = document.getElementById(grid1);
        var txt2 = document.getElementById(grid2);
        if (txt1 != null) {
            var dbl = txt1.rows.length;  //計算RadioButton在Table的位置
            for (var i = 1; i < dbl; i++) {
                var id = txt1.rows(i).cells(0).getElementsByTagName('input')(0).id;
                var onclick_event = txt1.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value;
                onclick_event = onclick_event + "document.getElementById('" + id + "').checked=true; document.getElementById('HiddenFieldConnTypeId').value='" + (i - 1) + "';" + aft_onclick_event;
                txt1.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value = onclick_event;
            }
        }
        if (txt2 != null) {
            var dbl = txt2.rows.length;  //計算RadioButton在Table的位置
            for (var i = 1; i < dbl; i++) {
                var id = txt2.rows(i).cells(0).getElementsByTagName('input')(0).id;
                var onclick_event = txt2.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value;
                onclick_event = onclick_event + "document.getElementById('" + id + "').checked=true; document.getElementById('HiddenFieldConnTypeId').value='" + (i - 1) + "';" + aft_onclick_event;
                txt2.rows(i).cells(0).getElementsByTagName('input')(0).attributes.item("onclick").value = onclick_event;
            }
        }
    } catch (err) { return false; }
}

var div2Exist = false;
//Show All 帳務 Information
function popup2() {
    //判斷是否已有該Popup Div
    if (document.getElementById("clone2Id")) {
        if (document.getElementById("clone2Id").style.visibility == "hidden") {
            document.getElementById("clone2Id").style.visibility = "visible";
            hideShowSelect(false);
        } else {
            document.getElementById("clone2Id").style.visibility = "hidden";
            hideShowSelect(true);
            return false;
        }
    }
    var aTitle = new Array("綜合帳務", "延滯帳務", "呆帳帳務");
    if (!div2Exist) {
        var tabCont = $(aspTabContainer2);
        var clone = tabCont.cloneNode(true);
        var element = document.createElement("DIV");
        element.id = "clone2Id";
        document.body.appendChild(element);
        element.style.position = "absolute";
        element.style.width = "97%";
        switch (screen.width) {
            case 1024:
                element.style.width = "93%";
        }
        element.style.height = "auto";
        element.style.top = "2px";
        element.style.left = "2px";
        element.style.padding = "10px"
        element.innerHTML = ""
        element.ondblclick = function() {
            document.getElementById("clone2Id").style.visibility = "hidden";
            hideShowSelect(true);
            return false;
        }
        var links = clone.getElementsByTagName("A");
        for (var i = 0; i < links.length; i++) {
            links[i].disabled = true;
        }
        var divs = clone.getElementsByTagName("DIV");
        var j = 0;
        for (var i = 0; i < divs.length; i++) {
            if (divs[i].className == "BlockXY") {
                //alert(j+"|"+i);  
                if (document.getElementById('aspHiddenFieldTabControlIndexValue').value == "GRID" && (j == 1 || j == 0)) {  //NO呆前
                    j++
                    continue;
                }
                if (document.getElementById('aspHiddenFieldTabControlIndexValue').value == "DEBT" && j == 2)  //NO呆帳
                    continue;
                divs[i].style.height = "auto";
                divs[i].style.overflowX = "auto";
                element.innerHTML += aTitle[j];

                j++;
                element.appendChild(divs[i].cloneNode(true));
                element.innerHTML += "<br>";
            }
        }
        element.style.backgroundColor = "#BDCFBD";  //99cc99,D8F3C9,CCCC99 
        element.style.border = "5px outset";
        element.zIndex = 100;
        div2Exist = true;
        hideShowSelect(false);
    }
}


function hideShowSelect(swtch) {
    //    var tabCont = $(aspTabContainer1);
    //    var tabCont = document.getElementById("tabCont1");
    //    var sels = tabCont.getElementsByTagName("select");
    var sels = document.getElementsByTagName("select");
    var j = 0;
    for (var i = 0; i < sels.length; i++) {
        if (swtch) {
            sels[i].style.visibility = "visible";
        } else {
            sels[i].style.visibility = "hidden";
        }
    }
}

var entryZoomHeight = "393";
var entryNormalHeight = "225";

var entryZoomHeight1 = "340";
var entryZoomHeight2 = "173";

var entryZoomHeight3 = "400";
var entryZoomHeight4 = "220";
var multWidth = 0.945;
switch (screen.width) {
    case 1024:
        multWidth = 0.93;
}
var entryZoomWidth = parent.document.body.clientWidth * multWidth;


function showDb() {
    var db = document.getElementById("divENTRY");
    var dc = document.getElementById("divRecord");
    var dd = document.getElementById("divCSI");
    var de = document.getElementById("divCumstormComplain");
    var df = document.getElementById("divJCICQuery");
    var dg = document.getElementById("divFinishCase");
    var dh = document.getElementById("divLA");
    var di = document.getElementById("divJCICApply");
    var dbtxt = document.getElementById("aspHiddenFieldShowDb").value;
    var oDiv = document.getElementById("divSrch");
    var djj = document.getElementById('divRECG');
    var djinbound = document.getElementById('divInbound');
    if (oDiv.style.display == '' || oDiv.style.display == 'block') {
        oDiv.style.display = 'none';
        document.getElementById("aspHiddenFieldShowDb").value = 'none';
        if (db != null) {
            db.style.height = entryZoomHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryZoomHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryZoomHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryZoomHeight;
            de.style.width = entryZoomWidth;
        }
        if (df != null) {
            df.style.height = entryZoomHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryZoomHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryZoomHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryZoomHeight;
            di.style.width = entryZoomWidth;
        }
        if (djj != null)
        { djj.style.height = entryZoomHeight1; }
        if (djinbound != null) {
            djinbound.style.height = entryZoomHeight;
            djinbound.style.width = entryZoomWidth;
        }
    } else {
        oDiv.style.display = 'block';
        document.getElementById("aspHiddenFieldShowDb").value = 'block';
        if (db != null) {
            db.style.height = entryNormalHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryNormalHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryNormalHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryNormalHeight;
            de.style.width = entryZoomWidth;
        }
        if (df != null) {
            df.style.height = entryNormalHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryNormalHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryNormalHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryNormalHeight;
            di.style.width = entryZoomWidth;
        }
        if (djj != null)
        { djj.style.height = entryZoomHeight2; }
        if (djinbound != null) {
            djinbound.style.height = entryNormalHeight;
            djinbound.style.width = entryZoomWidth;
        }
    }
}

function showDb1() {

    var db = document.getElementById("divENTRY");
    var dc = document.getElementById("divRecord");
    var dd = document.getElementById("divCSI");
    var de = document.getElementById("divCumstormComplain");
    var df = document.getElementById("divJCICQuery");
    var dg = document.getElementById("divFinishCase");
    var dh = document.getElementById("divLA");
    var di = document.getElementById("divJCICApply");
    var djj = document.getElementById('divRECG');
    var dbtxt = document.getElementById("aspHiddenFieldShowDb").value;
    var oDiv = document.getElementById("divSrch");
    var djinbound = document.getElementById('divInbound');
    if (dbtxt == 'none') {
        oDiv.style.display = 'none';
        document.getElementById("aspHiddenFieldShowDb").value = 'none';
        if (db != null) {
            db.style.height = entryZoomHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryZoomHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryZoomHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryZoomHeight;
            de.style.width = entryZoomWidth;
        }
        if (df != null) {
            df.style.height = entryZoomHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryZoomHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryZoomHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryZoomHeight;
            di.style.width = entryZoomWidth;
        }
        if (djinbound != null) {
            djinbound.style.height = entryZoomHeight;
            djinbound.style.width = entryZoomWidth;
        }
        if (djj != null)
        { djj.style.height = entryZoomHeight1; }

    } else {
        oDiv.style.display = 'block';
        document.getElementById("aspHiddenFieldShowDb").value = 'block';
        if (db != null) {
            db.style.height = entryNormalHeight;
            db.style.width = entryZoomWidth;
        }
        if (dc != null) {
            dc.style.height = entryNormalHeight;
            dc.style.width = entryZoomWidth;
        }
        if (dd != null) {
            dd.style.height = entryNormalHeight;
            dd.style.width = entryZoomWidth;
        }
        if (de != null) {
            de.style.height = entryNormalHeight;
            de.style.width = entryZoomWidth;
        }
        if (df != null) {
            df.style.height = entryNormalHeight;
            df.style.width = entryZoomWidth;
        }
        if (dg != null) {
            dg.style.height = entryNormalHeight;
            dg.style.width = entryZoomWidth;
        }
        if (dh != null) {
            dh.style.height = entryNormalHeight;
            dh.style.width = entryZoomWidth;
        }
        if (di != null) {
            di.style.height = entryNormalHeight;
            di.style.width = entryZoomWidth;
        }
        if (djinbound != null) {
            djinbound.style.height = entryNormalHeight;
            djinbound.style.width = entryZoomWidth;
        }
        if (djj != null)
        { djj.style.height = entryZoomHeight2; }
    }
}

function showDb2() {
    var db = document.getElementById("divA");
    var dc = document.getElementById("divB");
    var dd = document.getElementById("divC");
    var de = document.getElementById("divD");
    var df = document.getElementById("divE");

    var dbtxt = document.getElementById("aspHiddenFieldShowDb1").value;
    var oDiv = document.getElementById("divSrchEntry");
    if (oDiv.style.display == '' || oDiv.style.display == 'block') {
        oDiv.style.display = 'none';
        document.getElementById("aspHiddenFieldShowDb1").value = 'none';
        if (db != null)
        { db.style.height = entryZoomHeight3; }
        if (dc != null)
        { dc.style.height = entryZoomHeight3; }
        if (dd != null)
        { dd.style.height = entryZoomHeight3; }
        if (de != null)
        { de.style.height = entryZoomHeight3; }
        if (df != null)
        { df.style.height = entryZoomHeight3; }

    } else {
        oDiv.style.display = 'block';
        document.getElementById("aspHiddenFieldShowDb1").value = 'block';
        if (db != null)
        { db.style.height = entryZoomHeight4; }
        if (dc != null)
        { dc.style.height = entryZoomHeight4; }
        if (dd != null)
        { dd.style.height = entryZoomHeight4; }
        if (de != null)
        { de.style.height = entryZoomHeight4; }
        if (df != null)
        { df.style.height = entryZoomHeight4; }

    }
}
function showDb3() {
    var db = document.getElementById("divA");
    var dc = document.getElementById("divB");
    var dd = document.getElementById("divC");
    var de = document.getElementById("divD");
    var df = document.getElementById("divE");

    var dbtxt = document.getElementById("aspHiddenFieldShowDb1").value;
    var oDiv = document.getElementById("divSrchEntry");

    if (dbtxt == 'none') {
        oDiv.style.display = 'none';
        document.getElementById("aspHiddenFieldShowDb1").value = 'none';
        if (db != null)
        { db.style.height = entryZoomHeight3; }
        if (dc != null)
        { dc.style.height = entryZoomHeight3; }
        if (dd != null)
        { dd.style.height = entryZoomHeight3; }
        if (de != null)
        { de.style.height = entryZoomHeight3; }
        if (df != null)
        { df.style.height = entryZoomHeight3; }
    } else {
        oDiv.style.display = 'block';
        document.getElementById("aspHiddenFieldShowDb1").value = 'block';
        if (db != null)
        { db.style.height = entryZoomHeight4; }
        if (dc != null)
        { dc.style.height = entryZoomHeight4; }
        if (dd != null)
        { dd.style.height = entryZoomHeight4; }
        if (de != null)
        { de.style.height = entryZoomHeight4; }
        if (df != null)
        { df.style.height = entryZoomHeight4; }

    }
}
function addWidth() {
    try {
        var multWidth1 = 0.945; //0.96;
        switch (screen.width) {
            case 1024:
                multWidth1 = 0.93; //3; 
        }
        if (document.getElementById("divRecord").style.width != '' && document.getElementById("divRecord").style.width != 0 && document.getElementById("divRecord").style.width.substring(document.getElementById("divRecord").style.width.length, document.getElementById("divRecord").style.width.length - 1) != '%')

            document.getElementById('divRECG').style.width = entryZoomWidth * 0.995;
        document.getElementById('divA').style.width = parent.document.body.clientWidth * multWidth1;
        document.getElementById('divE').style.width = parent.document.body.clientWidth * multWidth1;
        document.getElementById('divC').style.width = parent.document.body.clientWidth * multWidth1;
    }
    catch (err)
    { return false; }
}
function nowrap() {
    addWidth();
    //    //催收
    var txt = document.getElementById('aspgirdRecord_ShareGridView');
    //    if(txt!=null){
    //    for(var i=0;i<txt.cells.length;i++)
    //    {
    //        document.getElementById('aspgirdRecord_ShareGridView').cells(i).noWrap = 'noWrap';
    //    }}
    //延滯
    txt = document.getElementById('aspbillGird_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspbillGird_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }

    txt = document.getElementById('aspgridCard_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspgridCard_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }
    //呆帳
    txt = document.getElementById('aspgridDebtN_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspgridDebtN_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }

    txt = document.getElementById('aspgridDebtY_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspgridDebtY_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }
    //綜合帳務
    txt = document.getElementById('aspBankGird_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspBankGird_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }

    txt = document.getElementById('aspCardGird_ShareGridView');
    if (txt != null) {
        for (var i = 0; i < txt.cells.length; i++) {
            document.getElementById('aspCardGird_ShareGridView').cells(i).noWrap = 'noWrap';
        }
    }
}

//讓Gridview內容不換行
//function noWrap(tableID)
//{
//    var tableObj=document.getElementById(tableID);
//    if(tableObj!=null){
//        for (var i=0; i < tableObj.cells.length; i++) {
//            tableObj.cells(i).noWrap = true;
//        }
//    }
//}

function FrameSizeChange() {
    multWidth = 0.945;
    switch (screen.width) {
        case 1024:
            multWidth = 0.93;
    }
    entryZoomWidth = parent.document.body.clientWidth * multWidth;
    var db = document.getElementById("divENTRY");
    var dc = document.getElementById("divRecord");
    var dd = document.getElementById("divCSI");
    var de = document.getElementById("divCumstormComplain");
    var dg = document.getElementById("divFinishCase");
    var dh = document.getElementById("divLA");
    var CIF = document.getElementById("DivCIFBasicData");
    var TEL = document.getElementById("divCIFTel");
    var ADDR = document.getElementById("divCIFAddr");
    var Inbound = document.getElementById("divInbound");
    var divSrch1 = document.getElementById("divSrch1");
    var blockAll = document.getElementById("blockAll");
    if (db != null)
    { db.style.width = entryZoomWidth; }
    if (dc != null)
    { dc.style.width = entryZoomWidth; }
    if (dd != null)
    { dd.style.width = entryZoomWidth; }
    if (de != null)
    { de.style.width = entryZoomWidth; }
    if (dg != null)
    { dg.style.width = entryZoomWidth; }
    if (dh != null)
    { dh.style.width = entryZoomWidth; }
    if (CIF != null)
    { CIF.style.width = entryZoomWidth; }
    if (TEL != null)
    { TEL.style.width = entryZoomWidth; }
    if (ADDR != null)
    { ADDR.style.width = entryZoomWidth; }


    if (divSrch1 != null)
    { divSrch1.style.width = entryZoomWidth * 1.01; }
    if (blockAll != null)
    { blockAll.style.width = entryZoomWidth * 1.01; }
    if (Inbound != null)
    { Inbound.style.width = entryZoomWidth * 1.01; }
    nowrap();
}
function ReloadHead() {
    var txt = document.getElementById('aspHiddenFieldReloadHead');
    if (txt.value != "") {
        txt.value = "";
        var head = parent.parent.document.frames.item('FrameHead');
        try {
            head.document.form1.submit();
        }
        catch (err)
        { return false; }

    }
}
function Flash() {
    var UserFileNo = document.getElementById('aspHiddenFieldReloadDebt');
    if (UserFileNo.value == 3)
        document.getElementById('btnNoFlash').click();
    UserFileNo.value = "";
}

//gridview radiobutton 
function sBankAcct(arg, txt, debt) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHLink');
    oTxAcc1.value = arg;
    var oTxAcc2 = document.getElementById('aspHiddenFieldDetail');
    oTxAcc2.value = txt;
    var oTxAcc3 = document.getElementById('aspHiddenFieldDebt');
    oTxAcc3.value = debt;
    document.getElementById('btnNoFlash').click();
}

function sCardAcct(arg, txt, debt) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHLink');
    oTxAcc1.value = arg;
    var oTxAcc2 = document.getElementById('aspHiddenFieldDetail');
    oTxAcc2.value = txt;
    var oTxAcc3 = document.getElementById('aspHiddenFieldDebt');
    oTxAcc3.value = debt;
    document.getElementById('btnNoFlash').click();
}

function sLinkBAcct(txt, grid1, grid2) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHAcc');
    oTxAcc1.value = txt;
    var t1 = checkRadChoice(grid1, 'BAcct');
    var t2 = checkRadChoice(grid2, 'BAcct');
    if (t1 == 'none' || t1 == 'Bill')
    { clearRadChoice(grid1, 'BAcct'); }
    if (t2 == 'none' || t2 == 'Bill')
    { clearRadChoice(grid2, 'BAcct'); }
    document.getElementById('btnNoFlash').click();
}

function sLinkCAcct(txt, grid1, grid2) {
    var oTxAcc1 = document.getElementById('aspHiddenFieldHCard');
    oTxAcc1.value = txt;
    var t1 = checkRadChoice(grid1, 'CAcct');
    var t2 = checkRadChoice(grid2, 'CAcct');
    if (t1 == 'none' || t1 == 'Card')
    { clearRadChoice(grid1, 'CAcct'); }
    if (t2 == 'none' || t2 == 'Card')
    { clearRadChoice(grid2, 'CAcct'); }
    document.getElementById('btnNoFlash').click();
}

var checkflag = false;
function clearRadChoice(grid, type) {
    if (grid == null || grid == '')
        return true;
    var ele = document.getElementsByTagName('input');
    for (var i = 0; i < ele.length - 1; i++) {
        if (ele[i].type == 'radio') {
            if (ele[i].id.indexOf('RadioChoice') >= 0 && ele[i].id.indexOf(grid) >= 0) {
                ele[i].checked = checkflag;
                GirdViewchangeColorCheckBox(ele[i]);
            }
        }
    }
}

function checkRadChoice(grid, type) {
    if (grid != 'aspgridDebtN') { return 'none'; } //只有無擔需要判斷銀或卡
    var checkitem = "";
    var txt = document.getElementById(grid + '_ShareGridView');
    if (txt != null && txt.cells(14) != null) {
        var ele = document.getElementsByTagName('input');
        for (var i = 0; i < ele.length - 1; i++) {
            if (ele[i].type == 'radio') {
                if (ele[i].id.indexOf('RadioChoice') >= 0 && ele[i].id.indexOf(grid) >= 0) {
                    if (ele[i].checked) {
                        var index = (parseInt(ele[i].id.split('_')[2].substring(3), 10) - 1) * 13 - 2 * (parseInt(ele[i].id.split('_')[2].substring(3), 10) - 2);
                        if (txt.cells(index).innerText == '信用卡')
                        { if (type == "CAcct") ele[i].checked = false; }
                        else { if (type == "BAcct") ele[i].checked = false; }
                    }
                }
            }
        }
    }
    return '';
}


function sNLinkBAcct(grid1) {
    var t1 = checkRadChoice(grid1, 'BAcct');
    if (t1 == 'none' || t1 == 'Bill')
    { clearRadChoice(grid1, 'BAcct'); }
}

function sNLinkCAcct(grid1) {
    var t1 = checkRadChoice(grid1, 'CAcct');
    if (t1 == 'none' || t1 == 'Card')
    { clearRadChoice(grid1, 'CAcct'); }
}

function hidDiry() {
    var d1 = document.getElementById('trigger_dtPay1');
    var d2 = document.getElementById('trigger_dtPay2');
    var dd1 = document.getElementById('aspTextBoxDtPay1');
    var dd2 = document.getElementById('aspTextBoxDtPay2');
    if (dd1 != null && d1 != null) {
        if (dd1.disabled)
            d1.disabled = "disabled";
    }
    if (dd2 != null && d2 != null) {
        if (dd2.disabled)
            d2.disabled = "disabled";
    }

}

function shred() {
    if (document.getElementById('HiddenFieldShRed') != null && document.getElementById('HiddenFieldShRed').value == "show")
        document.getElementById('TPinfo').innerHTML = '<font color=red>客訴 &amp; 重要情報</font>';
}

function checkChoice() {
    document.getElementById('aspCheckBoxFCTypeTel').disabled = false;
    document.getElementById('aspCheckBoxFCTypeAddr').disabled = false;
    var txt = document.getElementById('aspHiddenFieldChoice').value;
    if (txt == '02' || txt == '04' || txt == '06' || txt == '07' || txt == '08' || txt == '09') {
        document.getElementById('aspCheckBoxFCTypeTel').checked = false;
        document.getElementById('aspCheckBoxFCTypeAddr').checked = false;
        document.getElementById('aspCheckBoxFCTypeTel').disabled = true;
        document.getElementById('aspCheckBoxFCTypeAddr').disabled = true;
    }
}


function changeTap(obj) {
    if (obj != null)
        if (obj.className != "selected") {
        switch (obj.id) {
            case "CIFTel":
                document.getElementById("divCIFBasicData").style.display = "none";
                document.getElementById("divCIFTel").style.display = "block";
                document.getElementById("divCIFAddr").style.display = "none";
                document.getElementById("CIFBasicData").className = "";
                document.getElementById("CIFAddr").className = "";
                break;
            case "CIFAddr":
                document.getElementById("divCIFBasicData").style.display = "none";
                document.getElementById("divCIFTel").style.display = "none";
                document.getElementById("divCIFAddr").style.display = "block";
                document.getElementById("CIFBasicData").className = "";
                document.getElementById("CIFTel").className = "";
                break;
            default:
                document.getElementById("divCIFBasicData").style.display = "block";
                document.getElementById("divCIFTel").style.display = "none";
                document.getElementById("divCIFAddr").style.display = "none";
                document.getElementById("CIFAddr").className = "";
                document.getElementById("CIFTel").className = "";
                break;
        }
        obj.className = "selected";
        document.getElementById('aspHiddenFieldTabControlIndex2').value = obj.id;
    }
    return false;
}
function changeTapAmt(obj) {
    if (obj.className == "selected")
        return false;
    else {
        var txt = document.getElementById('aspHiddenFieldTabControlIndexValue').value;
        switch (obj.id) {
            case "AMTArr":
                if (txt == "GRID") {
                    showJSMessageFor_aspSysMessageMaster(msgGrid);
                    return false;
                }
                else {
                    return true;
                }
                break;
            case "AMTDebt":
                if (txt == "DEBT") {
                    showJSMessageFor_aspSysMessageMaster(msgDebt);
                    return false;
                }
                else {
                    return true;
                }
                break;
            default:
                return true;
                break;
        }
    }
}
function changeTapEntry(obj) {
    if (obj.className == "selected")
        return false;
}

function Click_SysMessage() {
    var y = document.documentElement.offsetHeight;
    var txt = document.getElementById('aspHiddenFieldClick_SysMessage');
    if (txt != null) {
        if (txt.value == "") {
            //往上縮
            window.scrollTo(0, y);
            txt.value = "Click";
        }
        else {
            //回復
            window.scrollTo(0, 0);
            txt.value = "";
        }
    }
}


