﻿//防止CTRL+N
document.onkeydown = function() {
    if (event.ctrlKey && event.keyCode == 78){
        event.keyCode = 0;
        event.returnValue = false;
        }        
}

function init()
  {
  var Detail = document.getElementById("fieldsetDetail");
  var Confirmbtn= document.getElementById("Confirmbtn"); 
  var Cancelbtn= document.getElementById("Cancelbtn"); 
  Detail.disabled=true;
  Confirmbtn.style.visibility="hidden"; 
  Cancelbtn.style.visibility="hidden"; 
} 
  
function  initBtn(fieldsetID,btnConfirmID,btnCancelID)
{
    var Detail = document.getElementById(fieldsetID);
    var Confirmbtn= document.getElementById(btnConfirmID);
    var Cancelbtn= document.getElementById(btnCancelID);
    Detail.disabled=true;
    Confirmbtn.style.visibility="hidden";
    Cancelbtn.style.visibility="hidden";
}  

function changeMode(type,fieldsetID,newButtonID,CancelID,mode){ 

switch(type)
{
case 0:
 var Detail = document.getElementById(fieldsetID);
 var Confirmbtn= document.getElementById(newButtonID);
 var Cancelbtn= document.getElementById(CancelID); 
 Detail.disabled=false;
 Confirmbtn.style.visibility="visible";
 Cancelbtn.style.visibility="visible"; 
if(mode=="add") 
{
 Confirmbtn.innerText="新增確認";
}
else 
{
 Confirmbtn.innerText="修改確認";
}
break;

case 1:
showTab('tabcont');

break;
case 2:
showTab('tabcont_TabPanel2_tabcont2');

break;
} 

}
function  initTab()
  {
  var Detail = document.getElementById("fieldsetDetail");
  var Confirmbtn= document.getElementById("tabcont_TabPanel1_Confirmbtn1"); 
  var Cancelbtn= document.getElementById("tabcont_TabPanel1_Cancelbtn1"); 
  Detail.disabled=true;
  Confirmbtn.style.visibility="hidden"; 
  Cancelbtn.style.visibility="hidden"; 
} 

function openWindow(url,winName,width,height) {
	var myWindow;
	var left = (width < 0 ) ? 0 : parseInt((screen.availWidth/2) - (width/2));
	var top  = (width < 0 ) ? 0 : parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height + 
			",resizable,left=" + left + ",top=" + top + 
			",screenX=" + left + ",screenY=" + top;
	if (width<0 && document.all){
		windowFeatures=windowFeatures+",fullscreen"
		myWindow = window.open(url, winName, windowFeatures);
	} else {
		myWindow = window.open(url, winName, windowFeatures);
		if (width < 0)	{
			var offset = (navigator.userAgent.indexOf("Mac") != -1 || 
										navigator.userAgent.indexOf("Gecko") != -1 || 
										navigator.appName.indexOf("Netscape") != -1) ? 0 : 4;
			myWindow.moveTo(-offset, -offset);
			myWindow.resizeTo(screen.availWidth + (2 * offset), screen.availHeight + (2 * offset));
		}
	}
//		return  ;
//	myWindow.focus();
	
}

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	}
	else {
    window.onload = function() { 
      if (oldonload) { 
        oldonload(); 
      } 
      func(); 
    }
	}

}

function changeColor(control,func) {
    if (func=='over') {
    //document.forms["form1"].elements
        control.style.backgroundColor='aqua';
    }
    else if (func=='out') {
        control.style.backgroundColor='';
    }
}

function changeColorCheckBox(control) {
    if (control.checked == true) {
        control.parentElement.parentElement.style.backgroundColor='aqua';
    }
    else {
        control.parentElement.parentElement.style.backgroundColor='';
    }
}

function getCookie(CookieName) {
  var sCookieName = CookieName + "=";
  var dc = document.cookie;
  //alert(dc)
  var begin, end;
	//alert(cookiename);
  if (dc.length > 0) {
      begin = dc.indexOf(sCookieName);
      if (begin != -1) {
          begin += sCookieName.length;
          end = dc.indexOf(";", begin);
          if (end == -1) {
              end = dc.length;
          }

          return unescape(dc.substring(begin, end));
      } 
  }
  return '';
}

function setCookie(CookieName, CookieValue, CookieExpire, CookieDomain, CookiePath){
	var sCookieName 	= CookieName;
	var sCookieValue 	= CookieValue;
	var sCookieExpire = CookieExpire;
	var sCookieDomain = CookieDomain;
	var sCookiePath 	= CookiePath;
	var expDay, setDay
	if(sCookieExpire!=''){
		sCookieExpire = eval(sCookieExpire);
		setDay = new Date();
		setDay.setTime(setDay.getTime()+(sCookieExpire*1000*60*60*24));
		expDay = setDay.toGMTString();
	}
	if(sCookiePath==''){
		sCookiePath = '/';
	}
  document.cookie = sCookieName + "=" + escape (sCookieValue) +
    ((sCookieExpire == '') ? "" : ("; expires=" + expDay)) +
    ((sCookiePath == '') ? "" : ("; path=" + sCookiePath)) +
    ((sCookieDomain == '') ? "" : ("; domain=" + sCookieDomain)) ;
}

function controlEnable(control,status)  //controlEnable('Panel1',false)
{
    var elements = document.getElementById(control).all;
    for (var i=0; i<elements.length; i++)
    {
        //alert(elements[i].id);
        elements[i].disabled =! status;
    }
}




function GirdViewchangeMode(fieldsetID,btnConfirmID,btnCancelID,mode)
{ 
    var Detail = document.getElementById(fieldsetID);
    var Confirmbtn= document.getElementById(btnConfirmID);
    var Cancelbtn= document.getElementById(btnCancelID);
    if(mode=="cancel")
    {
        Detail.disabled=true;
        Confirmbtn.style.visibility="hidden";
        Cancelbtn.style.visibility="hidden";
    }
    else
    { 
        Detail.disabled=false;
        Confirmbtn.style.visibility="visible";
        Cancelbtn.style.visibility="visible";
        if(mode=="add")
        {
            Confirmbtn.innerText="新增確認";
        }
        else
        { 
            Confirmbtn.innerText="修改確認";
        }
    }
}

function GirdViewshowTab(tabcont,tabIndex)
{
    var ctrl1 = $find(tabcont);
    ctrl1.set_activeTabIndex(tabIndex);
}

function show(headerText,fileName){
    var oTxtHeader = parent.frames[2].window.document.getElementById("txtHeader");
    var oTxtFileName = parent.frames[2].window.document.getElementById("txtFileName");
    oTxtFileName.value = fileName;
    oTxtHeader.value = headerText;
}    

function  ShowMessage(msg)
{
    var msBox = window.parent.document.getElementById('ddlMsg');
    var RightNow = new Date();
    if (msBox!=null)
    {
        var objListBoxItem=document.createElement('OPTION'); 
        objListBoxItem.value = msBox.options.length;
         objListBoxItem.text = msg;
         
        objListBoxItem.text =RightNow.getHours() + ':'+ RightNow.getMinutes() + ':' + RightNow.getSeconds() + '  '+ msg;
        msBox.add(objListBoxItem);
        msBox.options[msBox.options.length-1].selected=true;
        if (msBox.options.length==11)
        {
        msBox.remove(0);
        }
    jsChgTimes=1;
    clearInterval(jsChange);
     jsChange=setInterval("jsChangColor('red','blue')",500);
      
     
        
//         changeLabelColor(str, 500, 9, "window.parent.document.getElementById('lblMsg')", "red", "blue")
        }
        else
        {
        alert( msg );
        }
}  
       
  var msglabel = window.parent.document.getElementById('lblMsg');
  if (msglabel)
  {
  var jsChange;
  var jsChgTime;
   function jsChangColor(colorA,colorB)
   {
   if (jsChgTimes > 9 )
   {
   clearInterval(jsChange);
   jsChgTimes=0;
   }
   if (msglabel.style.color== colorA)
   {
   msglabel.style.color= colorB ;  
   }
   else
   {
   msglabel.style.color= colorA ;
   }
   jsChgTimes++;
   }
}